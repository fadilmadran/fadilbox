/*
 * TestSuitImpl.h
 *
 *  Created on: May 8, 2014
 *      Author: bulentk
 *
 */

#ifndef TestSuitImpl_h__
#define TestSuitImpl_h__

class TestSuitImpl :
    public ITestSuit
{
public:
    TestSuitImpl();
    virtual ~TestSuitImpl();

    void getScopes(
            std::vector<std::string>& lst
            );
};

#endif /* TestSuitImpl_h__ */
