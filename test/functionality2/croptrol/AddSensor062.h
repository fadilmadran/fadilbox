/*
 * AddSensor062.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef AddSensor062_h__
#define AddSensor062_h__

class AddSensor062 :
    public ITest
{
public:
    std::string getDescription() const;
    void run();
    void run(xcp1::Message& resp);
    void run(elw::util::Properties& p);
};

#endif /* AddSensor062_h__ */
