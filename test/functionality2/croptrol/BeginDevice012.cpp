/*
 * BeginDevice012.cpp
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#include "stdafx.h"
#include "Dapp.h"
#include "BeginDevice012.h"
#include "util.h"

std::string BeginDevice012::getDescription() const
{
    return "MonSrvr BeginDevice request. INVALID TYPE ";
}

void BeginDevice012::run()
{
	Dapp::instance().reset();

    util::bact();

	std::string type = "99" ;
    elw::util::Properties p_bd_resp;
    xcp1::Message resp;
    util::begindevice(	type,
    					resp,
    					p_bd_resp);

    util::checkAttribute(xcp::attr::rslt, resp, "1"); // success

    if (resp.getAttribute(xcp::attr::error).getValues().size() != 1)
        throw elw::lang::Exception(MKSTR("invalid err desc. size:"
        		<< resp.getAttribute(xcp::attr::error).getValues().size()));
    if (resp.getAttribute(xcp::attr::error).getValues()[0] != "8" )
        throw elw::lang::Exception(MKSTR("invalid err desc. expected:"
        		<< 8 << " received:"
        		<< resp.getAttribute(xcp::attr::error).getValues()[0]));

}
