/*
 * RemoveSensor032.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef RemoveSensor032_h__
#define RemoveSensor032_h__

class RemoveSensor032 :
    public ITest
{
public:
    std::string getDescription() const;
    void run();
};

#endif /* RemoveSensor032_h__ */
