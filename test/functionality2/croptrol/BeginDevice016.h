/*
 * BeginDevice016.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef BeginDevice016_h__
#define BeginDevice016_h__

class BeginDevice016 :
    public ITest
{
public:
    std::string getDescription() const;
    void run();
    void run(xcp1::Message& resp);
    void run(elw::util::Properties& prop);
};

#endif /* BeginDevice016_h__ */
