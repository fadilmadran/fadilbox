/*
 * RemoveSensor021.cpp
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#include "stdafx.h"
#include "Dapp.h"
#include "RemoveSensor021.h"
#include "util.h"
#include "AddSensor021.h"

std::string RemoveSensor021::getDescription() const
{
    return "MonSrvr RS request. type:TELNET, IP:192.168.1.182-38000, ionum:0";
}

void RemoveSensor021::run()
{
	elw::util::Properties p_as_resp;
		AddSensor021 add;
		add.run(p_as_resp);

	    xcp1::Message resp;
		util::removeSensor(
				p_as_resp.get("devid"),
				p_as_resp.get("ionum"),
	    		resp);

		  util::checkAttribute(xcp::attr::rslt, resp, "0"); // success

		    if (resp.getAttribute(xcp::attr::port).getValues().size() != 1)
		        throw elw::lang::Exception(MKSTR("invalid port. size:"
		        		<< resp.getAttribute(xcp::attr::port).getValues().size()));
		    if (resp.getAttribute(xcp::attr::port).getValues()[0] != p_as_resp.get("port"))
		        throw elw::lang::Exception(MKSTR("invalid port. "
		        		"expected:"<< p_as_resp.get("port") <<
		        		"received:" << resp.getAttribute(xcp::attr::port).getValues()[0]));

		    if (resp.getAttribute(xcp::attr::devid).getValues().size() != 1)
		        throw elw::lang::Exception(MKSTR("invalid devid. size:"
		        		<< resp.getAttribute(xcp::attr::devid).getValues().size()));
		    if (resp.getAttribute(xcp::attr::devid).getValues()[0] != p_as_resp.get("devid"))
		        throw elw::lang::Exception(MKSTR("invalid devid. "
		        		"expected:"<< p_as_resp.get("devid") <<
		        		"received:" << resp.getAttribute(xcp::attr::devid).getValues()[0]));

		    if (resp.getAttribute(xcp::attr::ionum).getValues().size() != 1)
		        throw elw::lang::Exception(MKSTR("invalid ionum. size:"
		        		<< resp.getAttribute(xcp::attr::ionum).getValues().size()));
		    if (resp.getAttribute(xcp::attr::ionum).getValues()[0] != p_as_resp.get("ionum"))
		        throw elw::lang::Exception(MKSTR("invalid ionum. "
		        		"expected:"<< p_as_resp.get("ionum") <<
		        		"received:" << resp.getAttribute(xcp::attr::ionum).getValues()[0]));

		    try
		    	{
		    	elw::util::Properties p_wd_resp;
		    	util::waitForNewSensorData(p_wd_resp,5000);
		    	}
		    catch(elw::lang::Exception& e)
				{
		    	if (std::string(e.what()) != "receive timeout") throw e;
				}
	}
