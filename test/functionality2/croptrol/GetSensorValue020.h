/*
 * GetSensorValue020.h
 *
 *  Created on: May 12, 2014
 *      Author: bulentk
 *
 */

#ifndef GetSensorValue020_h__
#define GetSensorValue020_h__

class GetSensorValue020 :
    public ITest
{
public:
    std::string getDescription() const;
    void run();
};

#endif /* GetSensorValue020_h__ */
