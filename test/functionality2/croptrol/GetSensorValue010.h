/*
 * GetSensorValue010.h
 *
 *  Created on: May 12, 2014
 *      Author: bulentk
 *
 */

#ifndef GetSensorValue010_h__
#define GetSensorValue010_h__

class GetSensorValue010 :
    public ITest
{
public:
    std::string getDescription() const;
    void run();
};

#endif /* GetSensorValue010_h__ */
