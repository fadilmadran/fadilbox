/*
 * RemoveSensor051.cpp
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#include "stdafx.h"
#include "Dapp.h"
#include "RemoveSensor051.h"
#include "AddSensor050.h"

std::string RemoveSensor051::getDescription() const
{
    return "MonSrvr RS request. INVALID DEVID"
    			"type:PING, "
    			"IP:192.168.1.182, ionum:0	"
				"IP:192.168.1.136, ionum:1	"
    			"IP:192.168.1.33, ionum:2	"
    			"type:TELNET   "
    			"IP:192.168.1.182, ionum:0	"
    			"IP:192.168.1.223, ionum:1";
}

void RemoveSensor051::run()
{
	std::vector<util::sDevice> lstAdded = util::getDeviceList();
	AddSensor050 add;
	add.run(lstAdded);

	std::vector<util::sDevice>::iterator it = lstAdded.begin() ;


	std::list<std::string> tempKeys = (*it).ip.getKeys();
	std::list<std::string>::iterator it2 = tempKeys.begin();

	xcp1::Message resp;
	util::removeSensor(
			MKSTR( ::atoi( (*it).devid.c_str() ) + 2 ),
			(*it2).data() ,
			resp);

	util::checkAttribute(xcp::attr::rslt, resp, "1"); // success

	if (resp.getAttribute(xcp::attr::error).getValues().size() != 1)
		throw elw::lang::Exception(MKSTR("invalid err desc. size:"
				<< resp.getAttribute(xcp::attr::error).getValues().size()));
	if (resp.getAttribute(xcp::attr::error).getValues()[0] != MKSTR(xcp::attr::devid))
		throw elw::lang::Exception(MKSTR("invalid err desc. expected:"
				<< xcp::attr::devid << " received:"
				<< resp.getAttribute(xcp::attr::error).getValues()[0]));



	}
