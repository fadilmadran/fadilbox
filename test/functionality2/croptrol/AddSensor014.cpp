/*
 * AddSensor014.cpp
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#include "stdafx.h"
#include "Dapp.h"
#include "AddSensor014.h"
#include "util.h"

std::string AddSensor014::getDescription() const
{
    return "MonSrvr AS request. WITHOUT EndDevice type:PING, IP:192.168.1.182, ionum:0";
}

void AddSensor014::run()
{
	Dapp::instance().reset();
	Dapp::instance().reset();
    util::bact();

	std::string type = MKSTR (conf::dev::PING ) ;
    elw::util::Properties p_bd_resp;
    xcp1::Message resp;
    util::begindevice(	type,
    					resp,
    					p_bd_resp);


    std::string io = "0";
	std::string IP="192.168.1.182";
    util::addSensor(
    		p_bd_resp.get("port"),
    		p_bd_resp.get("devid") ,
    		io,
    		IP,
    		resp);

    util::checkAttribute(xcp::attr::rslt, resp, "0"); // success

    if (resp.getAttribute(xcp::attr::port).getValues().size() != 1)
            throw elw::lang::Exception(MKSTR("invalid port. size:"
            		<< resp.getAttribute(xcp::attr::port).getValues().size()));
	if (resp.getAttribute(xcp::attr::port).getValues()[0] != p_bd_resp.get("port"))
		throw elw::lang::Exception(MKSTR("invalid port. "
				"expected:" << p_bd_resp.get("port") <<
				"received:" << resp.getAttribute(xcp::attr::port).getValues()[0]));

    if (resp.getAttribute(xcp::attr::devid).getValues().size() != 1)
        throw elw::lang::Exception(MKSTR("invalid devid. size:"
        		<< resp.getAttribute(xcp::attr::devid).getValues().size()));
    if (resp.getAttribute(xcp::attr::devid).getValues()[0] != p_bd_resp.get("devid"))
        throw elw::lang::Exception(MKSTR("invalid devid. "
        		"expected:" << p_bd_resp.get("devid") <<
        		"received:" << resp.getAttribute(xcp::attr::devid).getValues()[0]));

    if (resp.getAttribute(xcp::attr::ionum).getValues().size() != 1)
        throw elw::lang::Exception(MKSTR("invalid ionum. size:"
        		<< resp.getAttribute(xcp::attr::ionum).getValues().size()));
    if (resp.getAttribute(xcp::attr::ionum).getValues()[0] != io)
        throw elw::lang::Exception(MKSTR("invalid ionum. "
        		"expected:" << io <<
        		"received:" << resp.getAttribute(xcp::attr::ionum).getValues()[0]));


//	util::enddevice();

	util::eact();

    try
    	{
    	elw::util::Properties p_wd_resp;
        util::waitForNewSensorData(p_wd_resp,5000);
    	}
    catch(elw::lang::Exception& e)
		{
    	if (std::string(e.what()) != "receive timeout") throw e;
		}
}

