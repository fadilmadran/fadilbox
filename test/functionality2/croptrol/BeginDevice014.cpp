/*
 * BeginDevice014.cpp
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#include "stdafx.h"
#include "Dapp.h"
#include "BeginDevice014.h"
#include "util.h"

std::string BeginDevice014::getDescription() const
{
    return "MonSrvr BeginDevice request. IN ACTIVATION PING, IN RUNNING TELNET;";
}

void BeginDevice014::run()
{
	Dapp::instance().reset();
    util::bact();

	std::string type = MKSTR (conf::dev::PING ) ;
    elw::util::Properties p_bd_resp;
    xcp1::Message resp;
    util::begindevice(	type,
    					resp,
    					p_bd_resp);

    util::checkAttribute(xcp::attr::rslt, resp, "0"); // success

	util::enddevice( );
	util::eact();

	type = MKSTR (conf::dev::TELNET ) ;
    elw::util::Properties p_bd_resp2;
    util::begindevice(	type,
    					resp,
    					p_bd_resp2);

    util::checkAttribute(xcp::attr::rslt, resp, "0"); // success

    if (resp.getAttribute(xcp::attr::devid).getValues().size() != 1)
        throw elw::lang::Exception(MKSTR("invalid devid. size:"
       		 << resp.getAttribute(xcp::attr::devid).getValues().size()));
    if (p_bd_resp.get("devid") == p_bd_resp2.get("devid"))
        throw elw::lang::Exception(MKSTR("invalid devid. "
        		"expected diffrent from:"<< p_bd_resp.get("devid") <<
        		 "received:" << p_bd_resp2.get("devid") ));

	util::enddevice( );


}
