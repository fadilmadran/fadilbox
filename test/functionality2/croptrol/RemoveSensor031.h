/*
 * RemoveSensor031.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef RemoveSensor031_h__
#define RemoveSensor031_h__

class RemoveSensor031 :
    public ITest
{
public:
    std::string getDescription() const;
    void run();
};

#endif /* RemoveSensor031_h__ */
