/*
 * GetSensorValue023.h
 *
 *  Created on: May 12, 2014
 *      Author: bulentk
 *
 */

#ifndef GetSensorValue023_h__
#define GetSensorValue023_h__

class GetSensorValue023 :
    public ITest
{
public:
    std::string getDescription() const;
    void run();
};

#endif /* GetSensorValue023_h__ */
