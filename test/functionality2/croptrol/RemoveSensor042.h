/*
 * RemoveSensor042.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef RemoveSensor042_h__
#define RemoveSensor042_h__

class RemoveSensor042 :
    public ITest
{
public:
    std::string getDescription() const;
    void run();
};

#endif /* RemoveSensor042_h__ */
