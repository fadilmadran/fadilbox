/*
 * AddSensor031.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef AddSensor031_h__
#define AddSensor031_h__

class AddSensor031 :
    public ITest
{
public:
    std::string getDescription() const;
    void run();
    void run(xcp1::Message& resp);
    void run(elw::util::Properties& prop);
};

#endif /* AddSensor031_h__ */
