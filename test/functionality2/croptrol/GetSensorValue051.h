/*
 * GetSensorValue051.h
 *
 *  Created on: May 12, 2014
 *      Author: bulentk
 *
 */

#ifndef GetSensorValue051_h__
#define GetSensorValue051_h__

#include "util.h"

class GetSensorValue051 :
    public ITest
{
public:
    std::string getDescription() const;
    void run();
};

#endif /* GetSensorValue051_h__ */
