/*
 * GetSensorValue022.h
 *
 *  Created on: May 12, 2014
 *      Author: bulentk
 *
 */

#ifndef GetSensorValue022_h__
#define GetSensorValue022_h__

class GetSensorValue022 :
    public ITest
{
public:
    std::string getDescription() const;
    void run();
};

#endif /* GetSensorValue022_h__ */
