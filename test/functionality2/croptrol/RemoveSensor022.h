/*
 * RemoveSensor022.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef RemoveSensor022_h__
#define RemoveSensor022_h__

class RemoveSensor022 :
    public ITest
{
public:
    std::string getDescription() const;
    void run();
};

#endif /* RemoveSensor022_h__ */
