/*
 * RemoveSensor020.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef RemoveSensor020_h__
#define RemoveSensor020_h__

class RemoveSensor020 :
    public ITest
{
public:
    std::string getDescription() const;
    void run();
};

#endif /* RemoveSensor020_h__ */
