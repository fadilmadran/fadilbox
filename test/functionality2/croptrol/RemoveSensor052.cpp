/*
 * RemoveSensor052.cpp
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#include "stdafx.h"
#include "Dapp.h"
#include "RemoveSensor052.h"
#include "AddSensor050.h"

std::string RemoveSensor052::getDescription() const
{
    return "MonSrvr RS request. INVALID IONUM"
    			"type:PING, "
    			"IP:192.168.1.182, ionum:0	"
				"IP:192.168.1.136, ionum:1	"
    			"IP:192.168.1.33, ionum:2	"
    			"type:TELNET   "
    			"IP:192.168.1.182, ionum:0	"
    			"IP:192.168.1.223, ionum:1";
}

void RemoveSensor052::run()
{
	std::vector<util::sDevice> lstAdded = util::getDeviceList();
	AddSensor050 add;
	add.run(lstAdded);

	std::vector<util::sDevice>::iterator it = lstAdded.begin() ;


	std::list<std::string> tempKeys = (*it).ip.getKeys();
	std::list<std::string>::iterator it2 = tempKeys.begin();

	xcp1::Message resp;
	util::removeSensor(
			(*it).devid,
			MKSTR(  ::atoi ( (*it2).data() ) + 10 ),
			resp);

	util::checkAttribute(xcp::attr::rslt, resp, "1"); // success

	if (resp.getAttribute(xcp::attr::error).getValues().size() != 1)
		throw elw::lang::Exception(MKSTR("invalid err desc. size:"
				<< resp.getAttribute(xcp::attr::error).getValues().size()));
	if (resp.getAttribute(xcp::attr::error).getValues()[0] != MKSTR(xcp::attr::ionum))
		throw elw::lang::Exception(MKSTR("invalid err desc. expected:"
				<< xcp::attr::ionum << " received:"
				<< resp.getAttribute(xcp::attr::error).getValues()[0]));



}
