/*
 * RemoveDevice011.cpp
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#include "stdafx.h"
#include "Dapp.h"
#include "RemoveDevice011.h"
#include "util.h"
#include "AddSensor010.h"

std::string RemoveDevice011::getDescription() const
{
    return "MonSrvr RD request.INVALID DEVID, type:PING";
}

void RemoveDevice011::run()
{
	elw::util::Properties p_as_resp;
	AddSensor010 add;
	add.run(p_as_resp);

    xcp1::Message resp;
	util::removedevice(
			p_as_resp.get("port"),
    		MKSTR( p_as_resp.get<int>("devid") + 1 ),
    		resp);

    util::checkAttribute(xcp::attr::rslt, resp, "1"); // success

    if (resp.getAttribute(xcp::attr::error).getValues().size() != 1)
        throw elw::lang::Exception(MKSTR("invalid err desc. size:"
        		<< resp.getAttribute(xcp::attr::error).getValues().size()));
    if (resp.getAttribute(xcp::attr::error).getValues()[0] != MKSTR(xcp::attr::devid))
        throw elw::lang::Exception(MKSTR("invalid err desc. expected:"
        		<< xcp::attr::devid << " received:"
        		<< resp.getAttribute(xcp::attr::error).getValues()[0]));
}
