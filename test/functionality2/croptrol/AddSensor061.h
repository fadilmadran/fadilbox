/*
 * AddSensor061.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef AddSensor061_h__
#define AddSensor061_h__

class AddSensor061 :
    public ITest
{
public:
    std::string getDescription() const;
    void run();
    void run(xcp1::Message& resp);
    void run(elw::util::Properties& p);
};

#endif /* AddSensor061_h__ */
