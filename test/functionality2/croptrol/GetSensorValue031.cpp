/*
 * GetSensorValue031.cpp
 *
 *  Created on: May 12, 2014
 *      Author: bulentk
 *
 */

#include "stdafx.h"
#include "Dapp.h"
#include "GetSensorValue031.h"
#include "util.h"
#include "AddSensor030.h"


std::string GetSensorValue031::getDescription() const
{
    return "MonSrvr GSD request.  INVALID DEVID type:PING, IP:192.168.1.182, ionum:0 "
    		"IP:192.168.1.136, ionum:1 "
    		"IP:192.168.1.33, ionum:2 ";
}

void GetSensorValue031::run()
{
	std::vector<util::sDevice> lstAdded = util::getDeviceList("PING");
	AddSensor030 add;
	add.run(lstAdded);

	std::vector<util::sDevice>::iterator it = lstAdded.begin() ;


	std::list<std::string> tempKeys = (*it).ip.getKeys();
	std::list<std::string>::iterator it2 = tempKeys.begin();

	xcp1::Message resp;
	util::getSensorData(
			MKSTR( ::atoi( (*it).devid.c_str() ) + 2 ),
			(*it2).data() ,
			resp);

	util::checkAttribute(xcp::attr::rslt, resp, "1"); // success

	if (resp.getAttribute(xcp::attr::error).getValues().size() != 1)
		throw elw::lang::Exception(MKSTR("invalid err desc. size:"
				<< resp.getAttribute(xcp::attr::error).getValues().size()));
	if (resp.getAttribute(xcp::attr::error).getValues()[0] != MKSTR(xcp::attr::devid))
		throw elw::lang::Exception(MKSTR("invalid err desc. expected:"
				<< xcp::attr::devid << " received:"
				<< resp.getAttribute(xcp::attr::error).getValues()[0]));



}
