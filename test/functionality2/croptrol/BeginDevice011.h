/*
 * BeginDevice011.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef BeginDevice011_h__
#define BeginDevice011_h__

class BeginDevice011 :
    public ITest
{
public:
    std::string getDescription() const;
    void run();
    void run(xcp1::Message& resp);
    void run(elw::util::Properties& prop);
};

#endif /* BeginDevice011_h__ */
