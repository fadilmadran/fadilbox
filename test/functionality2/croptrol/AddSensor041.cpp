/*
 * AddSensor041.cpp
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#include "stdafx.h"
#include "Dapp.h"
#include "AddSensor041.h"
#include "util.h"

std::string AddSensor041::getDescription() const
{
    return "MonSrvr AS request. WITHOUT EndActivation type:TELNET, "
    		"IP:192.168.1.182, ionum:0 "
    		"IP:192.168.1.223, ionum:1 ";
}

void AddSensor041::run()
{
	Dapp::instance().reset();
	    util::bact();

		std::vector<util::sDevice> lstAdded = util::getDeviceList("TELNET");

		elw::util::Properties p_bd_resp;
		std::vector<util::sDevice>::iterator it;
		for (it = lstAdded.begin() ; it != lstAdded.end() ; ++it)
		{

		    elw::util::Properties p_bd_resp;
		    xcp1::Message resp;
		    util::begindevice(	(*it).devType,
		    					resp,
		    					p_bd_resp);

			util::checkAttribute(xcp::attr::rslt, resp, "0"); // success

			(*it).devid=p_bd_resp.get("devid");
			(*it).port=p_bd_resp.get("port");

			std::list<std::string> tempKeys = (*it).ip.getKeys();
			std::list<std::string>::iterator it2;
			for(it2 = tempKeys.begin(); it2 != tempKeys.end(); it2++)
			{

				std::string IP="192.168.1.182";
			    util::addSensor(
			    		(*it).port,
			    		(*it).devid ,
			    		(*it2).data(),
			    		(*it).ip.get((*it2).data()),
			    		resp);

				util::checkAttribute(xcp::attr::rslt, resp, "0"); // success

				if (resp.getAttribute(xcp::attr::port).getValues().size() != 1)
						throw elw::lang::Exception(MKSTR("invalid port. size:"
								<< resp.getAttribute(xcp::attr::port).getValues().size()));
				if (resp.getAttribute(xcp::attr::port).getValues()[0] != (*it).port )
					throw elw::lang::Exception(MKSTR("invalid port. "
							"expected:" << (*it).port <<
							"received:" << resp.getAttribute(xcp::attr::port).getValues()[0]));

				if (resp.getAttribute(xcp::attr::devid).getValues().size() != 1)
					throw elw::lang::Exception(MKSTR("invalid devid. size:"
							<< resp.getAttribute(xcp::attr::devid).getValues().size()));
				if (resp.getAttribute(xcp::attr::devid).getValues()[0] != (*it).devid)
					throw elw::lang::Exception(MKSTR("invalid devid. "
							"expected:" << (*it).devid <<
							"received:" << resp.getAttribute(xcp::attr::devid).getValues()[0]));

				if (resp.getAttribute(xcp::attr::ionum).getValues().size() != 1)
					throw elw::lang::Exception(MKSTR("invalid ionum. size:"
							<< resp.getAttribute(xcp::attr::ionum).getValues().size()));
				if (resp.getAttribute(xcp::attr::ionum).getValues()[0] != (*it2).data())
					throw elw::lang::Exception(MKSTR("invalid ionum. "
							"expected:" << (*it2).data() <<
							"received:" << resp.getAttribute(xcp::attr::ionum).getValues()[0]));
			}


			util::enddevice();
		}

//		util::eact();


	try
		{
		elw::util::Properties p_wd_resp;
		util::waitForNewSensorData(p_wd_resp,5000);
		}
	catch(elw::lang::Exception& e)
		{
		if (std::string(e.what()) != "receive timeout") throw e;
		}



}
