/*
 * GetSensorValue040.cpp
 *
 *  Created on: May 12, 2014
 *      Author: bulentk
 *
 */

#include "stdafx.h"
#include "Dapp.h"
#include "GetSensorValue040.h"
#include "util.h"
#include "AddSensor040.h"


std::string GetSensorValue040::getDescription() const
{
    return "MonSrvr GSD request.  type:TELNET, IP:192.168.1.182, ionum:0 "
    		"IP:192.168.1.223, ionum:1 ";
}

void GetSensorValue040::run()
{
	std::vector<util::sDevice> lstAdded = util::getDeviceList("TELNET");
	AddSensor040 add;
	add.run(lstAdded);

	std::vector<util::sDevice>::iterator it;
	for (it = lstAdded.begin() ; it != lstAdded.end() ; it++)
	{

		std::list<std::string> tempKeys = (*it).ip.getKeys();
		std::list<std::string>::iterator it2;
		for(it2 = tempKeys.begin(); it2 != tempKeys.end(); it2++)
		{
			xcp1::Message resp;
			util::getSensorData(
						(*it).devid,
						(*it2).data() ,
						resp
						);

			util::checkAttribute(xcp::attr::rslt, resp, "0"); // success

			if (resp.getAttribute(xcp::attr::port).getValues().size() != 1)
				throw elw::lang::Exception(MKSTR("invalid port. size:"
						<< resp.getAttribute(xcp::attr::port).getValues().size()));
			if (resp.getAttribute(xcp::attr::port).getValues()[0] != (*it).port)
				throw elw::lang::Exception(MKSTR("invalid port. "
						"expected:" << (*it).port <<
						"received:" << resp.getAttribute(xcp::attr::port).getValues()[0]));

			if (resp.getAttribute(xcp::attr::devid).getValues().size() != 1)
				throw elw::lang::Exception(MKSTR("invalid devid. size:"
						<< resp.getAttribute(xcp::attr::devid).getValues().size()));
			if (resp.getAttribute(xcp::attr::devid).getValues()[0] != (*it).devid)
				throw elw::lang::Exception(MKSTR("invalid devid. "
						"expected:" << (*it).devid <<
						"received:" << resp.getAttribute(xcp::attr::devid).getValues()[0]));

			if (resp.getAttribute(xcp::attr::ionum).getValues().size() != 1)
				throw elw::lang::Exception(MKSTR("invalid ionum. size:"
						<< resp.getAttribute(xcp::attr::ionum).getValues().size()));
			if (resp.getAttribute(xcp::attr::ionum).getValues()[0] != (*it2).data())
				throw elw::lang::Exception(MKSTR("invalid ionum. "
						"expected:" << (*it2).data() <<
						"received:" << resp.getAttribute(xcp::attr::ionum).getValues()[0]));

		    if (resp.getAttribute(7).getValues().size() != 1)
		        throw elw::lang::Exception(MKSTR("invalid value. size:"
		        		<< resp.getAttribute(7).getValues().size()));
		    if (resp.getAttribute(8).getValues().size() != 1)
		        throw elw::lang::Exception(MKSTR("invalid time. size:"
		        		<< resp.getAttribute(8).getValues().size()));
		}

	}

}
