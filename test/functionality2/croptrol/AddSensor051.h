/*
 * AddSensor051.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef AddSensor051_h__
#define AddSensor051_h__

#include "AddSensor050.h"
#include "util.h"

class AddSensor051 :
    public ITest
{
public:
    std::string getDescription() const;
    void run();
    void run(xcp1::Message& resp);
    void run(elw::util::Properties& prop);
    void run(std::vector<util::sDevice>& lstAdded);
};



#endif /* AddSensor051___ */
