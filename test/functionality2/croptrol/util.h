///*
// * croptrol_util.h
// *
// *  Created on: May 9, 2014
// *      Author: bulentk
// *
// */
//
//#ifndef croptrol_util_h__
//#define croptrol_util_h__
//
//namespace util {
//
//void reset();
//void bact();
//void eact();
//
//void  begindevice(	std::string const type,
//					xcp1::Message& resp,
//					elw::util::Properties& p);
//
//void  removedevice(	std::string const& port,
//					std::string const& devid,
//					xcp1::Message& resp
//					);
//
//
//void enddevice();
//
//void addSensor(
//		std::string const port,
//		std::string const devid,
//		std::string const ionum,
//		std::string IP,
//		xcp1::Message& resp
//		);
//
//
//
//void removeSensor(
//		std::string const devid,
//		std::string const ionum,
//		xcp1::Message& resp
//		);
//
//
//void getSensorData(
//		std::string const devid,
//		std::string const ionum,
//		xcp1::Message& resp
//		);
//
//void sensorAtWarning(
//        std::string const& ionum,
//        xcp1::Message& resp
//        );
//
//void waitForNewSensorData(
//        elw::util::Properties& p,
//        dword dwWait
//        );
//
////void waitForNewSensorData(
////        elw::util::Properties& p,
////        dword dwWait,
////    	int port,
////    	int  did,
////    	int io
////        );
//
//void waitForNewSensorData(
//        elw::util::Properties& p,
//        dword dwWait
//        );
//
//
//bool isActive(
//        std::list<elw::util::Properties> const& lst,
//        std::string const& ionum
//        );
//
//bool isActive(
//        std::vector<std::string> const& ionums
//        );
//
//bool isActive(
//        std::string const& ionum
//        );
//
//void checkAttribute(
//        int i,
//        xcp1::Message const& m,
//        std::string const& val
//        );
//
//struct sDevice
//{
//	std::string devType;
//	std::string devid;
//	std::string port;
//	elw::util::Properties ip; // ionum, ip
//	int max_ionum;
//};
//
//std::vector<sDevice> getDeviceList ();
//
//std::vector<sDevice> getDeviceList (std::string devType);
//
//
//};
//
//
//
//#endif /* croptrol_util_h__ */
