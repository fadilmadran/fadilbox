/*
 * AddSensor030.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef AddSensor030_h__
#define AddSensor030_h__

#include "util.h"

class AddSensor030 :
    public ITest
{
public:
    std::string getDescription() const;
    void run();
    void run(xcp1::Message& resp);
    void run(elw::util::Properties& prop);
    void run(std::vector<util::sDevice>& lstAdded);
};



#endif /* AddSensor030___ */
