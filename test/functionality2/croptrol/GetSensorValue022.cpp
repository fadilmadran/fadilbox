/*
 * GetSensorValue022.cpp
 *
 *  Created on: May 12, 2014
 *      Author: bulentk
 *
 */

#include "stdafx.h"
#include "Dapp.h"
#include "GetSensorValue022.h"
#include "util.h"
#include "AddSensor020.h"


std::string GetSensorValue022::getDescription() const
{
    return "MonSrvr GSD request. INVALID DEVID type:TELNET, IP:192.168.1.182, ionum:0";
}

void GetSensorValue022::run()
{
	elw::util::Properties prop;
	AddSensor020 add;
	add.run(prop);

    xcp1::Message resp;
	util::getSensorData(
    		MKSTR( prop.get<int>("devid") + 1 ),
    		prop.get("ionum"),
    		resp);

    util::checkAttribute(xcp::attr::rslt, resp, "1"); // success

    if (resp.getAttribute(xcp::attr::error).getValues().size() != 1)
        throw elw::lang::Exception(MKSTR("invalid err desc. size:"
        		<< resp.getAttribute(xcp::attr::error).getValues().size()));
    if (resp.getAttribute(xcp::attr::error).getValues()[0] != MKSTR(xcp::attr::devid))
        throw elw::lang::Exception(MKSTR("invalid err desc. expected:"
        		<< xcp::attr::devid << " received:"
        		<< resp.getAttribute(xcp::attr::error).getValues()[0]));
}

