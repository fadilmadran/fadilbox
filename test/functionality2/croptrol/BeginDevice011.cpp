/*
 * BeginDevice011.cpp
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#include "stdafx.h"
#include "Dapp.h"
#include "BeginDevice011.h"
#include "util.h"

std::string BeginDevice011::getDescription() const
{
    return "MonSrvr BeginDevice request. type:TELNET ";
}

void BeginDevice011::run()
{
	Dapp::instance().reset();

    util::bact();

	std::string type = MKSTR (conf::dev::TELNET ) ;
    elw::util::Properties p_bd_resp;
    xcp1::Message resp;
    util::begindevice(	type,
    					resp,
    					p_bd_resp);

    util::checkAttribute(xcp::attr::rslt, resp, "0"); // success


	util::enddevice( );

	util::eact();

}
