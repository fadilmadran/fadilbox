/*
 * AddSensor023.cpp
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#include "stdafx.h"
#include "Dapp.h"
#include "AddSensor023.h"
#include "util.h"

std::string AddSensor023::getDescription() const
{
    return "MonSrvr AS request. INVALID DEVID type:TELNET, IP:192.168.1.182, ionum:0";
}

void AddSensor023::run()
{
	Dapp::instance().reset();
    util::bact();

	std::string type = MKSTR (conf::dev::TELNET ) ;
    elw::util::Properties p_bd_resp;
    xcp1::Message resp;
    util::begindevice(	type,
    					resp,
    					p_bd_resp);


    std::string io = "0";
	std::string IP="192.168.1.182";
    util::addSensor(
    		p_bd_resp.get("port") ,
    		MKSTR ( p_bd_resp.get<int>("devid") + 1 ),
    		io,
    		IP,
    		resp);

    util::checkAttribute(xcp::attr::rslt, resp, "1"); // success

    if (resp.getAttribute(xcp::attr::error).getValues().size() != 1)
        throw elw::lang::Exception(MKSTR("invalid err desc. size:"
        		<< resp.getAttribute(xcp::attr::error).getValues().size()));
    if (resp.getAttribute(xcp::attr::error).getValues()[0] != MKSTR(xcp::attr::devid))
        throw elw::lang::Exception(MKSTR("invalid err desc. "
        		"expected:"	<< xcp::attr::devid <<
        		"received:" << resp.getAttribute(xcp::attr::error).getValues()[0]));

}
