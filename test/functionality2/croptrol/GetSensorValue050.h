/*
 * GetSensorValue050.h
 *
 *  Created on: May 12, 2014
 *      Author: bulentk
 *
 */

#ifndef GetSensorValue050_h__
#define GetSensorValue050_h__

#include "util.h"

class GetSensorValue050 :
    public ITest
{
public:
    std::string getDescription() const;
    void run();
};

#endif /* GetSensorValue050_h__ */
