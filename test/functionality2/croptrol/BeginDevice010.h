/*
 * BeginDevice010.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef BeginDevice010_h__
#define BeginDevice010_h__

class BeginDevice010 :
    public ITest
{
public:
    std::string getDescription() const;
    void run();
    void run(xcp1::Message& resp);
    void run(elw::util::Properties& prop);
};

#endif /* BeginDevice010_h__ */
