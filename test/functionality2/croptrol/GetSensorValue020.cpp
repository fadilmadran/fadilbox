/*
 * GetSensorValue020.cpp
 *
 *  Created on: May 12, 2014
 *      Author: bulentk
 *
 */

#include "stdafx.h"
#include "Dapp.h"
#include "GetSensorValue020.h"
#include "util.h"
#include "AddSensor020.h"


std::string GetSensorValue020::getDescription() const
{
    return "MonSrvr GSD request. type:TELNET, IP:192.168.1.182, ionum:0";
}

void GetSensorValue020::run()
{
	elw::util::Properties p_as_resp;
		AddSensor020 add;
		add.run(p_as_resp);

	    xcp1::Message resp;
		util::getSensorData(
				p_as_resp.get("devid"),
				p_as_resp.get("ionum"),
	    		resp);

	    util::checkAttribute(xcp::attr::rslt, resp, "0");

	    if (resp.getAttribute(xcp::attr::port).getValues().size() != 1)
	         throw elw::lang::Exception(MKSTR("invalid port. size:"
	        		 << resp.getAttribute(xcp::attr::port).getValues().size()));
	     if (resp.getAttribute(xcp::attr::port).getValues()[0] != p_as_resp.get("port"))
	         throw elw::lang::Exception(MKSTR("invalid port. "
	         		"expected:"<< p_as_resp.get("port") <<
	         		 "received:" << resp.getAttribute(xcp::attr::port).getValues()[0]));

	     if (resp.getAttribute(xcp::attr::devid).getValues().size() != 1)
	         throw elw::lang::Exception(MKSTR("invalid devid. size:"
	        		 << resp.getAttribute(xcp::attr::devid).getValues().size()));
	     if (resp.getAttribute(xcp::attr::devid).getValues()[0] != p_as_resp.get("devid"))
	         throw elw::lang::Exception(MKSTR("invalid devid. "
	         		"expected:"<< p_as_resp.get("devid") <<
	         		 "received:" << resp.getAttribute(xcp::attr::devid).getValues()[0]));

	     if (resp.getAttribute(xcp::attr::ionum).getValues().size() != 1)
	         throw elw::lang::Exception(MKSTR("invalid ionum. size:"
	        		 << resp.getAttribute(xcp::attr::ionum).getValues().size()));
	     if (resp.getAttribute(xcp::attr::ionum).getValues()[0] != p_as_resp.get("ionum"))
	         throw elw::lang::Exception(MKSTR("invalid ionum. "
	         		"expected:"<< p_as_resp.get("ionum") <<
	         		 "received:" << resp.getAttribute(xcp::attr::ionum).getValues()[0]));

	    if (resp.getAttribute(7).getValues().size() != 1)
	        throw elw::lang::Exception(MKSTR("invalid value. size:"
	        		<< resp.getAttribute(7).getValues().size()));
	    if (resp.getAttribute(8).getValues().size() != 1)
	        throw elw::lang::Exception(MKSTR("invalid time. size:"
	        		<< resp.getAttribute(8).getValues().size()));
	}

