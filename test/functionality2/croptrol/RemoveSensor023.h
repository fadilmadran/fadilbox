/*
 * RemoveSensor023.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef RemoveSensor023_h__
#define RemoveSensor023_h__

class RemoveSensor023 :
    public ITest
{
public:
    std::string getDescription() const;
    void run();
};

#endif /* RemoveSensor023_h__ */
