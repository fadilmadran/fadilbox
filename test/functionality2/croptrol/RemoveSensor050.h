/*
 * RemoveSensor050.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef RemoveSensor050_h__
#define RemoveSensor050_h__

#include "util.h"

class RemoveSensor050 :
    public ITest
{
public:
    std::string getDescription() const;
    void run();
};

#endif /* RemoveSensor050_h__ */
