/*
 * GetSensorValue031.h
 *
 *  Created on: May 12, 2014
 *      Author: bulentk
 *
 */

#ifndef GetSensorValue031_h__
#define GetSensorValue031_h__

class GetSensorValue031 :
    public ITest
{
public:
    std::string getDescription() const;
    void run();
};

#endif /* GetSensorValue031_h__ */
