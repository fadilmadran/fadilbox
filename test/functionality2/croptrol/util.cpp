///*
// * AbstactTestcase.cpp
// *
// *  Created on: May 9, 2014
// *      Author: bulentk
// *
// */
//
//#include "stdafx.h"
//#include "util.h"
//#include "Dapp.h"
//
//namespace util {
//
//void checkAttribute(int i, xcp1::Message const& m, std::string const& val)
//{
//    if (m.getAttribute(i).getValues().empty())
//        throw elw::lang::Exception(MKSTR("Unexpected response. attr <" << i << "> is empty"));
//    if (m.getAttribute(i).getValues()[0] != val)
//        throw elw::lang::Exception(MKSTR("Unexpected response. attr <" << i << "> value is wrong. val:" << m.getAttribute(i).getValues()[0]));
//}
//
//bool isActive(std::list<elw::util::Properties> const& lst, std::string const& ionum)
//{
//    std::list<elw::util::Properties>::const_iterator it;
//    for (it = lst.begin(); it != lst.end(); ++it)
//    {
//        if ((*it).get("ionum") == ionum)
//        {
//            return (*it).get<int>("active") == 1;
//        }
//    }
//
//    throw elw::lang::Exception("Invalid ionum.");
//}
//
//bool isActive(std::string const& ionum)
//{
//    std::list<elw::util::Properties> lstFlash;
//    FlashFile::read<flash::List<flash::Properties> >("sensors", lstFlash);
//
//    return isActive(lstFlash, ionum);
//}
//
//bool isActive(std::vector<std::string> const& ionums)
//{
//    std::list<elw::util::Properties> lstFlash;
//    FlashFile::read<flash::List<flash::Properties> >("sensors", lstFlash);
//
//    std::vector<std::string>::const_iterator it;
//    for (it = ionums.begin(); it != ionums.end(); ++it)
//        if (!isActive(lstFlash, (*it)))
//            return false;
//    return true;
//}
//
//void reset()
//{
//    xcp1::Message req(xcp::msg::bact);
//    Dapp::instance().write2croptrol(req);
//
//    xcp1::Message resp;
//    Dapp::instance().getMessage(resp);
//
//    if (resp.getType() != xcp::msg::bact +1)
//        throw elw::lang::Exception(MKSTR("Unexpected response. type:" << resp.getType()));;
//
//    checkAttribute(xcp::attr::rslt, resp, "0");
//    checkAttribute(xcp::attr::hwaddr, resp, "255;0;1;0");
//}
//void bact()
//{
//	xcp1::Message req(xcp::msg::bact);
//	Dapp::instance().write2croptrol(req);
//
//	xcp1::Message resp;
//	Dapp::instance().getMessage(resp);
//
//	if (resp.getType() != xcp::msg::bact +1)
//		throw elw::lang::Exception(MKSTR("Unexpected response. type:" << resp.getType()));;
//
//    checkAttribute(xcp::attr::rslt, resp, "0");
//    checkAttribute(xcp::attr::hwaddr, resp, "255;0;8;0");
//}
//
//void  begindevice(	std::string const type,
//					xcp1::Message& resp,
//					elw::util::Properties& p)
//{
//	xcp1::Message req(xcp::msg::bd);
//
//	req.addAttribute( xcp1::Attribute(8).addValue(type) );
//
//	Dapp::instance().write2croptrol(req);
//
//	Dapp::instance().getMessage(resp);
//
//	if (resp.getType() != xcp::msg::bd +1)
//		throw elw::lang::Exception(MKSTR("Unexpected response. type:" << resp.getType()));;
//
//
//	if(resp.getAttribute(1).getValues()[0] == "0")
//	{
//		checkAttribute(xcp::attr::hwaddr, resp, "255;0;8;0");
//
//		p.set("port", resp.getAttribute(xcp::attr::port).getValues()[0]);
//		p.set("devid", resp.getAttribute(xcp::attr::devid).getValues()[0]);
//	}
//
//}
//
//void  removedevice(	std::string const& port,
//					std::string const& devid,
//					xcp1::Message& resp
//					)
//{
//	xcp1::Message req(xcp::msg::rd);
//
//	req.addAttribute(xcp1::Attribute(xcp::attr::port).addValue(port));
//	req.addAttribute(xcp1::Attribute(xcp::attr::devid).addValue(devid));
//
//	Dapp::instance().write2croptrol(req);
//
//	Dapp::instance().getMessage(resp);
//
//	if (resp.getType() != xcp::msg::rd +1)
//		throw elw::lang::Exception(MKSTR("Unexpected response. type:" << resp.getType()));;
//
//
//
//	checkAttribute(xcp::attr::hwaddr, resp, "255;0;8;0");
//
//
//}
//
//void enddevice()
//{
//	xcp1::Message req(xcp::msg::ed);
//
//	Dapp::instance().write2croptrol(req);
//
//	xcp1::Message resp;
//	Dapp::instance().getMessage(resp);
//
//	if (resp.getType() != xcp::msg::ed +1)
//	throw elw::lang::Exception(MKSTR("Unexpected response. type:" << resp.getType()));;
//
//	checkAttribute(xcp::attr::rslt, resp, "0");
//	checkAttribute(xcp::attr::hwaddr, resp, "255;0;8;0");
//
//}
//
//void eact()
//{
//	xcp1::Message req(xcp::msg::eact);
//	Dapp::instance().write2croptrol(req);
//
//	xcp1::Message resp;
//	Dapp::instance().getMessage(resp);
//
//	if (resp.getType() != xcp::msg::eact +1)
//		throw elw::lang::Exception(MKSTR("Unexpected response. type:" << resp.getType()));;
//
//    checkAttribute(xcp::attr::rslt, resp, "0");
//    checkAttribute(xcp::attr::hwaddr, resp, "255;0;8;0");
//}
//
//void addSensor(
//		std::string const port,
//		std::string const devid,
//		std::string const ionum,
//		std::string IP,
//		xcp1::Message& resp
//		)
//{
//    xcp1::Message req(xcp::msg::as);
//    req.addAttribute(xcp1::Attribute(xcp::attr::port).addValue(port) );
//    req.addAttribute(xcp1::Attribute(xcp::attr::devid).addValue(devid) );
//    req.addAttribute(xcp1::Attribute(xcp::attr::ionum).addValue(ionum) );
//    req.addAttribute(xcp1::Attribute(7).addValue(IP));
//
//
//    Dapp::instance().write2croptrol(req);
//
//    Dapp::instance().getMessage(resp);
//    if (resp.getType() != xcp::msg::as+1)
//        throw elw::lang::Exception(MKSTR("Unexpected response. type:" << resp.getType()));;
//
//    checkAttribute(xcp::attr::hwaddr, resp, "255;0;8;0");
//}
//
//void removeSensor(
//		std::string const devid,
//		std::string const ionum,
//		xcp1::Message& resp
//		)
//{
//    xcp1::Message req(xcp::msg::rs);
//    req.addAttribute(xcp1::Attribute(xcp::attr::devid).addValue(devid) );
//    req.addAttribute(xcp1::Attribute(xcp::attr::ionum).addValue(ionum) );
//
//    Dapp::instance().write2croptrol(req);
//
//    Dapp::instance().getMessage(resp);
//    if (resp.getType() != xcp::msg::rs+1)
//        throw elw::lang::Exception(MKSTR("Unexpected response. type:" << resp.getType()));;
//
//    checkAttribute(xcp::attr::hwaddr, resp, "255;0;8;0");
//}
//
//void getSensorData(
//		std::string const devid,
//		std::string const ionum,
//		xcp1::Message& resp
//		)
//	{
//		xcp1::Message req(xcp::msg::gsd);
//	    req.addAttribute(xcp1::Attribute(xcp::attr::devid).addValue(devid));
//	    req.addAttribute(xcp1::Attribute(xcp::attr::ionum).addValue(ionum));
//
//		Dapp::instance().write2croptrol(req);
//
//		Dapp::instance().getMessage(resp);
//		if (resp.getType() != xcp::msg::gsd+1)
//			throw elw::lang::Exception(MKSTR("Unexpected response. type:" << resp.getType()));;
//
//		checkAttribute(xcp::attr::hwaddr, resp, "255;0;8;0");
//	}
//
//
//
//
////void sensorAtAlarm(std::string const& ionum, xcp1::Message& resp)
////{
////    xcp1::Message req(xcp::msg::gsa);
////    req.addAttribute(xcp1::Attribute(3).addValue("0"));
////    req.addAttribute(xcp1::Attribute(4).addValue("0"));
////    req.addAttribute(xcp1::Attribute(5).addValue(ionum));
////
////    Dapp::instance().write2croptrol(req);
////
////    Dapp::instance().getMessage(resp, 2000);
////    if (resp.getType() != xcp::msg::gsa+1)
////        throw elw::lang::Exception(MKSTR("Unexpected response. type:" << resp.getType()));;
////
////    checkAttribute(xcp::attr::hwaddr, resp, "255;0;1;0");
////}
//
//void changeSensorStatus(
//	int  port,
//	int  devid,
//	int  ionum,
//	std::string const& status,
//	xcp1::Message& resp
//	)
//{
//	xcp1::Message req(xcp::msg::css);
//	req.addAttribute(xcp1::Attribute(xcp::attr::port).addValue(MKSTR(port)));
//	req.addAttribute(xcp1::Attribute(xcp::attr::devid).addValue(MKSTR(devid)));
//	req.addAttribute(xcp1::Attribute(xcp::attr::ionum).addValue(MKSTR(ionum)));
//	req.addAttribute(xcp1::Attribute(7).addValue(status));
//
//	Dapp::instance().write2croptrol(req);
//
//	Dapp::instance().getMessage(resp);
//	if (resp.getType() != xcp::msg::css+1)
//		throw elw::lang::Exception(MKSTR("Unexpected response. type:" << resp.getType()));;
//
//	checkAttribute(xcp::attr::hwaddr, resp, "255;0;4;0");
//}
//
////void waitForNewSensorData(elw::util::Properties& p,
////		dword dwWait,
////		int port,
////    	int  did,
////    	int io)
////{
////    xcp1::Message m;
////    Dapp::instance().getMessage(m, dwWait);
////
////    if (m.getType() != xcp::msg::nsd)
////        throw elw::lang::Exception(MKSTR("Unexpected response. type:" << m.getType()));;
////
////    checkAttribute(xcp::attr::hwaddr, m, "255;0;8;0");
////    checkAttribute(xcp::attr::port, m, MKSTR(port));
////    checkAttribute(xcp::attr::devid, m, MKSTR(did));
////    checkAttribute(xcp::attr::ionum, m, MKSTR(io));
////
////    if (m.getAttribute(7).getValues().size() != 1)
////        throw elw::lang::Exception(MKSTR("Invalid value attr. size:" << m.getAttribute(7).getValues().size()));
////    if (m.getAttribute(8).getValues().size() != 1)
////        throw elw::lang::Exception(MKSTR("Invalid time attr. size:" << m.getAttribute(8).getValues().size()));
////
////    p.set("ionum", m.getAttribute(xcp::attr::ionum).getValues()[0]);
////    p.set("value", m.getAttribute(7).getValues()[0]);
////    p.set("time", m.getAttribute(8).getValues()[0]);
////}
//
//void waitForNewSensorData(elw::util::Properties& p,
//		dword dwWait)
//{
//    xcp1::Message m;
//    Dapp::instance().getMessage(m, dwWait);
//
//    if (m.getType() != xcp::msg::nsd)
//        throw elw::lang::Exception(MKSTR("Unexpected response. type:" << m.getType()));;
//
//    checkAttribute(xcp::attr::hwaddr, m, "255;0;8;0");
//
//    if (m.getAttribute(7).getValues().size() != 1)
//        throw elw::lang::Exception(MKSTR("Invalid value attr. size:" << m.getAttribute(7).getValues().size()));
//    if (m.getAttribute(8).getValues().size() != 1)
//        throw elw::lang::Exception(MKSTR("Invalid time attr. size:" << m.getAttribute(8).getValues().size()));
//
//    p.set("port", m.getAttribute(xcp::attr::port).getValues()[0]);
//    p.set("devid", m.getAttribute(xcp::attr::devid).getValues()[0]);
//    p.set("ionum", m.getAttribute(xcp::attr::ionum).getValues()[0]);
//
//}
//
//std::vector<sDevice> getDeviceList ()
//{
//	std::vector<sDevice> lstAdded;
//
//	sDevice ping;
//	ping.devType = MKSTR( conf::dev::PING );
//
//	ping.ip.set("0", "192.168.1.182");
//	ping.ip.set("1", "192.168.1.136");
//	ping.ip.set("2", "192.168.1.33");
//
//	ping.max_ionum = 3;
//
//	lstAdded.push_back(ping);
//
//	sDevice telnet;
//	telnet.devType = MKSTR( conf::dev::TELNET );
//
//	telnet.ip.set("0", "192.168.1.182");
//	telnet.ip.set("1", "192.168.1.223");
//
//
//	telnet.max_ionum = 2;
//
//	lstAdded.push_back(telnet);
//
//	return lstAdded;
//}
//
//std::vector<sDevice> getDeviceList  (std::string devType)
//{
//	std::vector<sDevice> lstAdded;
//
//	if (devType == "PING")
//	{
//		sDevice ping;
//		ping.devType = MKSTR( conf::dev::PING );
//
//		ping.ip.set("0", "192.168.1.182");
//		ping.ip.set("1", "192.168.1.136");
//		ping.ip.set("2", "192.168.1.33");
//
//		ping.max_ionum = 3;
//
//		lstAdded.push_back(ping);
//	}
//
//	if (devType == "TELNET")
//	{
//		sDevice telnet;
//		telnet.devType = MKSTR( conf::dev::TELNET );
//
//		telnet.ip.set("0", "192.168.1.182");
//		telnet.ip.set("1", "192.168.1.223");
//
//
//		telnet.max_ionum = 2;
//
//		lstAdded.push_back(telnet);
//	}
//
//	return lstAdded;
//}
//
//}
//
//
