/*
 * BeginDevice013.cpp
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#include "stdafx.h"
#include "Dapp.h"
#include "BeginDevice013.h"
#include "util.h"

std::string BeginDevice013::getDescription() const
{
    return "MonSrvr BeginDevice request. IN RUNNING type:PING ";
}

void BeginDevice013::run()
{
	Dapp::instance().reset();

    util::bact();
	util::eact();

	std::string type = MKSTR (conf::dev::PING ) ;
    elw::util::Properties p_bd_resp;
    xcp1::Message resp;
    util::begindevice(	type,
    					resp,
    					p_bd_resp);

    util::checkAttribute(xcp::attr::rslt, resp, "0"); // success


	util::enddevice( );



}
