/*
 * BeginDevice013.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef BeginDevice013_h__
#define BeginDevice013_h__

class BeginDevice013 :
    public ITest
{
public:
    std::string getDescription() const;
    void run();
    void run(xcp1::Message& resp);
    void run(elw::util::Properties& prop);
};

#endif /* BeginDevice013_h__ */
