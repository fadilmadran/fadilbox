/*
 * AddSensor030.cpp
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#include "stdafx.h"
#include "Dapp.h"
#include "AddSensor030.h"


std::string AddSensor030::getDescription() const
{
    return "MonSrvr AS request. type:PING, IP:192.168.1.182, ionum:0 "
    		"IP:192.168.1.136, ionum:1 "
    		"IP:192.168.1.33, ionum:2 ";
}

void AddSensor030::run()
{
	Dapp::instance().reset();
    util::bact();

	std::vector<util::sDevice> lstAdded = util::getDeviceList("PING");

	elw::util::Properties p_bd_resp;
	std::vector<util::sDevice>::iterator it;
	for (it = lstAdded.begin() ; it != lstAdded.end() ; ++it)
	{
	    elw::util::Properties p_bd_resp;
	    xcp1::Message resp;
	    util::begindevice(	(*it).devType,
	    					resp,
	    					p_bd_resp);

		util::checkAttribute(xcp::attr::rslt, resp, "0"); // success

		(*it).devid=p_bd_resp.get("devid");
		(*it).port=p_bd_resp.get("port");

		std::list<std::string> tempKeys = (*it).ip.getKeys();
		std::list<std::string>::iterator it2;
		for(it2 = tempKeys.begin(); it2 != tempKeys.end(); it2++)
		{

			std::string IP="192.168.1.182";
		    util::addSensor(
		    		(*it).port,
		    		(*it).devid ,
		    		(*it2).data(),
		    		(*it).ip.get((*it2).data()),
		    		resp);

			util::checkAttribute(xcp::attr::rslt, resp, "0"); // success

			if (resp.getAttribute(xcp::attr::port).getValues().size() != 1)
					throw elw::lang::Exception(MKSTR("invalid port. size:"
							<< resp.getAttribute(xcp::attr::port).getValues().size()));
			if (resp.getAttribute(xcp::attr::port).getValues()[0] != (*it).port )
				throw elw::lang::Exception(MKSTR("invalid port. "
						"expected:" << (*it).port <<
						"received:" << resp.getAttribute(xcp::attr::port).getValues()[0]));

			if (resp.getAttribute(xcp::attr::devid).getValues().size() != 1)
				throw elw::lang::Exception(MKSTR("invalid devid. size:"
						<< resp.getAttribute(xcp::attr::devid).getValues().size()));
			if (resp.getAttribute(xcp::attr::devid).getValues()[0] != (*it).devid)
				throw elw::lang::Exception(MKSTR("invalid devid. "
						"expected:" << (*it).devid <<
						"received:" << resp.getAttribute(xcp::attr::devid).getValues()[0]));

			if (resp.getAttribute(xcp::attr::ionum).getValues().size() != 1)
				throw elw::lang::Exception(MKSTR("invalid ionum. size:"
						<< resp.getAttribute(xcp::attr::ionum).getValues().size()));
			if (resp.getAttribute(xcp::attr::ionum).getValues()[0] != (*it2).data())
				throw elw::lang::Exception(MKSTR("invalid ionum. "
						"expected:" << (*it2).data() <<
						"received:" << resp.getAttribute(xcp::attr::ionum).getValues()[0]));
		}


		util::enddevice();
	}

	util::eact();

	std::vector<util::sDevice> lstAdded_Temp = lstAdded;

	while(lstAdded.empty() != 1)
	{
	    elw::util::Properties p_wd_resp;
		util::waitForNewSensorData(p_wd_resp,20000);


		for (it = lstAdded.begin() ; it != lstAdded.end() ; ++it) // check ionum
		{
			if( (*it).port == p_wd_resp.get("port") &&
					(*it).devid == p_wd_resp.get("devid") &&
					(*it).ip.hasKey(p_wd_resp.get("ionum")) )
			{
				(*it).ip.remove(p_wd_resp.get("ionum"));
			}

		}

		for (it = lstAdded.begin() ; it != lstAdded.end() ; ++it) // check device ionum list empty
		{
			std::list<std::string> tempKeysss = (*it).ip.getKeys();
			if(tempKeysss.empty())
				break;
		}

		if(it != lstAdded.end())
		{
			lstAdded.erase (it);
		}
	}

	lstAdded = lstAdded_Temp;
}

void AddSensor030::run(std::vector<util::sDevice>& lstAdded)
{
	Dapp::instance().reset();
    util::bact();


	elw::util::Properties p_bd_resp;
	std::vector<util::sDevice>::iterator it;
	for (it = lstAdded.begin() ; it != lstAdded.end() ; ++it)
	{

	    elw::util::Properties p_bd_resp;
	    xcp1::Message resp;
	    util::begindevice(	(*it).devType,
	    					resp,
	    					p_bd_resp);

		util::checkAttribute(xcp::attr::rslt, resp, "0"); // success

		(*it).devid=p_bd_resp.get("devid");
		(*it).port=p_bd_resp.get("port");

		std::list<std::string> tempKeys = (*it).ip.getKeys();
		std::list<std::string>::iterator it2;
		for(it2 = tempKeys.begin(); it2 != tempKeys.end(); it2++)
		{

			std::string IP="192.168.1.182";
		    util::addSensor(
		    		(*it).port,
		    		(*it).devid ,
		    		(*it2).data(),
		    		(*it).ip.get((*it2).data()),
		    		resp);

			util::checkAttribute(xcp::attr::rslt, resp, "0"); // success

			if (resp.getAttribute(xcp::attr::port).getValues().size() != 1)
					throw elw::lang::Exception(MKSTR("invalid port. size:"
							<< resp.getAttribute(xcp::attr::port).getValues().size()));
			if (resp.getAttribute(xcp::attr::port).getValues()[0] != (*it).port )
				throw elw::lang::Exception(MKSTR("invalid port. "
						"expected:" << (*it).port <<
						"received:" << resp.getAttribute(xcp::attr::port).getValues()[0]));

			if (resp.getAttribute(xcp::attr::devid).getValues().size() != 1)
				throw elw::lang::Exception(MKSTR("invalid devid. size:"
						<< resp.getAttribute(xcp::attr::devid).getValues().size()));
			if (resp.getAttribute(xcp::attr::devid).getValues()[0] != (*it).devid)
				throw elw::lang::Exception(MKSTR("invalid devid. "
						"expected:" << (*it).devid <<
						"received:" << resp.getAttribute(xcp::attr::devid).getValues()[0]));

			if (resp.getAttribute(xcp::attr::ionum).getValues().size() != 1)
				throw elw::lang::Exception(MKSTR("invalid ionum. size:"
						<< resp.getAttribute(xcp::attr::ionum).getValues().size()));
			if (resp.getAttribute(xcp::attr::ionum).getValues()[0] != (*it2).data())
				throw elw::lang::Exception(MKSTR("invalid ionum. "
						"expected:" << (*it2).data() <<
						"received:" << resp.getAttribute(xcp::attr::ionum).getValues()[0]));
		}


		util::enddevice();
	}

	util::eact();

	std::vector<util::sDevice> lstAdded_Temp = lstAdded;

	while(lstAdded.empty() != 1)
	{
	    elw::util::Properties p_wd_resp;
		util::waitForNewSensorData(p_wd_resp,20000);


		for (it = lstAdded.begin() ; it != lstAdded.end() ; ++it) // check ionum
		{
			if( (*it).port == p_wd_resp.get("port") &&
					(*it).devid == p_wd_resp.get("devid") &&
					(*it).ip.hasKey(p_wd_resp.get("ionum")) )
			{
				(*it).ip.remove(p_wd_resp.get("ionum"));
			}

		}

		for (it = lstAdded.begin() ; it != lstAdded.end() ; ++it) // check device ionum list empty
		{
			std::list<std::string> tempKeysss = (*it).ip.getKeys();
			if(tempKeysss.empty())
				break;
		}

		if(it != lstAdded.end())
		{
			lstAdded.erase (it);
		}
	}

	lstAdded = lstAdded_Temp;
}
