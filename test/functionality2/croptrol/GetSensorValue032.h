/*
 * GetSensorValue032.h
 *
 *  Created on: May 12, 2014
 *      Author: bulentk
 *
 */

#ifndef GetSensorValue032_h__
#define GetSensorValue032_h__

class GetSensorValue032 :
    public ITest
{
public:
    std::string getDescription() const;
    void run();
};

#endif /* GetSensorValue032_h__ */
