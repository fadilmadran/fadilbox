/*
 * AddSensor080.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef AddSensor080_h__
#define AddSensor080_h__


#include "util.h"

class AddSensor080 :
    public ITest
{
public:
    std::string getDescription() const;
    void run();
    void run(xcp1::Message& resp);
    void run(elw::util::Properties& p);
    void run(std::vector<util::sDevice>& lstAdded);
};

#endif /* AddSensor080_h__ */
