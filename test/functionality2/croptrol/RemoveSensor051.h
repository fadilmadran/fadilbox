/*
 * RemoveSensor051.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef RemoveSensor051_h__
#define RemoveSensor051_h__

#include "util.h"

class RemoveSensor051 :
    public ITest
{
public:
    std::string getDescription() const;
    void run();
};

#endif /* RemoveSensor051_h__ */
