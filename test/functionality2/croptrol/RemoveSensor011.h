/*
 * RemoveSensor011.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef RemoveSensor011_h__
#define RemoveSensor011_h__

class RemoveSensor011 :
    public ITest
{
public:
    std::string getDescription() const;
    void run();
};

#endif /* RemoveSensor011_h__ */
