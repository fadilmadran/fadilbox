/*
 * GetSensorValue052.h
 *
 *  Created on: May 12, 2014
 *      Author: bulentk
 *
 */

#ifndef GetSensorValue052_h__
#define GetSensorValue052_h__

#include "util.h"

class GetSensorValue052 :
    public ITest
{
public:
    std::string getDescription() const;
    void run();
};

#endif /* GetSensorValue052_h__ */
