/*
 * RemoveDevice010.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef RemoveDevice010_h__
#define RemoveDevice010_h__

class RemoveDevice010 :
    public ITest
{
public:
    std::string getDescription() const;
    void run();
};

#endif /* RemoveDevice010_h__ */
