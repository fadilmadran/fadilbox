/*
 * AddSensor032.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef AddSensor032_h__
#define AddSensor032_h__

class AddSensor032 :
    public ITest
{
public:
    std::string getDescription() const;
    void run();
    void run(xcp1::Message& resp);
    void run(elw::util::Properties& prop);
};

#endif /* AddSensor032_h__ */
