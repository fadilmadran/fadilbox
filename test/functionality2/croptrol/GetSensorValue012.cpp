/*
 * GetSensorValue012.cpp
 *
 *  Created on: May 12, 2014
 *      Author: bulentk
 *
 */

#include "stdafx.h"
#include "Dapp.h"
#include "GetSensorValue012.h"
#include "util.h"
#include "AddSensor010.h"


std::string GetSensorValue012::getDescription() const
{
    return "MonSrvr GSD request. INVALID IONUM type:PING, IP:192.168.1.182, ionum:0";
}

void GetSensorValue012::run()
{
	elw::util::Properties p_as_resp;
	AddSensor010 add;
	add.run(p_as_resp);

	xcp1::Message resp;
	util::getSensorData(
			p_as_resp.get("devid"),
			MKSTR( p_as_resp.get<int>("ionum") + 2 ),
			resp);

	util::checkAttribute(xcp::attr::rslt, resp, "1"); // success

	if (resp.getAttribute(xcp::attr::error).getValues().size() != 1)
		throw elw::lang::Exception(MKSTR("invalid err desc. size:"
				<< resp.getAttribute(xcp::attr::error).getValues().size()));
	if (resp.getAttribute(xcp::attr::error).getValues()[0] != MKSTR(xcp::attr::ionum))
		throw elw::lang::Exception(MKSTR("invalid err desc. expected:"
				<< xcp::attr::ionum << " received:"
				<< resp.getAttribute(xcp::attr::error).getValues()[0]));
}
