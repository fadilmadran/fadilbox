/*
 * RemoveSensor052.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef RemoveSensor052_h__
#define RemoveSensor052_h__

#include "util.h"

class RemoveSensor052 :
    public ITest
{
public:
    std::string getDescription() const;
    void run();
};

#endif /* RemoveSensor052_h__ */
