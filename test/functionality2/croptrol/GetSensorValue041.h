/*
 * GetSensorValue041.h
 *
 *  Created on: May 12, 2014
 *      Author: bulentk
 *
 */

#ifndef GetSensorValue041_h__
#define GetSensorValue041_h__

class GetSensorValue041 :
    public ITest
{
public:
    std::string getDescription() const;
    void run();
};

#endif /* GetSensorValue041_h__ */
