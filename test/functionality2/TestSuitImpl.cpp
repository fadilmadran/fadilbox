/*
 * TestSuitImpl.cpp
 *
 *  Created on: May 8, 2014
 *      Author: bulentk
 *
 */

#include "stdafx.h"
#include "TestSuitImpl.h"
#include "Dapp.h"
#include "testcases.h"

TestSuitImpl::TestSuitImpl()
{
	elw::lang::ClassFactory::Registrar<ITest, BeginDevice010>("croptrol1");
	elw::lang::ClassFactory::Registrar<ITest, BeginDevice011>("croptrol2");
	elw::lang::ClassFactory::Registrar<ITest, BeginDevice012>("croptrol3");

	elw::lang::ClassFactory::Registrar<ITest, BeginDevice013>("croptrol4");
	elw::lang::ClassFactory::Registrar<ITest, BeginDevice014>("croptrol5");
	elw::lang::ClassFactory::Registrar<ITest, BeginDevice015>("croptrol6");
	elw::lang::ClassFactory::Registrar<ITest, BeginDevice016>("croptrol7");

	elw::lang::ClassFactory::Registrar<ITest, AddSensor010>("croptrol4");
	elw::lang::ClassFactory::Registrar<ITest, AddSensor011>("croptrol5");
	elw::lang::ClassFactory::Registrar<ITest, AddSensor012>("croptrol6");
	elw::lang::ClassFactory::Registrar<ITest, AddSensor013>("croptrol7");
	elw::lang::ClassFactory::Registrar<ITest, AddSensor014>("croptrol8");

	elw::lang::ClassFactory::Registrar<ITest, AddSensor020>("croptrol9");
	elw::lang::ClassFactory::Registrar<ITest, AddSensor021>("croptrol10");
	elw::lang::ClassFactory::Registrar<ITest, AddSensor022>("croptrol11");
	elw::lang::ClassFactory::Registrar<ITest, AddSensor023>("croptrol12");
	elw::lang::ClassFactory::Registrar<ITest, AddSensor024>("croptrol13");
	elw::lang::ClassFactory::Registrar<ITest, AddSensor025>("croptrol14");

	elw::lang::ClassFactory::Registrar<ITest, AddSensor030>("croptrol15");
	elw::lang::ClassFactory::Registrar<ITest, AddSensor031>("croptrol16");
	elw::lang::ClassFactory::Registrar<ITest, AddSensor032>("croptrol17");

	elw::lang::ClassFactory::Registrar<ITest, AddSensor040>("croptrol18");
	elw::lang::ClassFactory::Registrar<ITest, AddSensor041>("croptrol19");
	elw::lang::ClassFactory::Registrar<ITest, AddSensor042>("croptrol20");

	elw::lang::ClassFactory::Registrar<ITest, AddSensor050>("croptrol21");
	elw::lang::ClassFactory::Registrar<ITest, AddSensor051>("croptrol22");
	elw::lang::ClassFactory::Registrar<ITest, AddSensor052>("croptrol23");

	elw::lang::ClassFactory::Registrar<ITest, AddSensor060>("croptrol24");
	elw::lang::ClassFactory::Registrar<ITest, AddSensor061>("croptrol25");
	elw::lang::ClassFactory::Registrar<ITest, AddSensor062>("croptrol26");

	elw::lang::ClassFactory::Registrar<ITest, AddSensor070>("croptrol27");
	elw::lang::ClassFactory::Registrar<ITest, AddSensor071>("croptrol28");
	elw::lang::ClassFactory::Registrar<ITest, AddSensor072>("croptrol29");

	elw::lang::ClassFactory::Registrar<ITest, AddSensor080>("croptrol30");
	elw::lang::ClassFactory::Registrar<ITest, AddSensor081>("croptrol31");

	elw::lang::ClassFactory::Registrar<ITest, RemoveSensor010>("croptrol32");
	elw::lang::ClassFactory::Registrar<ITest, RemoveSensor011>("croptrol33");
	elw::lang::ClassFactory::Registrar<ITest, RemoveSensor012>("croptrol34");

	elw::lang::ClassFactory::Registrar<ITest, RemoveSensor020>("croptrol35");
	elw::lang::ClassFactory::Registrar<ITest, RemoveSensor021>("croptrol36");
	elw::lang::ClassFactory::Registrar<ITest, RemoveSensor022>("croptrol37");
	elw::lang::ClassFactory::Registrar<ITest, RemoveSensor023>("croptrol38");

	elw::lang::ClassFactory::Registrar<ITest, RemoveSensor030>("croptrol39");
	elw::lang::ClassFactory::Registrar<ITest, RemoveSensor031>("croptrol40");
	elw::lang::ClassFactory::Registrar<ITest, RemoveSensor032>("croptrol41");

	elw::lang::ClassFactory::Registrar<ITest, RemoveSensor040>("croptrol42");
	elw::lang::ClassFactory::Registrar<ITest, RemoveSensor041>("croptrol43");
	elw::lang::ClassFactory::Registrar<ITest, RemoveSensor042>("croptrol44");

	elw::lang::ClassFactory::Registrar<ITest, RemoveSensor050>("croptrol45");
	elw::lang::ClassFactory::Registrar<ITest, RemoveSensor051>("croptrol46");
	elw::lang::ClassFactory::Registrar<ITest, RemoveSensor052>("croptrol47");

	elw::lang::ClassFactory::Registrar<ITest, GetSensorValue010>("croptrol48");
	elw::lang::ClassFactory::Registrar<ITest, GetSensorValue011>("croptrol49");
	elw::lang::ClassFactory::Registrar<ITest, GetSensorValue012>("croptrol50");

	elw::lang::ClassFactory::Registrar<ITest, GetSensorValue020>("croptrol51");
	elw::lang::ClassFactory::Registrar<ITest, GetSensorValue021>("croptrol52");
	elw::lang::ClassFactory::Registrar<ITest, GetSensorValue022>("croptrol53");
	elw::lang::ClassFactory::Registrar<ITest, GetSensorValue023>("croptrol54");

	elw::lang::ClassFactory::Registrar<ITest, GetSensorValue030>("croptrol55");
	elw::lang::ClassFactory::Registrar<ITest, GetSensorValue031>("croptrol56");
	elw::lang::ClassFactory::Registrar<ITest, GetSensorValue032>("croptrol57");

	elw::lang::ClassFactory::Registrar<ITest, GetSensorValue040>("croptrol58");
	elw::lang::ClassFactory::Registrar<ITest, GetSensorValue041>("croptrol59");
	elw::lang::ClassFactory::Registrar<ITest, GetSensorValue042>("croptrol60");

	elw::lang::ClassFactory::Registrar<ITest, GetSensorValue050>("croptrol61");
	elw::lang::ClassFactory::Registrar<ITest, GetSensorValue051>("croptrol62");
	elw::lang::ClassFactory::Registrar<ITest, GetSensorValue052>("croptrol63");

	elw::lang::ClassFactory::Registrar<ITest, RemoveDevice010>("croptrol64");
	elw::lang::ClassFactory::Registrar<ITest, RemoveDevice011>("croptrol65");
	elw::lang::ClassFactory::Registrar<ITest, RemoveDevice012>("croptrol66");
}

TestSuitImpl::~TestSuitImpl()
{
//    Dapp::release();
}

void TestSuitImpl::getScopes(std::vector<std::string>& lst)
{
    lst.push_back("croptrol");
    lst.push_back("discovery");
}

REGISTER_CLASS("TestSuit", ITestSuit, TestSuitImpl);
