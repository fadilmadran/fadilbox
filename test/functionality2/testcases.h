/*
 * testcases.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef testcases_h__
#define testcases_h__


#include "croptrol/BeginDevice010.h"
#include "croptrol/BeginDevice011.h"
#include "croptrol/BeginDevice012.h"
#include "croptrol/BeginDevice013.h"
#include "croptrol/BeginDevice014.h"
#include "croptrol/BeginDevice015.h"
#include "croptrol/BeginDevice016.h"

#include "croptrol/AddSensor010.h"
#include "croptrol/AddSensor011.h"
#include "croptrol/AddSensor012.h"
#include "croptrol/AddSensor013.h"
#include "croptrol/AddSensor014.h"

#include "croptrol/AddSensor020.h"
#include "croptrol/AddSensor021.h"
#include "croptrol/AddSensor022.h"
#include "croptrol/AddSensor023.h"
#include "croptrol/AddSensor024.h"
#include "croptrol/AddSensor025.h"

#include "croptrol/AddSensor030.h"
#include "croptrol/AddSensor031.h"
#include "croptrol/AddSensor032.h"

#include "croptrol/AddSensor040.h"
#include "croptrol/AddSensor041.h"
#include "croptrol/AddSensor042.h"

#include "croptrol/AddSensor050.h"
#include "croptrol/AddSensor051.h"
#include "croptrol/AddSensor052.h"

#include "croptrol/AddSensor060.h"
#include "croptrol/AddSensor061.h"
#include "croptrol/AddSensor062.h"

#include "croptrol/AddSensor070.h"
#include "croptrol/AddSensor071.h"
#include "croptrol/AddSensor072.h"

#include "croptrol/AddSensor080.h"
#include "croptrol/AddSensor081.h"

#include "croptrol/RemoveSensor010.h"
#include "croptrol/RemoveSensor011.h"
#include "croptrol/RemoveSensor012.h"

#include "croptrol/RemoveSensor020.h"
#include "croptrol/RemoveSensor021.h"
#include "croptrol/RemoveSensor022.h"
#include "croptrol/RemoveSensor023.h"

#include "croptrol/RemoveSensor030.h"
#include "croptrol/RemoveSensor031.h"
#include "croptrol/RemoveSensor032.h"

#include "croptrol/RemoveSensor040.h"
#include "croptrol/RemoveSensor041.h"
#include "croptrol/RemoveSensor042.h"

#include "croptrol/RemoveSensor050.h"
#include "croptrol/RemoveSensor051.h"
#include "croptrol/RemoveSensor052.h"

#include "croptrol/GetSensorValue010.h"
#include "croptrol/GetSensorValue011.h"
#include "croptrol/GetSensorValue012.h"

#include "croptrol/GetSensorValue020.h"
#include "croptrol/GetSensorValue021.h"
#include "croptrol/GetSensorValue022.h"
#include "croptrol/GetSensorValue023.h"

#include "croptrol/GetSensorValue030.h"
#include "croptrol/GetSensorValue031.h"
#include "croptrol/GetSensorValue032.h"

#include "croptrol/GetSensorValue040.h"
#include "croptrol/GetSensorValue041.h"
#include "croptrol/GetSensorValue042.h"

#include "croptrol/GetSensorValue050.h"
#include "croptrol/GetSensorValue051.h"
#include "croptrol/GetSensorValue052.h"

#include "croptrol/RemoveDevice010.h"
#include "croptrol/RemoveDevice011.h"
#include "croptrol/RemoveDevice012.h"

#endif /* testcases_h__ */
