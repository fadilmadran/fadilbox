/*
 * Dapp.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef Dapp_h__
#define Dapp_h__


class WriterImpl : public IWriter
{
    elw::thread::ThreadQueue<xcp1::Message> m_queue;
public:
    void write(
        xcp1::Message const& msg
        )
    {
        std::cout << "RECV " << msg << std::endl;
        m_queue.push(msg);
    }

    void getMessage(xcp1::Message& m, dword dwTimeout)
    {
        dword dw0 = elw::thread::tick();
        while (!m_queue.peek(m))
        {
            elw::thread::sleep(10);
            if (elw::thread::tick()-dw0 >= dwTimeout)
                throw elw::lang::Exception("receive timeout");
        }
    }

    void clear()
    {
        m_queue.clear();
    }
};

class Dapp
{
    static elw::lang::SmartPtr<Dapp> m_ptr;
public:
    static Dapp& instance();
    static void release();

private:
    elw::sys::SharedLibrary m_dapp;

    elw::lang::SmartPtr<IDeviceApplication> m_ptrApp;
    elw::lang::SmartPtr<WriterImpl> m_ptrWriter;

public:
    void start();
    void stop();
    void reset();

    void getMessage(xcp1::Message& m, dword dwTimeout = 2000);
    void clearWaitingMessages();

    void write2croptrol(xcp1::Message const& m);
    void write2discovery(xcp1::Message const& m);

protected:
    Dapp();
};

#endif /* Dapp_h__ */
