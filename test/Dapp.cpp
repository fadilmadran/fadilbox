/*
 * Dapp.cpp
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#include "stdafx.h"
#include "Dapp.h"

extern "C" {
IDeviceApplication* create(int argc, char* argv[]);
}

elw::lang::SmartPtr<Dapp> Dapp::m_ptr;

Dapp& Dapp::instance()
{
	ENTEREXIT0();
    if (m_ptr == NULL)
        m_ptr = new Dapp();
    return *m_ptr;
}

void Dapp::release()
{
	ENTEREXIT0();
    if (m_ptr != NULL)
        m_ptr->stop();
    m_ptr = NULL;
}

Dapp::Dapp()
{
	ENTEREXIT0();

    __g_uDevBoxLogID__ = LOGGER().newModule(
            "monsrvr",
            0
            );

	LOGGER().addTarget("logmngr::target::stdout");

//    LOGGER().setModuleLevel(__g_uDevBoxLogID__,255);


}

void Dapp::reset()
{
	ENTEREXIT0();
	stop();
	start();
}

void Dapp::start()
{
	ENTEREXIT0();

	if (m_ptrApp != NULL)
		stop();

    m_ptrWriter = new WriterImpl();

    char* argv[] = {
            "monsrvr"
            };
	m_ptrApp = create(1, argv);

    ICommunicationHandler& c = m_ptrApp->getCroptrolHandler();
    c.setWriter(m_ptrWriter.ptr());
    ICommunicationHandler& d = m_ptrApp->getDiscoveryHandler();
    d.setWriter(m_ptrWriter.ptr());

    m_ptrApp->start();
	clearWaitingMessages();
}

void Dapp::stop()
{
	ENTEREXIT0();
	clearWaitingMessages();
	if (m_ptrApp != NULL)
		m_ptrApp->stop();

	m_ptrApp = NULL;
}

void Dapp::clearWaitingMessages()
{
	ENTEREXIT0();
    m_ptrWriter->clear();
}

void Dapp::getMessage(xcp1::Message& m, dword dw)
{
	ENTEREXIT0();
    m_ptrWriter->getMessage(m, dw);
}

void Dapp::write2croptrol(xcp1::Message const& m)
{
	ENTEREXIT0();
    std::cout << "SEND " << m << std::endl;
    m_ptrApp->getCroptrolHandler().onMessage(m);
}

void Dapp::isQueueEmpty()
{
	ENTEREXIT0();
	elw::thread::sleep(1000);
	if (!m_ptrWriter->isQueueEmpty())
    throw elw::lang::Exception(MKSTR("Message Queue is not Empty"));

}

void Dapp::write2discovery(xcp1::Message const& m)
{
    m_ptrApp->getDiscoveryHandler().onMessage(m);
}
