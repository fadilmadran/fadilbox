/*
 * AbstactTestcase.cpp
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#include "stdafx.h"
#include "util.h"
#include "Dapp.h"

namespace util {

std::string const& phsyHwaddrTest 	= "255;0;8;0" ;
std::string const& processName 		= "fadilbox" ;

void waitForNewSensorData(
		xcp1::Message& resp,
		dword dwWait)
{
    Dapp::instance().getMessage(resp, dwWait);

    checkType(xcp::msg::nsd,resp);

    checkAttribute(xcp::attr::hwaddr, resp, phsyHwaddrTest);

    if (resp.getAttribute(7).getValues().size() != 1)
        throw elw::lang::Exception(MKSTR("Invalid value attr. size:" << resp.getAttribute(7).getValues().size()));
    if (resp.getAttribute(8).getValues().size() != 1)
        throw elw::lang::Exception(MKSTR("Invalid time attr. size:" << resp.getAttribute(8).getValues().size()));
}

void checkAttribute(
		int i,
		xcp1::Message const& m,
		std::string const& val
		)
{
//	std::cout << "i: " << i << " m.getType: " << m.getType() << " val: " << val <<  std::endl;

    if (m.getAttribute(i).getValues().empty())
        throw elw::lang::Exception(MKSTR("Unexpected response. attr <" << i << "> is empty"));
    if (m.getAttribute(i).getValues()[0] != val)
        throw elw::lang::Exception(MKSTR("Unexpected response. attr <" << i << "> value is wrong. val:" << m.getAttribute(i).getValues()[0]));
}

void checkType(
		int i,
		xcp1::Message const& m
		)
{
//	std::cout << "i: " << i << " m.getType: " << m.getType() << std::endl;


	if (m.getType() != i)
		throw elw::lang::Exception(MKSTR("Unexpected response. type:" << m.getType()));


//	try
//	{
//		if (m.getType() != i)
//	//		throw elw::lang::Exception(MKSTR("Unexpected response. type:" << m.getType()));
//		throw elw::lang::Exception("ASDSDASD sELAMMM ");
//
//	}
//	catch ( elw::lang::Exception &e)
//	{
//		std::cout << "Exceptionn " << e.what() << std::endl;
//		throw e;
//	}
}

void bact()
{
	xcp1::Message req(xcp::msg::bact);
	Dapp::instance().write2croptrol(req);
	xcp1::Message resp;
	Dapp::instance().getMessage(resp);

	checkType(xcp::msg::bact +1,resp);
    checkAttribute(xcp::attr::rslt, resp, "0");
    checkAttribute(xcp::attr::hwaddr, resp, phsyHwaddrTest);
}

void eact()
{
	xcp1::Message req(xcp::msg::eact);
	Dapp::instance().write2croptrol(req);
	xcp1::Message resp;
	Dapp::instance().getMessage(resp);

	checkType(xcp::msg::eact +1,resp);
    checkAttribute(xcp::attr::rslt, resp, "0");
    checkAttribute(xcp::attr::hwaddr, resp, phsyHwaddrTest);
}

void reset()
{
    xcp1::Message req(xcp::msg::rst);
    Dapp::instance().write2croptrol(req);
    xcp1::Message resp;
    Dapp::instance().getMessage(resp);

	checkType(xcp::msg::rst +1,resp);
    checkAttribute(xcp::attr::rslt, resp, "0");
    checkAttribute(xcp::attr::hwaddr, resp, phsyHwaddrTest);
}

void ping()
{
    xcp1::Message req(xcp::msg::ping);
    Dapp::instance().write2croptrol(req);
    xcp1::Message resp;
    Dapp::instance().getMessage(resp);

	checkType(xcp::msg::ping +1,resp);
    checkAttribute(xcp::attr::rslt, resp, "0");
    checkAttribute(xcp::attr::hwaddr, resp, phsyHwaddrTest);
}

void configurePort(
		std::string const& port,
		std::string const& baudrate,
		std::string const& databits,
		std::string const& stopbits,
		std::string const& parity,
		xcp1::Message& resp,
		std::string const& mode
		)
{
	xcp1::Message req(xcp::msg::cfgp);
	req.addAttribute(xcp1::Attribute(xcp::attr::port).addValue(port));
	if(mode != "")
		req.addAttribute(xcp1::Attribute(7).addValue(mode));

	req.addAttribute(xcp1::Attribute(8).addValue(baudrate));
	req.addAttribute(xcp1::Attribute(9).addValue(databits));
	req.addAttribute(xcp1::Attribute(10).addValue(stopbits));
	req.addAttribute(xcp1::Attribute(11).addValue(parity));

	Dapp::instance().write2croptrol(req);
	Dapp::instance().getMessage(resp);

	checkType(xcp::msg::cfgp +1,resp);
	util::checkAttribute(xcp::attr::rslt,resp,"0");
	checkAttribute(xcp::attr::hwaddr, resp, phsyHwaddrTest);
}

void  beginDevice(
		std::string const& port,
		std::string const& devid,
		std::string const& read_period,
		xcp1::Message& resp
		)
{
	xcp1::Message req(xcp::msg::bd);
	req.addAttribute(xcp1::Attribute(xcp::attr::port).addValue(port));
	req.addAttribute(xcp1::Attribute(xcp::attr::devid).addValue(devid));
	req.addAttribute(xcp1::Attribute(8).addValue(read_period));

	Dapp::instance().write2croptrol(req);
	Dapp::instance().getMessage(resp);

	checkType(xcp::msg::bd +1,resp);
	util::checkAttribute(xcp::attr::rslt,resp,"0");
	checkAttribute(xcp::attr::hwaddr, resp, phsyHwaddrTest);
}

void endDevice(
		std::string const& port,
		std::string const& devid
		)
{
	xcp1::Message req(xcp::msg::ed);
	req.addAttribute(xcp1::Attribute(xcp::attr::port).addValue(port));
	req.addAttribute(xcp1::Attribute(xcp::attr::devid).addValue(devid));

	Dapp::instance().write2croptrol(req);
	xcp1::Message resp;
	Dapp::instance().getMessage(resp);

	util::checkType(xcp::msg::ed+1,resp);
	util::checkAttribute(xcp::attr::rslt,resp,"0");
	checkAttribute(xcp::attr::hwaddr, resp, phsyHwaddrTest);

}

void  updateDevice(
		std::string const& port,
		std::string const& read_period,
		xcp1::Message& resp
		)
{
	xcp1::Message req(xcp::msg::ud);
	req.addAttribute(xcp1::Attribute(xcp::attr::port).addValue(port));
	req.addAttribute(xcp1::Attribute(8).addValue(read_period));

	Dapp::instance().write2croptrol(req);
	Dapp::instance().getMessage(resp);

	checkType(xcp::msg::ud +1,resp);
	util::checkAttribute(xcp::attr::rslt,resp,"0");
	checkAttribute(xcp::attr::hwaddr, resp, phsyHwaddrTest);
}
void  removeDevice(
		std::string const& port,
		std::string const& devid,
		xcp1::Message& resp
		)
{
	xcp1::Message req(xcp::msg::rd);
	req.addAttribute(xcp1::Attribute(xcp::attr::port).addValue(port));
	req.addAttribute(xcp1::Attribute(xcp::attr::devid).addValue(devid));

	Dapp::instance().write2croptrol(req);
	Dapp::instance().getMessage(resp);

	checkType(xcp::msg::rd +1,resp);
	util::checkAttribute(xcp::attr::rslt,resp,"0");
	checkAttribute(xcp::attr::hwaddr, resp, phsyHwaddrTest);
}


void addSensor(
		std::string const& port,
		std::string const& devid,
		std::string const& ionum,
		xcp1::Message& resp
		)
{
    xcp1::Message req(xcp::msg::as);
    req.addAttribute(xcp1::Attribute(xcp::attr::port).addValue(port));
    req.addAttribute(xcp1::Attribute(xcp::attr::devid).addValue(devid));
    req.addAttribute(xcp1::Attribute(xcp::attr::ionum).addValue(ionum));

	Dapp::instance().write2croptrol(req);
	Dapp::instance().getMessage(resp);

	util::checkType(xcp::msg::as+1,resp);
	util::checkAttribute(xcp::attr::rslt,resp,"0");
    checkAttribute(xcp::attr::hwaddr, resp, phsyHwaddrTest);
}

void updateSensor(
		std::string const& port,
		std::string const& devid,
		std::string const& ionum,
		xcp1::Message& resp
		)
{
    xcp1::Message req(xcp::msg::us);
    req.addAttribute(xcp1::Attribute(xcp::attr::port).addValue(port));
    req.addAttribute(xcp1::Attribute(xcp::attr::devid).addValue(devid));
    req.addAttribute(xcp1::Attribute(xcp::attr::ionum).addValue(ionum));

	Dapp::instance().write2croptrol(req);
	Dapp::instance().getMessage(resp);

	util::checkType(xcp::msg::us+1,resp);
	util::checkAttribute(xcp::attr::rslt,resp,"0");
    checkAttribute(xcp::attr::hwaddr, resp, phsyHwaddrTest);
}

void removeSensor(
		std::string const& port,
		std::string const& devid,
		std::string const& ionum,
		xcp1::Message& resp
		)
{
	xcp1::Message req(xcp::msg::rs);
    req.addAttribute(xcp1::Attribute(xcp::attr::port).addValue(port));
    req.addAttribute(xcp1::Attribute(xcp::attr::devid).addValue(devid));
    req.addAttribute(xcp1::Attribute(xcp::attr::ionum).addValue(ionum));

    Dapp::instance().write2croptrol(req);
    Dapp::instance().getMessage(resp);

	util::checkType(xcp::msg::rs+1,resp);
	util::checkAttribute(xcp::attr::rslt,resp,"0");
    checkAttribute(xcp::attr::hwaddr, resp, phsyHwaddrTest);
}

void getSensorData(
		std::string const& port,
		std::string const& devid,
		std::string const& ionum,
		xcp1::Message& resp
		)
	{
		xcp1::Message req(xcp::msg::gsd);
	    req.addAttribute(xcp1::Attribute(xcp::attr::port).addValue(port));
	    req.addAttribute(xcp1::Attribute(xcp::attr::devid).addValue(devid));
	    req.addAttribute(xcp1::Attribute(xcp::attr::ionum).addValue(ionum));

		Dapp::instance().write2croptrol(req);
		Dapp::instance().getMessage(resp);

		util::checkType(xcp::msg::gsd+1,resp);
		util::checkAttribute(xcp::attr::rslt,resp,"0");
	    checkAttribute(xcp::attr::hwaddr, resp, phsyHwaddrTest);

	    if (resp.getAttribute(xcp::attr::rslt).getValues()[0] == "0")
		{
		    if (resp.getAttribute(7).getValues().size() != 1)
		        throw elw::lang::Exception(MKSTR("Invalid value attr. size:" << resp.getAttribute(7).getValues().size()));
		    if (resp.getAttribute(8).getValues().size() != 1)
		        throw elw::lang::Exception(MKSTR("Invalid time attr. size:" << resp.getAttribute(8).getValues().size()));

		}
	}

void writeSensorValue(
		std::string const& port,
		std::string const& devid,
		std::string const& ionum,
		std::string const& oid,
		xcp1::Message& resp
		)
{
    xcp1::Message req(xcp::msg::wsv);
    req.addAttribute(xcp1::Attribute(xcp::attr::port).addValue(port));
    req.addAttribute(xcp1::Attribute(xcp::attr::devid).addValue(devid));
    req.addAttribute(xcp1::Attribute(xcp::attr::ionum).addValue(ionum));
    req.addAttribute(xcp1::Attribute(7).addValue(oid));

	Dapp::instance().write2croptrol(req);
	Dapp::instance().getMessage(resp);

	util::checkType(xcp::msg::wsv+1,resp);
	util::checkAttribute(xcp::attr::rslt,resp,"0");
    checkAttribute(xcp::attr::hwaddr, resp, phsyHwaddrTest);
}

void changeSensorStatus(
		std::string const& port,
		std::string const& devid,
		std::string const& ionum,
		std::string const& oid,
		xcp1::Message& resp
		)
{
    xcp1::Message req(xcp::msg::css);
    req.addAttribute(xcp1::Attribute(xcp::attr::port).addValue(port));
    req.addAttribute(xcp1::Attribute(xcp::attr::devid).addValue(devid));
    req.addAttribute(xcp1::Attribute(xcp::attr::ionum).addValue(ionum));
    req.addAttribute(xcp1::Attribute(7).addValue(oid));

	Dapp::instance().write2croptrol(req);
	Dapp::instance().getMessage(resp);

	util::checkType(xcp::msg::css+1,resp);
	util::checkAttribute(xcp::attr::rslt,resp,"0");
    checkAttribute(xcp::attr::hwaddr, resp, phsyHwaddrTest);
}

}
