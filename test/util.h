/*
 * croptrol_util.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef croptrol_util_h__
#define croptrol_util_h__

namespace util {

extern std::string const& phsyHwaddrTest  ;
extern std::string const& processName ;

void waitForNewSensorData(
		xcp1::Message& resp,
        dword dwWait
        );

void checkAttribute(
        int i,
        xcp1::Message const& m,
        std::string const& val
        );

void checkType(
		int i,
		xcp1::Message const& m
		);

void bact();
void eact();
void reset();
void ping();

void configurePort(
		std::string const& port,
		std::string const& baudrate,
		std::string const& databits,
		std::string const& stopbits,
		std::string const& parity,
		xcp1::Message& resp,
		std::string const& mode = ""
		);

void beginDevice(
		std::string const& port,
		std::string const& devid,
		std::string const& read_period,
		xcp1::Message& resp
		);

void endDevice(
		std::string const& port,
		std::string const& devid);

void  updateDevice(
		std::string const& port,
		std::string const& read_period,
		xcp1::Message& resp
		);

void removeDevice(
		std::string const& port,
		std::string const& devid,
		xcp1::Message& resp
		);

void addSensor(
		std::string const& port,
		std::string const& devid,
		std::string const& ionum,
		xcp1::Message& resp
		);

void updateSensor(
		std::string const& port,
		std::string const& devid,
		std::string const& ionum,
		xcp1::Message& resp
		);

void removeSensor(
		std::string const& port,
		std::string const& devid,
		std::string const& ionum,
		xcp1::Message& resp
		);

void getSensorData(
		std::string const& port,
		std::string const& devid,
		std::string const& ionum,
		xcp1::Message& resp
		);

void writeSensorValue(
		std::string const& port,
		std::string const& devid,
		std::string const& ionum,
		std::string const& oid,
		xcp1::Message& resp
		);

void changeSensorStatus(
		std::string const& port,
		std::string const& devid,
		std::string const& ionum,
		std::string const& oid,
		xcp1::Message& resp
		);
}

#endif /* croptrol_util_h__ */
