/*
 * Communication044.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef Communication044_h__
#define Communication044_h__

class Communication044 :
    public ITest
{
public:
	Communication044()  {	Dapp::instance().start();	}
	~Communication044()	{	Dapp::instance().stop();	}
    std::string getDescription() const;
    void run();
};

#endif /* Communication044_h__ */
