/*
 * Communication009.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef Communication009_h__
#define Communication009_h__

class Communication009 :
    public ITest
{
public:
	Communication009()  {	Dapp::instance().start();	}
	~Communication009()	{	Dapp::instance().stop();	}
    std::string getDescription() const;
    void run();
};

#endif /* Communication009_h__ */
