/*
 * Communication075.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef Communication075_h__
#define Communication075_h__

class Communication075 :
    public ITest
{
public:
	Communication075()  {	Dapp::instance().start();	}
	~Communication075()	{	Dapp::instance().stop();	}
    std::string getDescription() const;
    void run();
};

#endif /* Communication075_h__ */
