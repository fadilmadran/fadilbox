/*
 * Communication001.cpp
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#include "stdafx.h"
#include "Dapp.h"
#include "Communication001.h"
#include "util.h"

std::string Communication001::getDescription() const
{
    return MKSTR("Communication001 - " << util::processName
    		<< "\n Unsupported Message (CroptrolHandler)");
}

void Communication001::run()
{
	std::cout << "Please wait. Loading... " << std::endl;
	xcp1::Message m;
	Dapp::instance().getMessage(m,20000);
	util::checkType(xcp::msg::act,m);

	//Test//

	xcp1::Message req(125);
	Dapp::instance().write2croptrol(req);
	xcp1::Message resp;
    Dapp::instance().getMessage(resp);
	util::checkType(xcp::msg::act, resp);

	Dapp::instance().isQueueEmpty();
}
