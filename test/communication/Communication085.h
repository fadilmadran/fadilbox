/*
 * Communication085.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef Communication085_h__
#define Communication085_h__

class Communication085 :
    public ITest
{
public:
	Communication085()  {	Dapp::instance().start();	}
	~Communication085()	{	Dapp::instance().stop();	}
    std::string getDescription() const;
    void run();
};

#endif /* Communication085_h__ */
