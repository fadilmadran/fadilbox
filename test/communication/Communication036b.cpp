/*
 * Communication036b.cpp
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#include "stdafx.h"
#include "Dapp.h"
#include "Communication036b.h"
#include "util.h"

std::string Communication036b::getDescription() const
{
    return MKSTR("Communication036b - " << util::processName
    		<< "\n Bact,Eact,BeginDevice - Invalid devType       EndDevice");
}

void Communication036b::run()
{
	std::cout << "Please wait. Loading... " << std::endl;
	xcp1::Message m;
	Dapp::instance().getMessage(m,20000);
	util::checkType(xcp::msg::act,m);

	//Test//

	util::bact();
	util::eact();

	std::string port 		= "0";
	std::string read_period = "10000";
	std::string devType 	= MKSTR(conf::dev::PING);

	// BeginDevice
	devType 	= MKSTR(conf::dev::PING+5);

	xcp1::Message req(xcp::msg::bd);
	req.addAttribute(xcp1::Attribute(xcp::attr::port).addValue(port));
	req.addAttribute(xcp1::Attribute(8).addValue(read_period));
	req.addAttribute(xcp1::Attribute(11).addValue(devType));

	Dapp::instance().write2croptrol(req);
	xcp1::Message resp;
	Dapp::instance().getMessage(resp);

	util::checkType(xcp::msg::bd+1,resp);
	util::checkAttribute(xcp::attr::rslt,resp,"2");
	util::checkAttribute(xcp::attr::hwaddr,resp, util::phsyHwaddrTest);

	std::string devid = resp.getAttribute(xcp::attr::devid).getValues()[0];

	// EndDevice
	util::endDevice(port,devid);

	Dapp::instance().isQueueEmpty();
}
