/*
 * Communication054.cpp
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#include "stdafx.h"
#include "Dapp.h"
#include "Communication054.h"
#include "util.h"

std::string Communication054::getDescription() const
{
    return MKSTR("Communication054 - " << util::processName
    		<< "\n RemoveDevice - Missing Mandatory Devid");
}

void Communication054::run()
{
	std::cout << "Please wait. Loading... " << std::endl;
	xcp1::Message m;
	Dapp::instance().getMessage(m,20000);
	util::checkType(xcp::msg::act,m);

	//Test//

	std::string port 		= "0";
	std::string devid	 	= "1";

	// RemoveDevice
	xcp1::Message req(xcp::msg::rd);
	req.addAttribute(xcp1::Attribute(xcp::attr::port).addValue(port));
//	req.addAttribute(xcp1::Attribute(xcp::attr::devid).addValue(devid));

	Dapp::instance().write2croptrol(req);
	xcp1::Message resp;
	Dapp::instance().getMessage(resp);

	util::checkType(xcp::msg::rd+1,resp);
	util::checkAttribute(xcp::attr::rslt,resp,"1");
	util::checkAttribute(xcp::attr::hwaddr,resp, util::phsyHwaddrTest);

	Dapp::instance().isQueueEmpty();
}
