/*
 * Communication084.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef Communication084_h__
#define Communication084_h__

class Communication084 :
    public ITest
{
public:
	Communication084()  {	Dapp::instance().start();	}
	~Communication084()	{	Dapp::instance().stop();	}
    std::string getDescription() const;
    void run();
};

#endif /* Communication084_h__ */
