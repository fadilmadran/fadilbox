/*
 * Communication091.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef Communication091_h__
#define Communication091_h__

class Communication091 :
    public ITest
{
public:
	Communication091()  {	Dapp::instance().start();	}
	~Communication091()	{	Dapp::instance().stop();	}
    std::string getDescription() const;
    void run();
};

#endif /* Communication091_h__ */
