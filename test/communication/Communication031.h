/*
 * Communication031.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef Communication031_h__
#define Communication031_h__

class Communication031 :
    public ITest
{
public:
	Communication031()  {	Dapp::instance().start();	}
	~Communication031()	{	Dapp::instance().stop();	}
    std::string getDescription() const;
    void run();
};

#endif /* Communication031_h__ */
