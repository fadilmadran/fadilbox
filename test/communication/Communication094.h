/*
 * Communication094.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef Communication094_h__
#define Communication094_h__

class Communication094 :
    public ITest
{
public:
	Communication094()  {	Dapp::instance().start();	}
	~Communication094()	{	Dapp::instance().stop();	}
    std::string getDescription() const;
    void run();
};

#endif /* Communication094_h__ */
