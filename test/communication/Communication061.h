/*
 * Communication061.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef Communication061_h__
#define Communication061_h__

class Communication061 :
    public ITest
{
public:
	Communication061()  {	Dapp::instance().start();	}
	~Communication061()	{	Dapp::instance().stop();	}
    std::string getDescription() const;
    void run();
};

#endif /* Communication061_h__ */
