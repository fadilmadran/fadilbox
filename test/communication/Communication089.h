/*
 * Communication089.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef Communication089_h__
#define Communication089_h__

class Communication089 :
    public ITest
{
public:
	Communication089()  {	Dapp::instance().start();	}
	~Communication089()	{	Dapp::instance().stop();	}
    std::string getDescription() const;
    void run();
};

#endif /* Communication089_h__ */
