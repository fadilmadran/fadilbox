/*
 * Communication035.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef Communication035_h__
#define Communication035_h__

class Communication035 :
    public ITest
{
public:
	Communication035()  {	Dapp::instance().start();	}
	~Communication035()	{	Dapp::instance().stop();	}
    std::string getDescription() const;
    void run();
};

#endif /* Communication035_h__ */
