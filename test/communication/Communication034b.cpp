/*
 * Communication034b.cpp
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#include "stdafx.h"
#include "Dapp.h"
#include "Communication034b.h"
#include "util.h"

std::string Communication034b::getDescription() const
{
    return MKSTR("Communication034b - " << util::processName
    		<< "\n BeginDevice - Missing Mandatory devType");
}

void Communication034b::run()
{
	std::cout << "Please wait. Loading... " << std::endl;
	xcp1::Message m;
	Dapp::instance().getMessage(m,20000);
	util::checkType(xcp::msg::act,m);

	//Test//

	std::string port 		= "0";
	std::string read_period = "10000";
	std::string devType 	= MKSTR(conf::dev::PING);

	// BeginDevice
	xcp1::Message req(xcp::msg::bd);
	req.addAttribute(xcp1::Attribute(xcp::attr::port).addValue(port));
	req.addAttribute(xcp1::Attribute(8).addValue(read_period));
//	req.addAttribute(xcp1::Attribute(11).addValue(devType));

	Dapp::instance().write2croptrol(req);
	xcp1::Message resp;
	Dapp::instance().getMessage(resp);

	util::checkType(xcp::msg::bd+1,resp);
	util::checkAttribute(xcp::attr::rslt,resp,"1");
	util::checkAttribute(xcp::attr::hwaddr,resp, util::phsyHwaddrTest);

	Dapp::instance().isQueueEmpty();
}
