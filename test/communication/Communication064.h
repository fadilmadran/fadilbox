/*
 * Communication064.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef Communication064_h__
#define Communication064_h__

class Communication064 :
    public ITest
{
public:
	Communication064()  {	Dapp::instance().start();	}
	~Communication064()	{	Dapp::instance().stop();	}
    std::string getDescription() const;
    void run();
};

#endif /* Communication064_h__ */
