/*
 * Communication050.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef Communication050_h__
#define Communication050_h__

class Communication050 :
    public ITest
{
public:
	Communication050()  {	Dapp::instance().start();	}
	~Communication050()	{	Dapp::instance().stop();	}
    std::string getDescription() const;
    void run();
};

#endif /* Communication050_h__ */
