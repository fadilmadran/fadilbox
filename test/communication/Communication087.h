/*
 * Communication087.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef Communication087_h__
#define Communication087_h__

class Communication087 :
    public ITest
{
public:
	Communication087()  {	Dapp::instance().start();	}
	~Communication087()	{	Dapp::instance().stop();	}
    std::string getDescription() const;
    void run();
};

#endif /* Communication087_h__ */
