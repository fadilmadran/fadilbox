/*
 * Communication005.cpp
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#include "stdafx.h"
#include "Dapp.h"
#include "Communication005.h"
#include "util.h"

std::string Communication005::getDescription() const
{
    return MKSTR("Communication005 - " << util::processName
    		<< "\n Eact");
}

void Communication005::run()
{
	std::cout << "Please wait. Loading... " << std::endl;
	xcp1::Message m;
	Dapp::instance().getMessage(m,20000);
	util::checkType(xcp::msg::act,m);

	//Test//

	util::eact();

	Dapp::instance().isQueueEmpty();
}
