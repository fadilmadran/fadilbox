/*
 * Communication068.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef Communication068_h__
#define Communication068_h__

class Communication068 :
    public ITest
{
public:
	Communication068()  {	Dapp::instance().start();	}
	~Communication068()	{	Dapp::instance().stop();	}
    std::string getDescription() const;
    void run();
};

#endif /* Communication068_h__ */
