/*
 * Communication079.cpp
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#include "stdafx.h"
#include "Dapp.h"
#include "Communication079.h"
#include "util.h"

std::string Communication079::getDescription() const
{
    return MKSTR("Communication079 - " << util::processName
    		<< "\n Bact,Eact,BeginDevice,EndDevice,AddSensor,UpdateSensor - Invalid Ionum");
}

void Communication079::run()
{
	std::cout << "Please wait. Loading... " << std::endl;
	xcp1::Message m;
	Dapp::instance().getMessage(m,20000);
	util::checkType(xcp::msg::act,m);

	//Test//

	std::string port 		= "0";
	std::string devid 		= "0";
	std::string read_period = "10000";
	std::string devType 	= MKSTR(conf::dev::PING);

	std::string ionum 		= "1";
	std::string remoteID	= "192.168.1.229";

	// Bact, Enact,BeginDevice,EndDevice

	util::bact();
	util::eact();
	xcp1::Message resp;
	util::beginDevice(port,devid,read_period,resp);


	util::endDevice(port,devid);

	elw::thread::sleep(1000);
//	Dapp::instance().getMessage(resp);
//	util::checkType(xcp::msg::ddc,resp);

	util::addSensor(port,devid,ionum,resp);

	// UpdateSensor
	ionum = "5";
	remoteID	= "192.168.1.183";
    xcp1::Message req(xcp::msg::us);
    req.addAttribute(xcp1::Attribute(xcp::attr::port).addValue(port));
    req.addAttribute(xcp1::Attribute(xcp::attr::devid).addValue(devid));
    req.addAttribute(xcp1::Attribute(xcp::attr::ionum).addValue(ionum));
    req.addAttribute(xcp1::Attribute(xcp::attr::addr).addValue(remoteID));

	Dapp::instance().write2croptrol(req);
	Dapp::instance().getMessage(resp);

	util::checkType(xcp::msg::us+1,resp);
	util::checkAttribute(xcp::attr::rslt,resp,"2");
	util::checkAttribute(xcp::attr::hwaddr,resp, util::phsyHwaddrTest);

	Dapp::instance().isQueueEmpty();
}
