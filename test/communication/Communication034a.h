/*
 * Communication034a.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef Communication034a_h__
#define Communication034a_h__

class Communication034a :
    public ITest
{
public:
	Communication034a()  {	Dapp::instance().start();	}
	~Communication034a()	{	Dapp::instance().stop();	}
    std::string getDescription() const;
    void run();
};

#endif /* Communication034a_h__ */
