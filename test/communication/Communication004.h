/*
 * Communication004.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef Communication004_h__
#define Communication004_h__

class Communication004 :
    public ITest
{
public:
	Communication004()  {	Dapp::instance().start();	}
	~Communication004()	{	Dapp::instance().stop();	}
    std::string getDescription() const;
    void run();
};

#endif /* Communication004_h__ */
