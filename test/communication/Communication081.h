/*
 * Communication081.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef Communication081_h__
#define Communication081_h__

class Communication081 :
    public ITest
{
public:
	Communication081()  {	Dapp::instance().start();	}
	~Communication081()	{	Dapp::instance().stop();	}
    std::string getDescription() const;
    void run();
};

#endif /* Communication081_h__ */
