/*
 * Communication009.cpp
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#include "stdafx.h"
#include "Dapp.h"
#include "Communication009.h"
#include "util.h"

std::string Communication009::getDescription() const
{
    return MKSTR("Communication009 - " << util::processName
    		<< "\n Ping");
}

void Communication009::run()
{
	std::cout << "Please wait. Loading... " << std::endl;
	xcp1::Message m;
	Dapp::instance().getMessage(m,20000);
	util::checkType(xcp::msg::act,m);

	//Test//

	util::ping();

	std::cout << "Test sadece benimle çalışıyor :S " << std::endl;

	Dapp::instance().isQueueEmpty();
}
