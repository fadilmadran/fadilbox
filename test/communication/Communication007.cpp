/*
 * Communication007.cpp
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#include "stdafx.h"
#include "Dapp.h"
#include "Communication007.h"
#include "util.h"

std::string Communication007::getDescription() const
{
    return MKSTR("Communication007 - " << util::processName
    		<< "\n Reset");
}

void Communication007::run()
{
	std::cout << "Please wait. Loading... " << std::endl;
	xcp1::Message m;
	Dapp::instance().getMessage(m,20000);
	util::checkType(xcp::msg::act,m);

	//Test//

	util::reset();

	std::cout << "Test sadece benimle çalışıyor :S " << std::endl;

	Dapp::instance().isQueueEmpty();
}
