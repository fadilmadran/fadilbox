/*
 * Communication057.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef Communication057_h__
#define Communication057_h__

class Communication057 :
    public ITest
{
public:
	Communication057()  {	Dapp::instance().start();	}
	~Communication057()	{	Dapp::instance().stop();	}
    std::string getDescription() const;
    void run();
};

#endif /* Communication057_h__ */
