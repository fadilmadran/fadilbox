/*
 * Communication048a.cpp
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#include "stdafx.h"
#include "Dapp.h"
#include "Communication048a.h"
#include "util.h"

std::string Communication048a::getDescription() const
{
    return MKSTR("Communication048a - " << util::processName
    		<< "\n Bact,Eact,BeginDevice,EndDevice,UpdateDevice - Missing Mandatory readPeriod");
}

void Communication048a::run()
{
	std::cout << "Please wait. Loading... " << std::endl;
	xcp1::Message m;
	Dapp::instance().getMessage(m,20000);
	util::checkType(xcp::msg::act,m);

	//Test//

	std::string port 		= "0";
	std::string devid 		= "0";
	std::string read_period = "10000";
	std::string devType 	= MKSTR(conf::dev::PING);

	// Bact, Enact,BeginDevice,EndDevice

	util::bact();
	util::eact();
	xcp1::Message resp;
	util::beginDevice(port,devid,read_period,resp);


	util::endDevice(port,devid);

	elw::thread::sleep(1000);
//	Dapp::instance().getMessage(resp);
//	util::checkType(xcp::msg::ddc,resp);

	// UpdateDevice

	xcp1::Message req(xcp::msg::ud);
	req.addAttribute(xcp1::Attribute(xcp::attr::port).addValue(port));
	req.addAttribute(xcp1::Attribute(xcp::attr::devid).addValue(devid));
//	req.addAttribute(xcp1::Attribute(8).addValue(read_period));

	Dapp::instance().write2croptrol(req);
	Dapp::instance().getMessage(resp);

	util::checkType(xcp::msg::ud+1,resp);
	util::checkAttribute(xcp::attr::rslt,resp,"1");
	util::checkAttribute(xcp::attr::hwaddr,resp, util::phsyHwaddrTest);

	Dapp::instance().isQueueEmpty();
}
