/*
 * Communication039.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef Communication039_h__
#define Communication039_h__

class Communication039 :
    public ITest
{
public:
	Communication039()  {	Dapp::instance().start();	}
	~Communication039()	{	Dapp::instance().stop();	}
    std::string getDescription() const;
    void run();
};

#endif /* Communication039_h__ */
