/*
 * Communication004.cpp
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#include "stdafx.h"
#include "Dapp.h"
#include "Communication004.h"
#include "util.h"

std::string Communication004::getDescription() const
{
    return MKSTR("Communication004 - " << util::processName
    		<< "\n Bact");
}

void Communication004::run()
{
	std::cout << "Please wait. Loading... " << std::endl;
	xcp1::Message m;
	Dapp::instance().getMessage(m,20000);
	util::checkType(xcp::msg::act,m);

	//Test//

	util::bact();

	Dapp::instance().isQueueEmpty();
}
