/*
 * Communication088.cpp
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#include "stdafx.h"
#include "Dapp.h"
#include "Communication088.h"
#include "util.h"

std::string Communication088::getDescription() const
{
    return MKSTR("Communication088 - " << util::processName
    		<< "\n Bact,Eact,BeginDevice,EndDevice,AddSensor,GetSensorData - All Valid (DeviceManager Running)  ");
}

void Communication088::run()
{
	std::cout << "Please wait. Loading... " << std::endl;
	xcp1::Message m;
	Dapp::instance().getMessage(m,20000);
	util::checkType(xcp::msg::act,m);

	std::string port 		= "0";
	std::string devid 		= "1";
	std::string ionum 		= "1";
	std::string read_period = "10000";

	xcp1::Message resp;

	// Bact, Eact,BeginDevice,EndDevice
	util::bact();
	util::eact();
	util::beginDevice(port,devid,read_period,resp);
	util::endDevice(port,devid);

	elw::thread::sleep(1000);

	//add Sensor
	util::addSensor(port,devid,ionum,resp);

	// GetSensorData
    xcp1::Message req(xcp::msg::gsd);
    req.addAttribute(xcp1::Attribute(xcp::attr::port).addValue(port));
    req.addAttribute(xcp1::Attribute(xcp::attr::devid).addValue(devid));
    req.addAttribute(xcp1::Attribute(xcp::attr::ionum).addValue(ionum));

	Dapp::instance().write2croptrol(req);
	Dapp::instance().getMessage(resp);

	util::checkType(xcp::msg::gsd+1,resp);
	util::checkAttribute(xcp::attr::rslt,resp,"0");
	util::checkAttribute(xcp::attr::hwaddr,resp, util::phsyHwaddrTest);

	Dapp::instance().getMessage(resp);

	Dapp::instance().isQueueEmpty();
}
