/*
 * Communication086.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef Communication086_h__
#define Communication086_h__

class Communication086 :
    public ITest
{
public:
	Communication086()  {	Dapp::instance().start();	}
	~Communication086()	{	Dapp::instance().stop();	}
    std::string getDescription() const;
    void run();
};

#endif /* Communication086_h__ */
