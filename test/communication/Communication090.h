/*
 * Communication090.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef Communication090_h__
#define Communication090_h__

class Communication090 :
    public ITest
{
public:
	Communication090()  {	Dapp::instance().start();	}
	~Communication090()	{	Dapp::instance().stop();	}
    std::string getDescription() const;
    void run();
};

#endif /* Communication090_h__ */
