/*
 * Communication033.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef Communication033_h__
#define Communication033_h__

class Communication033 :
    public ITest
{
public:
	Communication033()  {	Dapp::instance().start();	}
	~Communication033()	{	Dapp::instance().stop();	}
    std::string getDescription() const;
    void run();
};

#endif /* Communication033_h__ */
