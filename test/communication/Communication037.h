/*
 * Communication037.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef Communication037_h__
#define Communication037_h__

class Communication037 :
    public ITest
{
public:
	Communication037()  {	Dapp::instance().start();	}
	~Communication037()	{	Dapp::instance().stop();	}
    std::string getDescription() const;
    void run();
};

#endif /* Communication037_h__ */
