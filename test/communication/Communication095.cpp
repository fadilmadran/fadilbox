/*
 * Communication095.cpp
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#include "stdafx.h"
#include "Dapp.h"
#include "Communication095.h"
#include "util.h"

std::string Communication095::getDescription() const
{
    return MKSTR("Communication095 - " << util::processName
    		<< "\n Bact,Eact,BeginDevice,EndDevice,AddSensor - All Valid (DeviceManager Running)");
}

void Communication095::run()
{
	std::cout << "Please wait. Loading... " << std::endl;
	xcp1::Message m;
	Dapp::instance().getMessage(m,20000);
	util::checkType(xcp::msg::act,m);


	std::string port 		= "0";
	std::string devid 		= "1";
	std::string read_period = "10000";
	std::string ionum 		= "1";

	xcp1::Message resp;

	// Bact, Eact,BeginDevice,EndDevice
	util::bact();
	util::eact();
	util::beginDevice(port,devid,read_period,resp);
	util::endDevice(port,devid);

	elw::thread::sleep(1000);

	//add Sensor
	util::addSensor(port,devid,ionum,resp);

	Dapp::instance().getMessage(resp);

	//get sensor data
	util::getSensorData(port,devid,ionum,resp);

	//remove sensor
	util::removeSensor(port,devid,ionum,resp);


	//add Sensor
	util::addSensor(port,devid,ionum,resp);

	//get sensor data
	util::getSensorData(port,devid,ionum,resp);

	Dapp::instance().getMessage(resp);

	Dapp::instance().isQueueEmpty();
}
