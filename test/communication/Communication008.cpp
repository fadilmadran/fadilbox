/*
 * Communication008.cpp
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#include "stdafx.h"
#include "Dapp.h"
#include "Communication008.h"
#include "util.h"

std::string Communication008::getDescription() const
{
    return MKSTR("Communication008 - " << util::processName
    		<< "\n Bact,Eact,Reset");
}

void Communication008::run()
{
	std::cout << "Please wait. Loading... " << std::endl;
	xcp1::Message m;
	Dapp::instance().getMessage(m,20000);
	util::checkType(xcp::msg::act,m);

	//Test//

	util::bact();
	util::eact();
	util::reset();

	Dapp::instance().isQueueEmpty();
}
