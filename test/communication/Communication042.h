/*
 * Communication042.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef Communication042_h__
#define Communication042_h__

class Communication042 :
    public ITest
{
public:
	Communication042()  {	Dapp::instance().start();	}
	~Communication042()	{	Dapp::instance().stop();	}
    std::string getDescription() const;
    void run();
};

#endif /* Communication042_h__ */
