/*
 * Communication070.cpp
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#include "stdafx.h"
#include "Dapp.h"
#include "Communication070.h"
#include "util.h"

std::string Communication070::getDescription() const
{
    return MKSTR("Communication070 - " << util::processName
    		<< "\n Bact,BeginDevice,AddSensor,Eact,EndDevice - All valid  (DeviceManager Running)");
}

void Communication070::run()
{
	std::cout << "Please wait. Loading... " << std::endl;
	xcp1::Message m;
	Dapp::instance().getMessage(m,20000);
	util::checkType(xcp::msg::act,m);


	std::string port 		= "0";
	std::string devid 		= "1";
	std::string read_period = "10000";
	std::string ionum 		= "1";

	xcp1::Message resp;

	// Bact,BeginDevice
	util::bact();
	util::beginDevice(port,devid,read_period,resp);


	elw::thread::sleep(1000);

	// AddSensor
    xcp1::Message req(xcp::msg::as);
    req.addAttribute(xcp1::Attribute(xcp::attr::port).addValue(port));
    req.addAttribute(xcp1::Attribute(xcp::attr::devid).addValue(devid));
    req.addAttribute(xcp1::Attribute(xcp::attr::ionum).addValue(ionum));

	Dapp::instance().write2croptrol(req);
	Dapp::instance().getMessage(resp);

	util::checkType(xcp::msg::as+1,resp);
	util::checkAttribute(xcp::attr::rslt,resp,"0");
	util::checkAttribute(xcp::attr::hwaddr,resp, util::phsyHwaddrTest);

	//End Device,Eact
	util::endDevice(port,devid);
	util::eact();

	Dapp::instance().getMessage(resp);

	Dapp::instance().isQueueEmpty();
}
