/*
 * Communication081.cpp
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#include "stdafx.h"
#include "Dapp.h"
#include "Communication081.h"
#include "util.h"

std::string Communication081::getDescription() const
{
    return MKSTR("Communication081 - " << util::processName
    		<< "\n Bact,Eact,BeginDevice,EndDevice,AddSensor,RemoveSensor - All Valid (DeviceManager Running)  ");
}

void Communication081::run()
{
	std::cout << "Please wait. Loading... " << std::endl;
	xcp1::Message m;
	Dapp::instance().getMessage(m,20000);
	util::checkType(xcp::msg::act,m);

	std::string port 		= "0";
	std::string devid 		= "1";
	std::string ionum 		= "1";
	std::string read_period = "10000";

	xcp1::Message resp;

	// Bact, Eact,BeginDevice,EndDevice
	util::bact();
	util::eact();
	util::beginDevice(port,devid,read_period,resp);
	util::endDevice(port,devid);

	elw::thread::sleep(1000);

	//addSensor
	util::addSensor(port,devid,ionum,resp);

	Dapp::instance().getMessage(resp);

	// RemoveSensor
    xcp1::Message req(xcp::msg::rs);
    req.addAttribute(xcp1::Attribute(xcp::attr::port).addValue(port));
    req.addAttribute(xcp1::Attribute(xcp::attr::devid).addValue(devid));
    req.addAttribute(xcp1::Attribute(xcp::attr::ionum).addValue(ionum));

	Dapp::instance().write2croptrol(req);
	Dapp::instance().getMessage(resp);

	util::checkType(xcp::msg::rs+1,resp);
	util::checkAttribute(xcp::attr::rslt,resp,"0");
	util::checkAttribute(xcp::attr::hwaddr,resp, util::phsyHwaddrTest);

	Dapp::instance().isQueueEmpty();
}
