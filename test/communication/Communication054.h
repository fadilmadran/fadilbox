/*
 * Communication054.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef Communication054_h__
#define Communication054_h__

class Communication054 :
    public ITest
{
public:
	Communication054()  {	Dapp::instance().start();	}
	~Communication054()	{	Dapp::instance().stop();	}
    std::string getDescription() const;
    void run();
};

#endif /* Communication054_h__ */
