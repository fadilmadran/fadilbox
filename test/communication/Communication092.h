/*
 * Communication092.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef Communication092_h__
#define Communication092_h__

class Communication092 :
    public ITest
{
public:
	Communication092()  {	Dapp::instance().start();	}
	~Communication092()	{	Dapp::instance().stop();	}
    std::string getDescription() const;
    void run();
};

#endif /* Communication092_h__ */
