/*
 * Communication074.cpp
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#include "stdafx.h"
#include "Dapp.h"
#include "Communication074.h"
#include "util.h"

std::string Communication074::getDescription() const
{
    return MKSTR("Communication074 - " << util::processName
    		<< "\n UpdateSensor - Missing Mandatory Devid ");
}

void Communication074::run()
{
	std::cout << "Please wait. Loading... " << std::endl;
	xcp1::Message m;
	Dapp::instance().getMessage(m,20000);
	util::checkType(xcp::msg::act,m);

	//Test//

	std::string port 		= "0";
	std::string read_period = "10000";
	std::string devType 	= MKSTR(conf::dev::PING);

	std::string ionum 		= "1";
	std::string remoteID	= "192.168.1.229";

	std::string devid = "1";


	// UpdateSensor

	xcp1::Message resp;

    xcp1::Message req(xcp::msg::us);
    req.addAttribute(xcp1::Attribute(xcp::attr::port).addValue(port));
//    req.addAttribute(xcp1::Attribute(xcp::attr::devid).addValue(devid));
    req.addAttribute(xcp1::Attribute(xcp::attr::ionum).addValue(ionum));
    req.addAttribute(xcp1::Attribute(xcp::attr::addr).addValue(remoteID));

	Dapp::instance().write2croptrol(req);
	Dapp::instance().getMessage(resp);

	util::checkType(xcp::msg::us+1,resp);
	util::checkAttribute(xcp::attr::rslt,resp,"1");
	util::checkAttribute(xcp::attr::hwaddr,resp, util::phsyHwaddrTest);

	Dapp::instance().isQueueEmpty();
}
