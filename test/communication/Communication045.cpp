/*
 * Communication045.cpp
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#include "stdafx.h"
#include "Dapp.h"
#include "Communication045.h"
#include "util.h"

std::string Communication045::getDescription() const
{
    return MKSTR("Communication045 - " << util::processName
    		<< "\n Bact,Eact,BeginDevice,EndDevice,UpdateDevice - All Valid (DeviceManager Running)");
}

void Communication045::run()
{
	std::cout << "Please wait. Loading... " << std::endl;
	xcp1::Message m;
	Dapp::instance().getMessage(m,20000);
	util::checkType(xcp::msg::act,m);

	std::string port 		= "0";
	std::string devid 		= "0";
	std::string read_period = "10000";
	std::string devType 	= MKSTR(conf::dev::PING);

	std::string ionum 		= "1";

	// Bact, Eact,BeginDevice,EndDevice
	util::bact();
	util::eact();
	xcp1::Message resp;
	util::beginDevice(port,devid,read_period,resp);
	util::endDevice(port,devid);

	elw::thread::sleep(1000);

	// UpdateDevice
	xcp1::Message req(xcp::msg::ud);
	req.addAttribute(xcp1::Attribute(xcp::attr::port).addValue(port));
	req.addAttribute(xcp1::Attribute(xcp::attr::devid).addValue(devid));
	req.addAttribute(xcp1::Attribute(xcp::attr::ionum).addValue(ionum));
	req.addAttribute(xcp1::Attribute(8).addValue(read_period));

	Dapp::instance().write2croptrol(req);
	Dapp::instance().getMessage(resp);

	util::checkType(xcp::msg::ud+1,resp);
	util::checkAttribute(xcp::attr::rslt,resp,"0");
	util::checkAttribute(xcp::attr::hwaddr,resp, util::phsyHwaddrTest);

	Dapp::instance().isQueueEmpty();
}
