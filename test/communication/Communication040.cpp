/*
 * Communication040.cpp
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#include "stdafx.h"
#include "Dapp.h"
#include "Communication040.h"
#include "util.h"

std::string Communication040::getDescription() const
{
    return MKSTR("Communication040 - " << util::processName
    		<< "\n EndDevice - Without BeginDevice");
}

void Communication040::run()
{
	std::cout << "Please wait. Loading... " << std::endl;
	xcp1::Message m;
	Dapp::instance().getMessage(m,20000);
	util::checkType(xcp::msg::act,m);

	//Test//

	std::string port 		= "0";
	std::string devid 		= "1";

	// EndDevice

	xcp1::Message resp;
	xcp1::Message req(xcp::msg::ed);
	req.addAttribute(xcp1::Attribute(xcp::attr::port).addValue(port));
	req.addAttribute(xcp1::Attribute(xcp::attr::devid).addValue(devid));

	Dapp::instance().write2croptrol(req);
	Dapp::instance().getMessage(resp);

	util::checkType(xcp::msg::ed+1,resp);
	util::checkAttribute(xcp::attr::rslt,resp,"2");
	util::checkAttribute(xcp::attr::hwaddr,resp, util::phsyHwaddrTest);


	Dapp::instance().isQueueEmpty();
}
