/*
 * Communication065.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef Communication065_h__
#define Communication065_h__

class Communication065 :
    public ITest
{
public:
	Communication065()  {	Dapp::instance().start();	}
	~Communication065()	{	Dapp::instance().stop();	}
    std::string getDescription() const;
    void run();
};

#endif /* Communication065_h__ */
