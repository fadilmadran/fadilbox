/*
 * Communication010.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef Communication010_h__
#define Communication010_h__

class Communication010 :
    public ITest
{
public:
	Communication010()  {	Dapp::instance().start();	}
	~Communication010()	{	Dapp::instance().stop();	}
    std::string getDescription() const;
    void run();
};

#endif /* Communication010_h__ */
