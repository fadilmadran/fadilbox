/*
 * Communication069.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef Communication069_h__
#define Communication069_h__

class Communication069 :
    public ITest
{
public:
	Communication069()  {	Dapp::instance().start();	}
	~Communication069()	{	Dapp::instance().stop();	}
    std::string getDescription() const;
    void run();
};

#endif /* Communication069_h__ */
