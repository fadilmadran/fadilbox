/*
 * Communication000.cpp
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#include "stdafx.h"
#include "Dapp.h"
#include "Communication000.h"
#include "util.h"

std::string Communication000::getDescription() const
{
    return MKSTR("Communication000 - " << util::processName
    		<< "\n Description");
}

void Communication000::run()
{
	std::cout << "Please wait. Loading... " << std::endl;
	xcp1::Message m;
	Dapp::instance().getMessage(m,20000);
	util::checkType(xcp::msg::act,m);

	//Test//

	Dapp::instance().isQueueEmpty();
}
