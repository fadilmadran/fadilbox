/*
 * Communication005.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef Communication005_h__
#define Communication005_h__

class Communication005 :
    public ITest
{
public:
	Communication005()  {	Dapp::instance().start();	}
	~Communication005()	{	Dapp::instance().stop();	}
    std::string getDescription() const;
    void run();
};

#endif /* Communication005_h__ */
