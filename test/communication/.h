/*
 * Communication045.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef Communication045_h__
#define Communication045_h__

class Communication045 :
    public ITest
{
public:
	Communication045()  {	Dapp::instance().start();	}
	~Communication045()	{	Dapp::instance().stop();	}
    std::string getDescription() const;
    void run();
};

#endif /* Communication045_h__ */
