/*
 * Communication088.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef Communication088_h__
#define Communication088_h__

class Communication088 :
    public ITest
{
public:
	Communication088()  {	Dapp::instance().start();	}
	~Communication088()	{	Dapp::instance().stop();	}
    std::string getDescription() const;
    void run();
};

#endif /* Communication088_h__ */
