/*
 * Communication047.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef Communication047_h__
#define Communication047_h__

class Communication047 :
    public ITest
{
public:
	Communication047()  {	Dapp::instance().start();	}
	~Communication047()	{	Dapp::instance().stop();	}
    std::string getDescription() const;
    void run();
};

#endif /* Communication047_h__ */
