/*
 * Communication002.cpp
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#include "stdafx.h"
#include "Dapp.h"
#include "Communication002.h"
#include "util.h"

std::string Communication002::getDescription() const
{
    return MKSTR("Communication002 - " << util::processName
    		<< "\n Unsupported Message (DeviceManager Activating)");
}

void Communication002::run()
{
	std::cout << "Please wait. Loading... " << std::endl;
	xcp1::Message m;
	Dapp::instance().getMessage(m,20000);
	util::checkType(xcp::msg::act,m);

	//Test//

	xcp1::Message req(xcp::msg::ping);
	Dapp::instance().write2croptrol(req);
	xcp1::Message resp;
    Dapp::instance().getMessage(resp);
	util::checkType(xcp::msg::act, resp);

	Dapp::instance().isQueueEmpty();
}
