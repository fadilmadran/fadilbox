/*
 * Communication093.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef Communication093_h__
#define Communication093_h__

class Communication093 :
    public ITest
{
public:
	Communication093()  {	Dapp::instance().start();	}
	~Communication093()	{	Dapp::instance().stop();	}
    std::string getDescription() const;
    void run();
};

#endif /* Communication093_h__ */
