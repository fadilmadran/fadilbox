/*
 * Communication046.cpp
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#include "stdafx.h"
#include "Dapp.h"
#include "Communication046.h"
#include "util.h"

std::string Communication046::getDescription() const
{
    return MKSTR("Communication046 - " << util::processName
    		<< "\n UpdateDevice -Missing Mandatory Port (DeviceManager Running)");
}

void Communication046::run()
{
	std::cout << "Please wait. Loading... " << std::endl;
	xcp1::Message m;
	Dapp::instance().getMessage(m,20000);
	util::checkType(xcp::msg::act,m);

	//Test//

	std::string port 		= "0";
	std::string devid	 	= "1";
	std::string read_period = "10000";

	// UpdateDevice

	xcp1::Message req(xcp::msg::ud);
//	req.addAttribute(xcp1::Attribute(xcp::attr::port).addValue(port));
	req.addAttribute(xcp1::Attribute(xcp::attr::devid).addValue(devid));
	req.addAttribute(xcp1::Attribute(8).addValue(read_period));

	Dapp::instance().write2croptrol(req);
	xcp1::Message resp;
	Dapp::instance().getMessage(resp);

	util::checkType(xcp::msg::ud+1,resp);
	util::checkAttribute(xcp::attr::rslt,resp,"1");
	util::checkAttribute(xcp::attr::hwaddr,resp, util::phsyHwaddrTest);

	Dapp::instance().isQueueEmpty();
}
