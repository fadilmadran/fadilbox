/*
 * Communication003.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef Communication003_h__
#define Communication003_h__

class Communication003 :
    public ITest
{
public:
	Communication003()  {	Dapp::instance().start();	}
	~Communication003()	{	Dapp::instance().stop();	}
    std::string getDescription() const;
    void run();
};

#endif /* Communication003_h__ */
