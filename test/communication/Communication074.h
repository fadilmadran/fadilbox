/*
 * Communication074.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef Communication074_h__
#define Communication074_h__

class Communication074 :
    public ITest
{
public:
	Communication074()  {	Dapp::instance().start();	}
	~Communication074()	{	Dapp::instance().stop();	}
    std::string getDescription() const;
    void run();
};

#endif /* Communication074_h__ */
