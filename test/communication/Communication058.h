/*
 * Communication058.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef Communication058_h__
#define Communication058_h__

class Communication058 :
    public ITest
{
public:
	Communication058()  {	Dapp::instance().start();	}
	~Communication058()	{	Dapp::instance().stop();	}
    std::string getDescription() const;
    void run();
};

#endif /* Communication058_h__ */
