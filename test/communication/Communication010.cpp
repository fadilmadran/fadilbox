/*
 * Communication010.cpp
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#include "stdafx.h"
#include "Dapp.h"
#include "Communication010.h"
#include "util.h"

std::string Communication010::getDescription() const
{
    return MKSTR("Communication010 - " << util::processName
    		<< "\n Bact,Eact,Ping");
}

void Communication010::run()
{
	std::cout << "Please wait. Loading... " << std::endl;
	xcp1::Message m;
	Dapp::instance().getMessage(m,20000);
	util::checkType(xcp::msg::act,m);

	//Test//

	util::bact();
	util::eact();
	util::ping();

	Dapp::instance().isQueueEmpty();
}
