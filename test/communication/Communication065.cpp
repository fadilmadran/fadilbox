/*
 * Communication065.cpp
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#include "stdafx.h"
#include "Dapp.h"
#include "Communication065.h"
#include "util.h"

std::string Communication065::getDescription() const
{
    return MKSTR("Communication065 - " << util::processName
    		<< "\n BeginDevice,AddSensor - Invalid Devid    (CroptrolHandler)");
}

void Communication065::run()
{
	std::cout << "Please wait. Loading... " << std::endl;
	xcp1::Message m;
	Dapp::instance().getMessage(m,20000);
	util::checkType(xcp::msg::act,m);


	std::string port 		= "0";
	std::string devid 		= "0";
	std::string read_period = "10000";
	std::string ionum 		= "1";

	// BeginDevice
	xcp1::Message resp;
	util::beginDevice(port,devid,read_period,resp);


	// AddSensor
	devid = "5";
    xcp1::Message req(xcp::msg::as);
    req.addAttribute(xcp1::Attribute(xcp::attr::port).addValue(port));
    req.addAttribute(xcp1::Attribute(xcp::attr::devid).addValue(devid));
    req.addAttribute(xcp1::Attribute(xcp::attr::ionum).addValue(ionum));

	Dapp::instance().write2croptrol(req);
	Dapp::instance().getMessage(resp);

	util::checkType(xcp::msg::as+1,resp);
	util::checkAttribute(xcp::attr::rslt,resp,"2");
	util::checkAttribute(xcp::attr::hwaddr,resp, util::phsyHwaddrTest);


	Dapp::instance().isQueueEmpty();
}
