/*
 * Communication073.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef Communication073_h__
#define Communication073_h__

class Communication073 :
    public ITest
{
public:
	Communication073()  {	Dapp::instance().start();	}
	~Communication073()	{	Dapp::instance().stop();	}
    std::string getDescription() const;
    void run();
};

#endif /* Communication073_h__ */
