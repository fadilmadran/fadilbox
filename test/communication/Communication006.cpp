/*
 * Communication006.cpp
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#include "stdafx.h"
#include "Dapp.h"
#include "Communication006.h"
#include "util.h"

std::string Communication006::getDescription() const
{
    return MKSTR("Communication006 - " << util::processName
    		<< "\n Bact,Eact");
}

void Communication006::run()
{
	std::cout << "Please wait. Loading... " << std::endl;
	xcp1::Message m;
	Dapp::instance().getMessage(m,20000);
	util::checkType(xcp::msg::act,m);

	//Test//

	util::bact();
	util::eact();

	Dapp::instance().isQueueEmpty();
}
