/*
 * Communication069.cpp
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#include "stdafx.h"
#include "Dapp.h"
#include "Communication069.h"
#include "util.h"

std::string Communication069::getDescription() const
{
    return MKSTR("Communication069 - " << util::processName
    		<< "\n Bact,Eact,BeginDevice,EndDevice,AddSensor - Invalid Devid    (DeviceManager Running)");
}

void Communication069::run()
{
	std::cout << "Please wait. Loading... " << std::endl;
	xcp1::Message m;
	Dapp::instance().getMessage(m,20000);
	util::checkType(xcp::msg::act,m);


	std::string port 		= "0";
	std::string devid 		= "0";
	std::string read_period = "10000";
	std::string ionum 		= "1";

	// Bact, Enact,BeginDevice,EndDevice
	util::bact();
	util::eact();
	xcp1::Message resp;
	util::beginDevice(port,devid,read_period,resp);
	util::endDevice(port,devid);

	elw::thread::sleep(1000);

	// AddSensor
	devid = "5";
    xcp1::Message req(xcp::msg::as);
    req.addAttribute(xcp1::Attribute(xcp::attr::port).addValue(port));
    req.addAttribute(xcp1::Attribute(xcp::attr::devid).addValue(devid));
    req.addAttribute(xcp1::Attribute(xcp::attr::ionum).addValue(ionum));

	Dapp::instance().write2croptrol(req);
	Dapp::instance().getMessage(resp);

	util::checkType(xcp::msg::as+1,resp);
	util::checkAttribute(xcp::attr::rslt,resp,"2");
	util::checkAttribute(xcp::attr::hwaddr,resp, util::phsyHwaddrTest);

	Dapp::instance().isQueueEmpty();
}
