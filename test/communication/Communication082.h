/*
 * Communication082.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef Communication082_h__
#define Communication082_h__

class Communication082 :
    public ITest
{
public:
	Communication082()  {	Dapp::instance().start();	}
	~Communication082()	{	Dapp::instance().stop();	}
    std::string getDescription() const;
    void run();
};

#endif /* Communication082_h__ */
