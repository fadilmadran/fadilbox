/*
 * Communication048a.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef Communication048a_h__
#define Communication048a_h__

class Communication048a :
    public ITest
{
public:
	Communication048a()  {	Dapp::instance().start();	}
	~Communication048a()	{	Dapp::instance().stop();	}
    std::string getDescription() const;
    void run();
};

#endif /* Communication048a_h__ */
