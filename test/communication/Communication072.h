/*
 * Communication072.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef Communication072_h__
#define Communication072_h__

class Communication072 :
    public ITest
{
public:
	Communication072()  {	Dapp::instance().start();	}
	~Communication072()	{	Dapp::instance().stop();	}
    std::string getDescription() const;
    void run();
};

#endif /* Communication072_h__ */
