/*
 * Communication056.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef Communication056_h__
#define Communication056_h__

class Communication056 :
    public ITest
{
public:
	Communication056()  {	Dapp::instance().start();	}
	~Communication056()	{	Dapp::instance().stop();	}
    std::string getDescription() const;
    void run();
};

#endif /* Communication056_h__ */
