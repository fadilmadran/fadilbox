/*
 * Communication049.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef Communication049_h__
#define Communication049_h__

class Communication049 :
    public ITest
{
public:
	Communication049()  {	Dapp::instance().start();	}
	~Communication049()	{	Dapp::instance().stop();	}
    std::string getDescription() const;
    void run();
};

#endif /* Communication049_h__ */
