/*
 * Communication083.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef Communication083_h__
#define Communication083_h__

class Communication083 :
    public ITest
{
public:
	Communication083()  {	Dapp::instance().start();	}
	~Communication083()	{	Dapp::instance().stop();	}
    std::string getDescription() const;
    void run();
};

#endif /* Communication083_h__ */
