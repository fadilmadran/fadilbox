/*
 * Communication008.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef Communication008_h__
#define Communication008_h__

class Communication008 :
    public ITest
{
public:
	Communication008()  {	Dapp::instance().start();	}
	~Communication008()	{	Dapp::instance().stop();	}
    std::string getDescription() const;
    void run();
};

#endif /* Communication008_h__ */
