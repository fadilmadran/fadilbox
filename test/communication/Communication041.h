/*
 * Communication041.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef Communication041_h__
#define Communication041_h__

class Communication041 :
    public ITest
{
public:
	Communication041()  {	Dapp::instance().start();	}
	~Communication041()	{	Dapp::instance().stop();	}
    std::string getDescription() const;
    void run();
};

#endif /* Communication041_h__ */
