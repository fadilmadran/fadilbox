/*
 * Communication043.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef Communication043_h__
#define Communication043_h__

class Communication043 :
    public ITest
{
public:
	Communication043()  {	Dapp::instance().start();	}
	~Communication043()	{	Dapp::instance().stop();	}
    std::string getDescription() const;
    void run();
};

#endif /* Communication043_h__ */
