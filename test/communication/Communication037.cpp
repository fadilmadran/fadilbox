/*
 * Communication037.cpp
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#include "stdafx.h"
#include "Dapp.h"
#include "Communication037.h"
#include "util.h"

std::string Communication037::getDescription() const
{
    return MKSTR("Communication037 - " << util::processName
    		<< "\n 64 x BeginDevice to get NOT available devid");
}

void Communication037::run()
{
	std::cout << "Please wait. Loading... " << std::endl;
	xcp1::Message m;
	Dapp::instance().getMessage(m,20000);
	util::checkType(xcp::msg::act,m);

	//Test//

	for(int i=1; ;i++)
	{
		std::string port 		= "0";
		std::string read_period = "10000";
		std::string devType 	= MKSTR(conf::dev::PING);

		// BeginDevice
		xcp1::Message req(xcp::msg::bd);
		req.addAttribute(xcp1::Attribute(xcp::attr::port).addValue(port));
		req.addAttribute(xcp1::Attribute(8).addValue(read_period));
		req.addAttribute(xcp1::Attribute(11).addValue(devType));


		Dapp::instance().write2croptrol(req);
		xcp1::Message resp;
		Dapp::instance().getMessage(resp);

		util::checkType(xcp::msg::bd+1,resp);

		if(resp.getAttribute(xcp::attr::rslt).getValues()[0] == "0")
		{
			std::string devid = resp.getAttribute(xcp::attr::devid).getValues()[0];
			util::endDevice(port,devid);
		}
		else
		{
			std::cout << "Devid not exist in " << i << "th try" << std::endl;
			return;
 		}
	}


	Dapp::instance().isQueueEmpty();
}
