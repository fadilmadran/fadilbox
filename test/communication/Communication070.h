/*
 * Communication070.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef Communication070_h__
#define Communication070_h__

class Communication070 :
    public ITest
{
public:
	Communication070()  {	Dapp::instance().start();	}
	~Communication070()	{	Dapp::instance().stop();	}
    std::string getDescription() const;
    void run();
};

#endif /* Communication070_h__ */
