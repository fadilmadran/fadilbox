/*
 * Communication038.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef Communication038_h__
#define Communication038_h__

class Communication038 :
    public ITest
{
public:
	Communication038()  {	Dapp::instance().start();	}
	~Communication038()	{	Dapp::instance().stop();	}
    std::string getDescription() const;
    void run();
};

#endif /* Communication038_h__ */
