/*
 * Communication036b.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef Communication036b_h__
#define Communication036b_h__

class Communication036b :
    public ITest
{
public:
	Communication036b()  {	Dapp::instance().start();	}
	~Communication036b()	{	Dapp::instance().stop();	}
    std::string getDescription() const;
    void run();
};

#endif /* Communication036b_h__ */
