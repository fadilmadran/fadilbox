/*
 * Communication079.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef Communication079_h__
#define Communication079_h__

class Communication079 :
    public ITest
{
public:
	Communication079()  {	Dapp::instance().start();	}
	~Communication079()	{	Dapp::instance().stop();	}
    std::string getDescription() const;
    void run();
};

#endif /* Communication079_h__ */
