/*
 * Communication055.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef Communication055_h__
#define Communication055_h__

class Communication055 :
    public ITest
{
public:
	Communication055()  {	Dapp::instance().start();	}
	~Communication055()	{	Dapp::instance().stop();	}
    std::string getDescription() const;
    void run();
};

#endif /* Communication055_h__ */
