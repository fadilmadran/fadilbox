/*
 * Communication006.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef Communication006_h__
#define Communication006_h__

class Communication006 :
    public ITest
{
public:
	Communication006()  {	Dapp::instance().start();	}
	~Communication006()	{	Dapp::instance().stop();	}
    std::string getDescription() const;
    void run();
};

#endif /* Communication006_h__ */
