/*
 * Communication052.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef Communication052_h__
#define Communication052_h__

class Communication052 :
    public ITest
{
public:
	Communication052()  {	Dapp::instance().start();	}
	~Communication052()	{	Dapp::instance().stop();	}
    std::string getDescription() const;
    void run();
};

#endif /* Communication052_h__ */
