/*
 * Communication046.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef Communication046_h__
#define Communication046_h__

class Communication046 :
    public ITest
{
public:
	Communication046()  {	Dapp::instance().start();	}
	~Communication046()	{	Dapp::instance().stop();	}
    std::string getDescription() const;
    void run();
};

#endif /* Communication046_h__ */
