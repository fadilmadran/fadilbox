/*
 * Communication034b.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef Communication034b_h__
#define Communication034b_h__

class Communication034b :
    public ITest
{
public:
	Communication034b()  {	Dapp::instance().start();	}
	~Communication034b()	{	Dapp::instance().stop();	}
    std::string getDescription() const;
    void run();
};

#endif /* Communication034b_h__ */
