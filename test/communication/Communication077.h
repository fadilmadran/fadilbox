/*
 * Communication077.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef Communication077_h__
#define Communication077_h__

class Communication077 :
    public ITest
{
public:
	Communication077()  {	Dapp::instance().start();	}
	~Communication077()	{	Dapp::instance().stop();	}
    std::string getDescription() const;
    void run();
};

#endif /* Communication077_h__ */
