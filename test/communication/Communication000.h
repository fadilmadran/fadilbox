/*
 * Communication000.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef Communication000_h__
#define Communication000_h__

class Communication000 :
    public ITest
{
public:
	Communication000()  {	Dapp::instance().start();	}
	~Communication000()	{	Dapp::instance().stop();	}
    std::string getDescription() const;
    void run();
};

#endif /* Communication000_h__ */
