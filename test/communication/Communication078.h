/*
 * Communication078.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef Communication078_h__
#define Communication078_h__

class Communication078 :
    public ITest
{
public:
	Communication078()  {	Dapp::instance().start();	}
	~Communication078()	{	Dapp::instance().stop();	}
    std::string getDescription() const;
    void run();
};

#endif /* Communication078_h__ */
