/*
 * Communication039.cpp
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#include "stdafx.h"
#include "Dapp.h"
#include "Communication039.h"
#include "util.h"

std::string Communication039::getDescription() const
{
    return MKSTR("Communication039 - " << util::processName
    		<< "\n Bact,Eact,BeginDevice,EndDevice - AllValid (DeviceManager Running)");
}

void Communication039::run()
{
	std::cout << "Please wait. Loading... " << std::endl;
	xcp1::Message m;
	Dapp::instance().getMessage(m,20000);
	util::checkType(xcp::msg::act,m);


	std::string port 		= "0";
	std::string devid 		= "1";
	std::string read_period = "10000";
	//std::string devType 	= MKSTR(conf::dev::PING);

	xcp1::Message resp;

	// BeginDevice
	util::bact();
	util::eact();
	util::beginDevice(port,devid,read_period,resp);

	// EndDevice
	xcp1::Message req(xcp::msg::ed);
	req.addAttribute(xcp1::Attribute(xcp::attr::port).addValue(port));
	req.addAttribute(xcp1::Attribute(xcp::attr::devid).addValue(devid));

	Dapp::instance().write2croptrol(req);
	Dapp::instance().getMessage(resp);

	util::checkType(xcp::msg::ed+1,resp);
	util::checkAttribute(xcp::attr::rslt,resp,"0");
	util::checkAttribute(xcp::attr::hwaddr,resp, util::phsyHwaddrTest);


	Dapp::instance().isQueueEmpty();
}
