/*
 * Communication007.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef Communication007_h__
#define Communication007_h__

class Communication007 :
    public ITest
{
public:
	Communication007()  {	Dapp::instance().start();	}
	~Communication007()	{	Dapp::instance().stop();	}
    std::string getDescription() const;
    void run();
};

#endif /* Communication007_h__ */
