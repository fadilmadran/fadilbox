/*
 * Communication001.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef Communication001_h__
#define Communication001_h__

class Communication001 :
    public ITest
{
public:
	Communication001()  {	Dapp::instance().start();	}
	~Communication001()	{	Dapp::instance().stop();	}
    std::string getDescription() const;
    void run();
};

#endif /* Communication001_h__ */
