/*
 * Communication043.cpp
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#include "stdafx.h"
#include "Dapp.h"
#include "Communication043.h"
#include "util.h"

std::string Communication043::getDescription() const
{
    return MKSTR("Communication043 - " << util::processName
    		<< "\n BeginDevice,EndDevice - Invalid Port");
}

void Communication043::run()
{
	std::cout << "Please wait. Loading... " << std::endl;
	xcp1::Message m;
	Dapp::instance().getMessage(m,20000);
	util::checkType(xcp::msg::act,m);

	//Test//

	std::string port 		= "0";
	std::string devid 		= "0";
	std::string read_period = "10000";
	std::string devType 	= MKSTR(conf::dev::PING);

	// BeginDevice

	xcp1::Message resp;
	util::beginDevice(port,devid,read_period,resp);


	// EndDevice

	port="5"; // Invalid Port

	xcp1::Message req(xcp::msg::ed);
	req.addAttribute(xcp1::Attribute(xcp::attr::port).addValue(port));
	req.addAttribute(xcp1::Attribute(xcp::attr::devid).addValue(devid));

	Dapp::instance().write2croptrol(req);
	Dapp::instance().getMessage(resp);

	util::checkType(xcp::msg::ed+1,resp);
	util::checkAttribute(xcp::attr::rslt,resp,"2");
	util::checkAttribute(xcp::attr::hwaddr,resp, util::phsyHwaddrTest);


	Dapp::instance().isQueueEmpty();
}
