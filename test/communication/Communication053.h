/*
 * Communication053.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef Communication053_h__
#define Communication053_h__

class Communication053 :
    public ITest
{
public:
	Communication053()  {	Dapp::instance().start();	}
	~Communication053()	{	Dapp::instance().stop();	}
    std::string getDescription() const;
    void run();
};

#endif /* Communication053_h__ */
