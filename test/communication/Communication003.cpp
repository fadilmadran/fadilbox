/*
 * Communication003.cpp
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#include "stdafx.h"
#include "Dapp.h"
#include "Communication003.h"
#include "util.h"

std::string Communication003::getDescription() const
{
    return MKSTR("Communication003 - " << util::processName
    		<< "\n Bact before ActRequest");
}

void Communication003::run()
{
	//Test//

	xcp1::Message req(xcp::msg::bact);
	Dapp::instance().write2croptrol(req);
	xcp1::Message resp;
	Dapp::instance().getMessage(resp);
	util::checkType(xcp::msg::act, resp);

	Dapp::instance().getMessage(resp);
	util::checkType(xcp::msg::bact+1, resp);
	util::checkAttribute(xcp::attr::rslt, resp, "0");

	Dapp::instance().isQueueEmpty();
}
