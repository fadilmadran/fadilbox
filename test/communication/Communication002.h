/*
 * Communication002.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef Communication002_h__
#define Communication002_h__

class Communication002 :
    public ITest
{
public:
	Communication002()  {	Dapp::instance().start();	}
	~Communication002()	{	Dapp::instance().stop();	}
    std::string getDescription() const;
    void run();
};

#endif /* Communication002_h__ */
