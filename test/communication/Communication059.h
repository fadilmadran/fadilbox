/*
 * Communication059.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef Communication059_h__
#define Communication059_h__

class Communication059 :
    public ITest
{
public:
	Communication059()  {	Dapp::instance().start();	}
	~Communication059()	{	Dapp::instance().stop();	}
    std::string getDescription() const;
    void run();
};

#endif /* Communication059_h__ */
