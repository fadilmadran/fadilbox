/*
 * Communication040.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef Communication040_h__
#define Communication040_h__

class Communication040 :
    public ITest
{
public:
	Communication040()  {	Dapp::instance().start();	}
	~Communication040()	{	Dapp::instance().stop();	}
    std::string getDescription() const;
    void run();
};

#endif /* Communication040_h__ */
