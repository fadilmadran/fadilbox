/*
 * Communication076a.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef Communication076a_h__
#define Communication076a_h__

class Communication076a :
    public ITest
{
public:
	Communication076a()  {	Dapp::instance().start();	}
	~Communication076a()	{	Dapp::instance().stop();	}
    std::string getDescription() const;
    void run();
};

#endif /* Communication076a_h__ */
