/*
 * Communication060.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef Communication060_h__
#define Communication060_h__

class Communication060 :
    public ITest
{
public:
	Communication060()  {	Dapp::instance().start();	}
	~Communication060()	{	Dapp::instance().stop();	}
    std::string getDescription() const;
    void run();
};

#endif /* Communication060_h__ */
