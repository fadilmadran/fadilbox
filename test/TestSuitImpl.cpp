/*
 * TestSuitImpl.cpp
 *
 *  Created on: May 8, 2014
 *      Author: bulentk
 *
 */

#include "stdafx.h"
#include "TestSuitImpl.h"
#include "Dapp.h"
#include "testcases.h"

TestSuitImpl::TestSuitImpl()
{

	/*elw::lang::ClassFactory::Registrar<ITest, Communication001>("communication1");
	elw::lang::ClassFactory::Registrar<ITest, Communication002>("communication2");
	elw::lang::ClassFactory::Registrar<ITest, Communication003>("communication3");
	elw::lang::ClassFactory::Registrar<ITest, Communication004>("communication4");
	elw::lang::ClassFactory::Registrar<ITest, Communication005>("communication5");
	elw::lang::ClassFactory::Registrar<ITest, Communication006>("communication6");
	elw::lang::ClassFactory::Registrar<ITest, Communication007>("communication7");
	elw::lang::ClassFactory::Registrar<ITest, Communication008>("communication8");
	elw::lang::ClassFactory::Registrar<ITest, Communication009>("communication9");
	elw::lang::ClassFactory::Registrar<ITest, Communication010>("communication10");
	elw::lang::ClassFactory::Registrar<ITest, Communication031>("communication11");
	elw::lang::ClassFactory::Registrar<ITest, Communication032>("communication12");
	elw::lang::ClassFactory::Registrar<ITest, Communication033>("communication13");
	elw::lang::ClassFactory::Registrar<ITest, Communication034a>("communication14");
	elw::lang::ClassFactory::Registrar<ITest, Communication034b>("communication15");
	elw::lang::ClassFactory::Registrar<ITest, Communication035>("communication16");
	elw::lang::ClassFactory::Registrar<ITest, Communication036b>("communication17");
	elw::lang::ClassFactory::Registrar<ITest, Communication037>("communication18");
	elw::lang::ClassFactory::Registrar<ITest, Communication038>("communication19");
	elw::lang::ClassFactory::Registrar<ITest, Communication039>("communication20");
	elw::lang::ClassFactory::Registrar<ITest, Communication040>("communication21");
	elw::lang::ClassFactory::Registrar<ITest, Communication041>("communication22");
	elw::lang::ClassFactory::Registrar<ITest, Communication042>("communication23");
	elw::lang::ClassFactory::Registrar<ITest, Communication043>("communication24");
	elw::lang::ClassFactory::Registrar<ITest, Communication044>("communication25");
	elw::lang::ClassFactory::Registrar<ITest, Communication045>("communication26");
	elw::lang::ClassFactory::Registrar<ITest, Communication046>("communication27");
	elw::lang::ClassFactory::Registrar<ITest, Communication047>("communication28");
	elw::lang::ClassFactory::Registrar<ITest, Communication048a>("communication29");
	elw::lang::ClassFactory::Registrar<ITest, Communication049>("communication30");
	elw::lang::ClassFactory::Registrar<ITest, Communication050>("communication31");
	elw::lang::ClassFactory::Registrar<ITest, Communication052>("communication32");
	elw::lang::ClassFactory::Registrar<ITest, Communication053>("communication33");
	elw::lang::ClassFactory::Registrar<ITest, Communication054>("communication34");
	elw::lang::ClassFactory::Registrar<ITest, Communication055>("communication35");
	elw::lang::ClassFactory::Registrar<ITest, Communication056>("communication36");
	elw::lang::ClassFactory::Registrar<ITest, Communication057>("communication37");

	elw::lang::ClassFactory::Registrar<ITest, Communication059>("communication39");
	elw::lang::ClassFactory::Registrar<ITest, Communication060>("communication40");
	elw::lang::ClassFactory::Registrar<ITest, Communication061>("communication41");
	elw::lang::ClassFactory::Registrar<ITest, Communication064>("communication43");
	elw::lang::ClassFactory::Registrar<ITest, Communication065>("communication44");
	elw::lang::ClassFactory::Registrar<ITest, Communication068>("communication45");
	elw::lang::ClassFactory::Registrar<ITest, Communication069>("communication46");
	elw::lang::ClassFactory::Registrar<ITest, Communication070>("communication47");

	elw::lang::ClassFactory::Registrar<ITest, Communication073>("communication49");
	elw::lang::ClassFactory::Registrar<ITest, Communication074>("communication50");
	elw::lang::ClassFactory::Registrar<ITest, Communication075>("communication51");
	elw::lang::ClassFactory::Registrar<ITest, Communication076a>("communication52");
	elw::lang::ClassFactory::Registrar<ITest, Communication077>("communication53");
	elw::lang::ClassFactory::Registrar<ITest, Communication078>("communication54");
	elw::lang::ClassFactory::Registrar<ITest, Communication079>("communication55");

	elw::lang::ClassFactory::Registrar<ITest, Communication082>("communication57");
	elw::lang::ClassFactory::Registrar<ITest, Communication083>("communication58");
	elw::lang::ClassFactory::Registrar<ITest, Communication084>("communication59");
	elw::lang::ClassFactory::Registrar<ITest, Communication085>("communication60");
	elw::lang::ClassFactory::Registrar<ITest, Communication086>("communication61");
	elw::lang::ClassFactory::Registrar<ITest, Communication087>("communication62");

	elw::lang::ClassFactory::Registrar<ITest, Communication089>("communication64");
	elw::lang::ClassFactory::Registrar<ITest, Communication090>("communication65");
	elw::lang::ClassFactory::Registrar<ITest, Communication091>("communication66");
	elw::lang::ClassFactory::Registrar<ITest, Communication092>("communication67");
	elw::lang::ClassFactory::Registrar<ITest, Communication093>("communication68");
	elw::lang::ClassFactory::Registrar<ITest, Communication094>("communication69");*/


	elw::lang::ClassFactory::Registrar<ITest, Communication058>("communication1");
	elw::lang::ClassFactory::Registrar<ITest, Communication072>("communication2");
	elw::lang::ClassFactory::Registrar<ITest, Communication081>("communication3");
	elw::lang::ClassFactory::Registrar<ITest, Communication088>("communication4");
	elw::lang::ClassFactory::Registrar<ITest, Communication095>("communication5");

}

TestSuitImpl::~TestSuitImpl()
{
	Dapp::instance().stop();
    Dapp::release();
}

void TestSuitImpl::getScopes(std::vector<std::string>& lst)
{
    lst.push_back("communication");
}

REGISTER_CLASS("TestSuit", ITestSuit, TestSuitImpl);
