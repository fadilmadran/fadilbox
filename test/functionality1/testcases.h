/*
 * testcases.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef testcases_h__
#define testcases_h__


#include "croptrol/BeginDevice010.h"
#include "croptrol/BeginDevice011.h"
#include "croptrol/BeginDevice012.h"

#include "croptrol/BeginDevice020.h"
#include "croptrol/BeginDevice021.h"
#include "croptrol/BeginDevice022.h"

#include "croptrol/BeginDevice030.h"
#include "croptrol/BeginDevice031.h"
#include "croptrol/BeginDevice032.h"
#include "croptrol/BeginDevice033.h"

#include "croptrol/AddSensor010.h"
#include "croptrol/AddSensor011.h"
#include "croptrol/AddSensor012.h"
#include "croptrol/AddSensor013.h"
#include "croptrol/AddSensor014.h"
#include "croptrol/AddSensor015.h"

#include "croptrol/AddSensor020.h"
#include "croptrol/AddSensor021.h"
#include "croptrol/AddSensor022.h"
#include "croptrol/AddSensor023.h"
#include "croptrol/AddSensor024.h"

#include "croptrol/AddSensor030.h"
#include "croptrol/AddSensor031.h"

#include "croptrol/AddSensor040.h"
#include "croptrol/AddSensor041.h"

#include "croptrol/AddSensor050.h"

#include "croptrol/AddSensor060.h"

#include "croptrol/AddSensor070.h"
#include "croptrol/AddSensor071.h"
#include "croptrol/AddSensor072.h"

#include "croptrol/GetSensorValue010.h"
#include "croptrol/GetSensorValue011.h"
#include "croptrol/GetSensorValue012.h"
#include "croptrol/GetSensorValue013.h"
#include "croptrol/GetSensorValue014.h"

#include "croptrol/GetSensorValue020.h"
#include "croptrol/GetSensorValue021.h"

#include "croptrol/GetSensorValue030.h"

#include "croptrol/RemoveSensor010.h"
#include "croptrol/RemoveSensor011.h"
#include "croptrol/RemoveSensor012.h"
#include "croptrol/RemoveSensor013.h"
#include "croptrol/RemoveSensor014.h"

#include "croptrol/RemoveSensor020.h"
#include "croptrol/RemoveSensor021.h"

#include "croptrol/RemoveSensor030.h"

#include "croptrol/RemoveDevice010.h"
#include "croptrol/RemoveDevice011.h"
#include "croptrol/RemoveDevice012.h"
#include "croptrol/RemoveDevice013.h"

#include "croptrol/RemoveDevice020.h"

#include "croptrol/GetSensorValue040.h"
#include "croptrol/GetSensorValue041.h"
#include "croptrol/GetSensorValue042.h"

#include "croptrol/RemoveSensor040.h"
#include "croptrol/RemoveSensor041.h"
#include "croptrol/RemoveSensor042.h"

#endif /* testcases_h__ */
