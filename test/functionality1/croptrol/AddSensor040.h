/*
 * AddSensor040.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef AddSensor040_h__
#define AddSensor040_h__


#include "util.h"

class AddSensor040 :
    public ITest
{
public:
    std::string getDescription() const;
    void run();
};



#endif /* AddSensor040___ */
