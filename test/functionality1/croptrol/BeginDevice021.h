/*
 * BeginDevice021.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef BeginDevice021_h__
#define BeginDevice021_h__

class BeginDevice021 :
    public ITest
{
public:
    std::string getDescription() const;
    void run();
};

#endif /* BeginDevice021_h__ */
