/*
 * GetSensorValue041.cpp
 *
 *  Created on: May 12, 2014
 *      Author: bulentk
 *
 */

#include "stdafx.h"
#include "Dapp.h"
#include "GetSensorValue041.h"
#include "util.h"
#include "AddSensor030.h"

std::string GetSensorValue041::getDescription() const
{
    return "Monsrvr GSD request.IN RUNNING 	Device I Multiple Sensor by Valid and Invalid Ionum List";
}

void GetSensorValue041::run()
{
	std::vector<util::sDevice> lstAdded = util::getDeviceList("1");
	AddSensor030 add;
	add.run(lstAdded);

	std::vector<util::sDevice> lstAdded_Temp = lstAdded;
	std::vector<util::sDevice>::iterator it;

	for (it = lstAdded.begin() ; it != lstAdded.end() ; ++it)
	{
		(*it).ionum.set("13","Invalid");
		(*it).ionum.set("19","Invalid");

	    xcp1::Message resp;
	    util::getSensorDataList(
	    		(*it).port,
	    		(*it).devid,
	    		(*it).ionum,
	    		resp
	    		);

	    util::checkAttribute(xcp::attr::rslt, resp, "0");
	    util::checkAttribute(xcp::attr::port, resp, (*it).port);
	    util::checkAttribute(xcp::attr::devid, resp, (*it).devid);
	    util::checkAttributeList(xcp::attr::ionum, resp, (*it).ionum);

	}
	lstAdded = lstAdded_Temp;
}

