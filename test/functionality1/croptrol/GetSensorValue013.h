/*
 * GetSensorValue013.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef GetSensorValue013_h__
#define GetSensorValue013_h__

class GetSensorValue013 :
    public ITest
{
public:
    std::string getDescription() const;
    void run();
};

#endif /* GetSensorValue013_h__ */
