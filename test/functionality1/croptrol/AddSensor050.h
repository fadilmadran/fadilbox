/*
 * AddSensor050.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef AddSensor050_h__
#define AddSensor050_h__


#include "util.h"

class AddSensor050 :
    public ITest
{
public:
    std::string getDescription() const;
    void run();
    void run(std::vector<util::sDevice>& lstAdded);
};



#endif /* AddSensor050___ */
