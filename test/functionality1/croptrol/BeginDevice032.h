/*
 * BeginDevice032.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef BeginDevice032_h__
#define BeginDevice032_h__

class BeginDevice032 :
    public ITest
{
public:
    std::string getDescription() const;
    void run();
};

#endif /* BeginDevice032_h__ */
