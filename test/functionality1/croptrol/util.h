/*
 * croptrol_util.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef croptrol_util_h__
#define croptrol_util_h__

namespace util {

void reset();
void bact();
void eact();

void  beginDevice(
		std::string const& devType,
		std::string const& port,
		xcp1::Message& resp
		);

void  removeDevice(
		std::string const& port,
		std::string const& devid,
		xcp1::Message& resp
		);

void endDevice();

void addSensor(
		std::string const& port,
		std::string const& devid,
		std::string const& ionum,
		std::string const& ip,
		xcp1::Message& resp
		);

void removeSensor(
		std::string const& port,
		std::string const& devid,
		std::string const& ionum,
		xcp1::Message& resp
		);

void getSensorData(
		std::string const& port,
		std::string const& devid,
		std::string const& ionum,
		xcp1::Message& resp
		);

void waitForNewSensorData(
		xcp1::Message& resp,
        dword dwWait
        );


bool isActive(
        std::list<elw::util::Properties> const& lst,
        std::string const& ionum
        );

bool isActive(
        std::vector<std::string> const& ionums
        );

bool isActive(
        std::string const& ionum
        );

void checkAttribute(
        int i,
        xcp1::Message const& m,
        std::string const& val
        );

void getSensorDataList(
		std::string const& port,
		std::string const& devid,
		elw::util::Properties ionum,
		xcp1::Message& resp
		);

void removeSensorList(
		std::string const& port,
		std::string const& devid,
		elw::util::Properties ionum,
		xcp1::Message& resp
		);

void checkAttributeList(
		int i,
		xcp1::Message const& m,
		elw::util::Properties const& val
		);

struct sDevice
{
	std::string devType;
	std::string name;

	std::string port;
	std::string devid;
	std::string first_ionum;
	elw::util::Properties ionum; // ionum, oid
	int number_ionum;
};

std::vector<sDevice> getDeviceList (
		std::string const& device = "0"
		);

};



#endif /* croptrol_util_h__ */
