/*
 * BeginDevice011.cpp
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#include "stdafx.h"
#include "Dapp.h"
#include "BeginDevice011.h"
#include "util.h"

std::string BeginDevice011::getDescription() const
{
    return "Monsrvr BeginDevice request. IN ACTIVATION 	Device II";
}

void BeginDevice011::run()
{
	Dapp::instance().reset();
    util::bact();
	std::vector<util::sDevice> lstAdded = util::getDeviceList("2");

    std::string expected_devid = "1";

    std::string devType = lstAdded[0].devType;
    std::string port =    lstAdded[0].port;
    xcp1::Message resp;
    util::beginDevice(
    					devType,
    					port,
    					resp);

    util::checkAttribute(xcp::attr::rslt, resp, "0"); // success
    util::checkAttribute(xcp::attr::port, resp, port);
    util::checkAttribute(xcp::attr::devid, resp, expected_devid);


    std::string devid = resp.getAttribute(xcp::attr::devid).getValues()[0];

	util::endDevice();

	util::eact();

}
