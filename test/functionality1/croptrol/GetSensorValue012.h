/*
 * GetSensorValue012.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef GetSensorValue012_h__
#define GetSensorValue012_h__

class GetSensorValue012 :
    public ITest
{
public:
    std::string getDescription() const;
    void run();
};

#endif /* GetSensorValue012_h__ */
