/*
 * RemoveSensor040.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef RemoveSensor040_h__
#define RemoveSensor040_h__

class RemoveSensor040 :
    public ITest
{
public:
    std::string getDescription() const;
    void run();
};

#endif /* RemoveSensor040_h__ */
