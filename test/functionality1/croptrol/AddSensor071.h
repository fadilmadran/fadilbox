/*
 * AddSensor071.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef AddSensor071_h__
#define AddSensor071_h__

class AddSensor071 :
    public ITest
{
public:
    std::string getDescription() const;
    void run();
};

#endif /* AddSensor071_h__ */
