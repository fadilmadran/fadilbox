/*
 * GetSensorValue030.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef GetSensorValue030_h__
#define GetSensorValue030_h__

class GetSensorValue030 :
    public ITest
{
public:
    std::string getDescription() const;
    void run();
};

#endif /* GetSensorValue030_h__ */
