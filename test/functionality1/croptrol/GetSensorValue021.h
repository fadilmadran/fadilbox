/*
 * GetSensorValue021.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef GetSensorValue021_h__
#define GetSensorValue021_h__

class GetSensorValue021 :
    public ITest
{
public:
    std::string getDescription() const;
    void run();
};

#endif /* GetSensorValue021_h__ */
