/*
 * AddSensor070.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef AddSensor070_h__
#define AddSensor070_h__

class AddSensor070 :
    public ITest
{
public:
    std::string getDescription() const;
    void run();
};

#endif /* AddSensor070_h__ */
