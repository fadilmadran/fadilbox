/*
 * RemoveSensor010.cpp
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#include "stdafx.h"
#include "Dapp.h"
#include "RemoveSensor010.h"
#include "util.h"
#include "AddSensor010.h"

std::string RemoveSensor010::getDescription() const
{
    return "Monsrvr RS request.IN RUNNING 	Device I";
}

void RemoveSensor010::run()
{
	AddSensor010 add;
	add.run();
	std::vector<util::sDevice> lstAdded = util::getDeviceList("1");

    xcp1::Message resp;
    std::string port = lstAdded[0].port;
    std::string devid = "1";
    std::string ionum = lstAdded[0].first_ionum;
    util::removeSensor(
    		port,
    		devid,
    		ionum,
    		resp);

    util::checkAttribute(xcp::attr::rslt, resp, "0");
    util::checkAttribute(xcp::attr::port, resp, port);
    util::checkAttribute(xcp::attr::devid, resp, devid);
    util::checkAttribute(xcp::attr::ionum, resp, ionum);

    util::getSensorData(
    		port,
    		devid,
    		ionum,
    		resp);
    util::checkAttribute(xcp::attr::rslt, resp, "1");

}

