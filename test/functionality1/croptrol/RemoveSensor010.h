/*
 * RemoveSensor010.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef RemoveSensor010_h__
#define RemoveSensor010_h__

class RemoveSensor010 :
    public ITest
{
public:
    std::string getDescription() const;
    void run();
};

#endif /* RemoveSensor010_h__ */
