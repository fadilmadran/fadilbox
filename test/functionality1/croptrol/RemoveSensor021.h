/*
 * RemoveSensor021.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef RemoveSensor021_h__
#define RemoveSensor021_h__

class RemoveSensor021 :
    public ITest
{
public:
    std::string getDescription() const;
    void run();
};

#endif /* RemoveSensor021_h__ */
