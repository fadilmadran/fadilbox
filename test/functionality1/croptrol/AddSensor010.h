/*
 * AddSensor010.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef AddSensor010_h__
#define AddSensor010_h__

class AddSensor010 :
    public ITest
{
public:
    std::string getDescription() const;
    void run();
};

#endif /* AddSensor010_h__ */
