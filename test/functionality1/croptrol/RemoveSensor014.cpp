/*
 * RemoveSensor014.cpp
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#include "stdafx.h"
#include "Dapp.h"
#include "RemoveSensor014.h"
#include "util.h"
#include "AddSensor010.h"

std::string RemoveSensor014::getDescription() const
{
    return "Monsrvr RS request.IN RUNNING  INVALID Ionum";
}

void RemoveSensor014::run()
{
	AddSensor010 add;
	add.run();
	std::vector<util::sDevice> lstAdded = util::getDeviceList("1");

    xcp1::Message resp;
    std::string port = lstAdded[0].port;
    std::string devid = "1";
    std::string ionum = lstAdded[0].first_ionum;
    util::removeSensor(port,
    		devid,
    		"5",
    		resp);

    util::checkAttribute(xcp::attr::rslt, resp, "1");
    util::checkAttribute(xcp::attr::error, resp, MKSTR(xcp::attr::ionum));

}

