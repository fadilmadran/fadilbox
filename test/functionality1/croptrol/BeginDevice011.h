/*
 * BeginDevice011.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef BeginDevice011_h__
#define BeginDevice011_h__

class BeginDevice011 :
    public ITest
{
public:
    std::string getDescription() const;
    void run();
};

#endif /* BeginDevice011_h__ */
