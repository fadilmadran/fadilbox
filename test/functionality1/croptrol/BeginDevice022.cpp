/*
 * BeginDevice022.cpp
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#include "stdafx.h"
#include "Dapp.h"
#include "BeginDevice022.h"
#include "util.h"

std::string BeginDevice022::getDescription() const
{
    return "Monsrvr BeginDevice request. IN RUNNING 	Invalid Port";
}

void BeginDevice022::run()
{
	Dapp::instance().reset();
    util::bact();
	std::vector<util::sDevice> lstAdded = util::getDeviceList("1");

    std::string expected_devid = "1";

    std::string devType = lstAdded[0].devType;
    std::string port =    lstAdded[0].port;
    xcp1::Message resp;
    util::beginDevice(
    					devType,
    					port,
    					resp);

    util::checkAttribute(xcp::attr::rslt, resp, "1");
    util::checkAttribute(xcp::attr::error, resp, MKSTR(xcp::attr::port));

}
