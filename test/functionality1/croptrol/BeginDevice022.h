/*
 * BeginDevice022.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef BeginDevice022_h__
#define BeginDevice022_h__

class BeginDevice022 :
    public ITest
{
public:
    std::string getDescription() const;
    void run();
};

#endif /* BeginDevice022_h__ */
