/*
 * AddSensor014.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef AddSensor014_h__
#define AddSensor014_h__

class AddSensor014 :
    public ITest
{
public:
    std::string getDescription() const;
    void run();
};

#endif /* AddSensor014_h__ */
