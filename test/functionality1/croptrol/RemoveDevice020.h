/*
 * RemoveDevice020.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef RemoveDevice020_h__
#define RemoveDevice020_h__

class RemoveDevice020 :
    public ITest
{
public:
    std::string getDescription() const;
    void run();
};

#endif /* RemoveDevice020_h__ */
