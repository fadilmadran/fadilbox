/*
 * AddSensor011.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef AddSensor011_h__
#define AddSensor011_h__

class AddSensor011 :
    public ITest
{
public:
    std::string getDescription() const;
    void run();
};

#endif /* AddSensor011_h__ */
