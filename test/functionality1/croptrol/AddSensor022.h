/*
 * AddSensor022.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef AddSensor022_h__
#define AddSensor022_h__

class AddSensor022 :
    public ITest
{
public:
    std::string getDescription() const;
    void run();
};

#endif /* AddSensor022_h__ */
