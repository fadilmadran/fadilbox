/*
 * RemoveSensor013.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef RemoveSensor013_h__
#define RemoveSensor013_h__

class RemoveSensor013 :
    public ITest
{
public:
    std::string getDescription() const;
    void run();
};

#endif /* RemoveSensor013_h__ */
