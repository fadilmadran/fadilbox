/*
 * AddSensor021.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef AddSensor021_h__
#define AddSensor021_h__

class AddSensor021 :
    public ITest
{
public:
    std::string getDescription() const;
    void run();
};

#endif /* AddSensor021_h__ */
