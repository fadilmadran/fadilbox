/*
 * RemoveSensor012.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef RemoveSensor012_h__
#define RemoveSensor012_h__

class RemoveSensor012 :
    public ITest
{
public:
    std::string getDescription() const;
    void run();
};

#endif /* RemoveSensor012_h__ */
