/*
 * GetSensorValue014.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef GetSensorValue014_h__
#define GetSensorValue014_h__

class GetSensorValue014 :
    public ITest
{
public:
    std::string getDescription() const;
    void run();
};

#endif /* GetSensorValue014_h__ */
