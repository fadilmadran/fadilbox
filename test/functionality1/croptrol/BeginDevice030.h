/*
 * BeginDevice030.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef BeginDevice030_h__
#define BeginDevice030_h__

class BeginDevice030 :
    public ITest
{
public:
    std::string getDescription() const;
    void run();
};

#endif /* BeginDevice030_h__ */
