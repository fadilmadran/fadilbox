/*
 * BeginDevice012.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef BeginDevice012_h__
#define BeginDevice012_h__

class BeginDevice012 :
    public ITest
{
public:
    std::string getDescription() const;
    void run();
};

#endif /* BeginDevice012_h__ */
