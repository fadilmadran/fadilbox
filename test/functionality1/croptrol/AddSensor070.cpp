/*
 * AddSensor070.cpp
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#include "stdafx.h"
#include "Dapp.h"
#include "AddSensor070.h"
#include "util.h"

std::string AddSensor070::getDescription() const
{
    return "Monsrvr AS request.IN ACTIVATION 	Device I Multiple Sensor IN RUNNING		Device II Multiple Sensor";
}

void AddSensor070::run()
{
	Dapp::instance().reset();
    util::bact();

	std::vector<util::sDevice> lstAdded = util::getDeviceList();

	std::vector<util::sDevice>::iterator it;
	for (it = lstAdded.begin() ; it != lstAdded.end() ; ++it)
	{
		if ((*it).name == "DeviceI")
		{
			xcp1::Message resp;
		    util::beginDevice(	(*it).devType,
								(*it).port,
		    					resp);

			util::checkAttribute(xcp::attr::rslt, resp, "0"); // success

			(*it).devid = resp.getAttribute(xcp::attr::devid).getValues()[0];

			std::list<std::string> tempKeys = (*it).ionum.getKeys();
			std::list<std::string>::iterator it2;
			for(it2 = tempKeys.begin(); it2 != tempKeys.end(); it2++)
			{
			    util::addSensor((*it).port,
								(*it).devid,
								(*it2).data() ,
								(*it).ionum.get((*it2).data()),
			    				resp);

			    util::checkAttribute(xcp::attr::rslt, resp, "0"); // success
			    util::checkAttribute(xcp::attr::port, resp, (*it).port);
			    util::checkAttribute(xcp::attr::devid, resp, (*it).devid);
			    util::checkAttribute(xcp::attr::ionum, resp, (*it2).data() );

			}

			util::endDevice();
		}
	}

	util::eact();

	for (it = lstAdded.begin() ; it != lstAdded.end() ; ++it)
	{
		if ((*it).name == "DeviceII")
		{

			xcp1::Message resp;
		    util::beginDevice(	(*it).devType,
								(*it).port,
		    					resp);

			util::checkAttribute(xcp::attr::rslt, resp, "0"); // success

			(*it).devid = resp.getAttribute(xcp::attr::devid).getValues()[0];

			std::list<std::string> tempKeys = (*it).ionum.getKeys();
			std::list<std::string>::iterator it2;
			for(it2 = tempKeys.begin(); it2 != tempKeys.end(); it2++)
			{
			    util::addSensor((*it).port,
								(*it).devid,
								(*it2).data() ,
								(*it).ionum.get((*it2).data()),
			    				resp);

			    util::checkAttribute(xcp::attr::rslt, resp, "0"); // success
			    util::checkAttribute(xcp::attr::port, resp, (*it).port);
			    util::checkAttribute(xcp::attr::devid, resp, (*it).devid);
			    util::checkAttribute(xcp::attr::ionum, resp, (*it2).data() );

			}

			util::endDevice();
		}

	}


	std::vector<util::sDevice> lstAdded_Temp = lstAdded;
	while(lstAdded.empty() != 1)
	{

		xcp1::Message resp;
	    util::waitForNewSensorData(
	    		resp,
	    		12000);

		for (it = lstAdded.begin() ; it != lstAdded.end() ; ++it) // check ionum
		{
			if( (*it).devid == resp.getAttribute(xcp::attr::devid).getValues()[0]
			      &&  (*it).ionum.hasKey( resp.getAttribute(xcp::attr::ionum).getValues()[0]) )
			{
				(*it).ionum.remove( resp.getAttribute(xcp::attr::ionum).getValues()[0] );
			}

		}

		for (it = lstAdded.begin() ; it != lstAdded.end() ; ++it) // check device ionum list empty
		{
			std::list<std::string> tempKeysss = (*it).ionum.getKeys();
			if(tempKeysss.empty())
				break;
		}

		if(it != lstAdded.end())
		{
			lstAdded.erase (it);
		}
	}

	lstAdded = lstAdded_Temp;
}
