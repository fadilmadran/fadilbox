/*
 * BeginDevice010.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef BeginDevice010_h__
#define BeginDevice010_h__

class BeginDevice010 :
    public ITest
{
public:
    std::string getDescription() const;
    void run();
};

#endif /* BeginDevice010_h__ */
