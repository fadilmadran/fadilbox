/*
 * RemoveSensor021.cpp
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#include "stdafx.h"
#include "Dapp.h"
#include "RemoveSensor021.h"
#include "util.h"
#include "AddSensor031.h"

std::string RemoveSensor021::getDescription() const
{
    return "Monsrvr RS request.IN RUNNING 	Device II Multiple Sensor";
}

void RemoveSensor021::run()
{
	std::vector<util::sDevice> lstAdded = util::getDeviceList("2");
	AddSensor031 add;
	add.run(lstAdded);

	std::vector<util::sDevice> lstAdded_Temp = lstAdded;
	std::vector<util::sDevice>::iterator it;

	for (it = lstAdded.begin() ; it != lstAdded.end() ; ++it)
	{
		std::list<std::string> tempKeys = (*it).ionum.getKeys();
		std::list<std::string>::iterator it2;
		for(it2 = tempKeys.begin(); it2 != tempKeys.end(); it2++)
		{
		    xcp1::Message resp;
		    util::removeSensor(
		    		(*it).port,
		    		(*it).devid,
		    		(*it2).data(),
		    		resp);

		    util::checkAttribute(xcp::attr::rslt, resp, "0");
		    util::checkAttribute(xcp::attr::port, resp, (*it).port);
		    util::checkAttribute(xcp::attr::devid, resp, (*it).devid);
		    util::checkAttribute(xcp::attr::ionum, resp, (*it2).data());
		}

	}
	for (it = lstAdded.begin() ; it != lstAdded.end() ; ++it)
	{
		std::list<std::string> tempKeys = (*it).ionum.getKeys();
		std::list<std::string>::iterator it2;
		for(it2 = tempKeys.begin(); it2 != tempKeys.end(); it2++)
		{
		    xcp1::Message resp;
		    util::getSensorData(
		    		(*it).port,
		    		(*it).devid,
		    		(*it2).data(),
		    		resp);

		    util::checkAttribute(xcp::attr::rslt, resp, "1");
		}

	}

	lstAdded = lstAdded_Temp;
}

