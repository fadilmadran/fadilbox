/*
 * RemoveSensor042.cpp
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#include "stdafx.h"
#include "Dapp.h"
#include "RemoveSensor042.h"
#include "util.h"
#include "AddSensor030.h"

std::string RemoveSensor042::getDescription() const
{
    return "Monsrvr RS request.IN RUNNING 	Device I Multiple Sensor by Invalid Ionum List";
}

void RemoveSensor042::run()
{
	std::vector<util::sDevice> lstAdded = util::getDeviceList("1");
	AddSensor030 add;
	add.run(lstAdded);

	elw::util::Properties invalid_ionum;
	invalid_ionum.set("13","Invalid");
	invalid_ionum.set("17","Invalid");
	invalid_ionum.set("19","Invalid");
	invalid_ionum.set("23","Invalid");

	std::vector<util::sDevice> lstAdded_Temp = lstAdded;
	std::vector<util::sDevice>::iterator it;

	for (it = lstAdded.begin() ; it != lstAdded.end() ; ++it)
	{
	    xcp1::Message resp;
	    util::removeSensorList(
	    		(*it).port,
	    		(*it).devid,
	    		invalid_ionum,
	    		resp);

	    util::checkAttribute(xcp::attr::rslt, resp, "0");
	    util::checkAttribute(xcp::attr::port, resp, (*it).port);
	    util::checkAttribute(xcp::attr::devid, resp, (*it).devid);
	    util::checkAttributeList(xcp::attr::ionum, resp, invalid_ionum);
	}

	for (it = lstAdded.begin() ; it != lstAdded.end() ; ++it)
	{
		std::list<std::string> tempKeys = (*it).ionum.getKeys();
		std::list<std::string>::iterator it2;
		for(it2 = tempKeys.begin(); it2 != tempKeys.end(); it2++)
		{
		    xcp1::Message resp;
		    util::getSensorData(
		    		(*it).port,
		    		(*it).devid,
		    		(*it2).data(),
		    		resp);

		    util::checkAttribute(xcp::attr::rslt, resp, "0");
		}

	}

	lstAdded = lstAdded_Temp;
}

