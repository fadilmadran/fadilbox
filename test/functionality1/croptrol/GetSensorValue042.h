/*
 * GetSensorValue042.h
 *
 *  Created on: May 12, 2014
 *      Author: bulentk
 *
 */

#ifndef GetSensorValue042_h__
#define GetSensorValue042_h__

class GetSensorValue042 :
    public ITest
{
public:
    std::string getDescription() const;
    void run();
};

#endif /* GetSensorValue042_h__ */
