/*
 * AddSensor012.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef AddSensor012_h__
#define AddSensor012_h__

class AddSensor012 :
    public ITest
{
public:
    std::string getDescription() const;
    void run();
};

#endif /* AddSensor012_h__ */
