/*
 * AddSensor024.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef AddSensor024_h__
#define AddSensor024_h__

class AddSensor024 :
    public ITest
{
public:
    std::string getDescription() const;
    void run();
};

#endif /* AddSensor024_h__ */
