/*
 * AbstactTestcase.cpp
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#include "stdafx.h"
#include "util.h"
#include "Dapp.h"

namespace util {

void checkAttribute(
		int i,
		xcp1::Message const& m,
		std::string const& val
		)
{
    if (m.getAttribute(i).getValues().empty())
        throw elw::lang::Exception(MKSTR("Unexpected response. attr <" << i << "> is empty"));
    if (m.getAttribute(i).getValues()[0] != val)
        throw elw::lang::Exception(MKSTR("Unexpected response. attr <" << i << "> value is wrong. val:" << m.getAttribute(i).getValues()[0]));
}

bool isActive(std::list<elw::util::Properties> const& lst, std::string const& ionum)
{
    std::list<elw::util::Properties>::const_iterator it;
    for (it = lst.begin(); it != lst.end(); ++it)
    {
        if ((*it).get("ionum") == ionum)
        {
            return (*it).get<int>("active") == 1;
        }
    }

    throw elw::lang::Exception("Invalid ionum.");
}

bool isActive(std::string const& ionum)
{
    std::list<elw::util::Properties> lstFlash;
    FlashFile::read<flash::List<flash::Properties> >("sensors", lstFlash);

    return isActive(lstFlash, ionum);
}

bool isActive(std::vector<std::string> const& ionums)
{
    std::list<elw::util::Properties> lstFlash;
    FlashFile::read<flash::List<flash::Properties> >("sensors", lstFlash);

    std::vector<std::string>::const_iterator it;
    for (it = ionums.begin(); it != ionums.end(); ++it)
        if (!isActive(lstFlash, (*it)))
            return false;
    return true;
}

void reset()
{
    xcp1::Message req(xcp::msg::bact);
    Dapp::instance().write2croptrol(req);

    xcp1::Message resp;
    Dapp::instance().getMessage(resp);

    if (resp.getType() != xcp::msg::bact +1)
        throw elw::lang::Exception(MKSTR("Unexpected response. type:" << resp.getType()));;

    checkAttribute(xcp::attr::rslt, resp, "0");
    checkAttribute(xcp::attr::hwaddr, resp, "255;0;8;0");
}
void bact()
{
	xcp1::Message req(xcp::msg::bact);
	Dapp::instance().write2croptrol(req);
	xcp1::Message resp;
	Dapp::instance().getMessage(resp);

	if (resp.getType() != xcp::msg::bact +1)
		throw elw::lang::Exception(MKSTR("Unexpected response. type:" << resp.getType()));;

    checkAttribute(xcp::attr::rslt, resp, "0");
    checkAttribute(xcp::attr::hwaddr, resp, "255;0;8;0");
}

void  beginDevice(
		std::string const& devType,
		std::string const& port,
		xcp1::Message& resp
		)
{
	xcp1::Message req(xcp::msg::bd);
	req.addAttribute(xcp1::Attribute(xcp::attr::port).addValue(port));
	req.addAttribute(xcp1::Attribute(8).addValue(devType));

	Dapp::instance().write2croptrol(req);
	Dapp::instance().getMessage(resp);

	if (resp.getType() != xcp::msg::bd +1)
		throw elw::lang::Exception(MKSTR("Unexpected response. type:" << resp.getType()));

	checkAttribute(xcp::attr::hwaddr, resp, "255;0;8;0");
}

void  removeDevice(
		std::string const& port,
		std::string const& devid,
		xcp1::Message& resp
		)
{
	xcp1::Message req(xcp::msg::rd);
	req.addAttribute(xcp1::Attribute(xcp::attr::port).addValue(port));
	req.addAttribute(xcp1::Attribute(xcp::attr::devid).addValue(devid));

	Dapp::instance().write2croptrol(req);
	Dapp::instance().getMessage(resp);

	if (resp.getType() != xcp::msg::rd +1)
		throw elw::lang::Exception(MKSTR("Unexpected response. type:" << resp.getType()));

	checkAttribute(xcp::attr::hwaddr, resp, "255;0;8;0");
}

void endDevice()
{
	xcp1::Message req(xcp::msg::ed);

	Dapp::instance().write2croptrol(req);
	xcp1::Message resp;
	Dapp::instance().getMessage(resp);

	if (resp.getType() != xcp::msg::ed +1)
	throw elw::lang::Exception(MKSTR("Unexpected response. type:" << resp.getType()));;

	checkAttribute(xcp::attr::rslt, resp, "0");
	checkAttribute(xcp::attr::hwaddr, resp, "255;0;8;0");

}

void eact()
{
	xcp1::Message req(xcp::msg::eact);
	Dapp::instance().write2croptrol(req);
	xcp1::Message resp;
	Dapp::instance().getMessage(resp);

	if (resp.getType() != xcp::msg::eact +1)
		throw elw::lang::Exception(MKSTR("Unexpected response. type:" << resp.getType()));;

    checkAttribute(xcp::attr::rslt, resp, "0");
    checkAttribute(xcp::attr::hwaddr, resp, "255;0;8;0");
}

void addSensor(
		std::string const& port,
		std::string const& devid,
		std::string const& ionum,
		std::string const& ip,
		xcp1::Message& resp
		)
{
    xcp1::Message req(xcp::msg::as);
    req.addAttribute(xcp1::Attribute(xcp::attr::port).addValue(port));
    req.addAttribute(xcp1::Attribute(xcp::attr::devid).addValue(devid));
    req.addAttribute(xcp1::Attribute(xcp::attr::ionum).addValue(ionum));
    req.addAttribute(xcp1::Attribute(7).addValue(ip));

    Dapp::instance().write2croptrol(req);
    Dapp::instance().getMessage(resp);

    if (resp.getType() != xcp::msg::as+1)
        throw elw::lang::Exception(MKSTR("Unexpected response. type:" << resp.getType()));;

    checkAttribute(xcp::attr::hwaddr, resp, "255;0;8;0");
}

void removeSensor(
		std::string const& port,
		std::string const& devid,
		std::string const& ionum,
		xcp1::Message& resp
		)
{
	xcp1::Message req(xcp::msg::rs);
    req.addAttribute(xcp1::Attribute(xcp::attr::port).addValue(port));
    req.addAttribute(xcp1::Attribute(xcp::attr::devid).addValue(devid));
    req.addAttribute(xcp1::Attribute(xcp::attr::ionum).addValue(ionum));

    Dapp::instance().write2croptrol(req);
    Dapp::instance().getMessage(resp);

    if (resp.getType() != xcp::msg::rs+1)
        throw elw::lang::Exception(MKSTR("Unexpected response. type:" << resp.getType()));;

    checkAttribute(xcp::attr::hwaddr, resp, "255;0;8;0");
}




void getSensorData(
		std::string const& port,
		std::string const& devid,
		std::string const& ionum,
		xcp1::Message& resp
		)
	{
		xcp1::Message req(xcp::msg::gsd);
	    req.addAttribute(xcp1::Attribute(xcp::attr::port).addValue(port));
	    req.addAttribute(xcp1::Attribute(xcp::attr::devid).addValue(devid));
	    req.addAttribute(xcp1::Attribute(xcp::attr::ionum).addValue(ionum));

		Dapp::instance().write2croptrol(req);
		Dapp::instance().getMessage(resp);

		if (resp.getType() != xcp::msg::gsd+1)
			throw elw::lang::Exception(MKSTR("Unexpected response. type:" << resp.getType()));;

		checkAttribute(xcp::attr::hwaddr, resp, "255;0;8;0");

	    if (resp.getAttribute(xcp::attr::rslt).getValues()[0] == "0")
		{
		    if (resp.getAttribute(7).getValues().size() != 1)
		        throw elw::lang::Exception(MKSTR("Invalid value attr. size:" << resp.getAttribute(7).getValues().size()));
		    if (resp.getAttribute(8).getValues().size() != 1)
		        throw elw::lang::Exception(MKSTR("Invalid time attr. size:" << resp.getAttribute(8).getValues().size()));

		}
	}

void getSensorDataList(
		std::string const& port,
		std::string const& devid,
		elw::util::Properties ionum,
		xcp1::Message& resp
		)
	{
		xcp1::Message req(xcp::msg::gsd);
	    req.addAttribute(xcp1::Attribute(xcp::attr::port).addValue(port));
	    req.addAttribute(xcp1::Attribute(xcp::attr::devid).addValue(devid));

	    xcp1::Attribute io(xcp::attr::ionum);
	    std::list<std::string> tempKeys = ionum.getKeys();
	    		std::list<std::string>::iterator it;
	    		for(it = tempKeys.begin(); it != tempKeys.end(); it++)
	    		{
	    			io.addValue(*it);
	    		}
		req.addAttribute(io);

		Dapp::instance().write2croptrol(req);
		Dapp::instance().getMessage(resp);

		if (resp.getType() != xcp::msg::gsd+1)
			throw elw::lang::Exception(MKSTR("Unexpected response. type:" << resp.getType()));;

		checkAttribute(xcp::attr::hwaddr, resp, "255;0;8;0");

	    if (resp.getAttribute(xcp::attr::rslt).getValues()[0] == "0")
		{
		    if (resp.getAttribute(7).getValues().size() != 1)
		        throw elw::lang::Exception(MKSTR("Invalid value attr. size:" << resp.getAttribute(7).getValues().size()));
		    if (resp.getAttribute(8).getValues().size() != 1)
		        throw elw::lang::Exception(MKSTR("Invalid time attr. size:" << resp.getAttribute(8).getValues().size()));

		}
	}

void removeSensorList(
		std::string const& port,
		std::string const& devid,
		elw::util::Properties ionum,
		xcp1::Message& resp
		)
	{
		xcp1::Message req(xcp::msg::rs);
	    req.addAttribute(xcp1::Attribute(xcp::attr::port).addValue(port));
	    req.addAttribute(xcp1::Attribute(xcp::attr::devid).addValue(devid));

	    xcp1::Attribute io(xcp::attr::ionum);
	    std::list<std::string> tempKeys = ionum.getKeys();
	    		std::list<std::string>::iterator it;
	    		for(it = tempKeys.begin(); it != tempKeys.end(); it++)
	    		{
	    			io.addValue(*it);
	    		}
		req.addAttribute(io);

		Dapp::instance().write2croptrol(req);
		Dapp::instance().getMessage(resp);

		if (resp.getType() != xcp::msg::gsd+1)
			throw elw::lang::Exception(MKSTR("Unexpected response. type:" << resp.getType()));;

		checkAttribute(xcp::attr::hwaddr, resp, "255;0;8;0");
	}

void checkAttributeList(
		int i,
		xcp1::Message const& m,
		elw::util::Properties const& val
		)
{
    if (m.getAttribute(i).getValues().empty())
        throw elw::lang::Exception(MKSTR("Unexpected response. attr <" << i << "> is empty"));

	std::vector<std::string> const& val0 = m.getAttribute(i).getValues();
	elw::util::Properties p_val0;
	std::vector<std::string>::const_iterator it;
	for(it = val0.begin(); it != val0.end(); it++)
	{
		p_val0.set((*it),(*it));
	}

	std::list<std::string> tempKeys = val.getKeys();
	std::list<std::string>::iterator it2;
	for(it2 = tempKeys.begin(); it2 != tempKeys.end(); it2++)
	{
		if(val.get((*it2)) != "Invalid")
		{
			if(!p_val0.hasKey((*it2) ) )
				throw elw::lang::Exception(MKSTR("Unexpected response. attr <" << i << "> value is not found. val:" << (*it2) ) );
		}
		else
		{
			if(p_val0.hasKey((*it2) ) )
				throw elw::lang::Exception(MKSTR("Unexpected response. attr <" << i << "> value is excessive. val:" << (*it2) ) );
		}
		p_val0.remove((*it2));
	}
	tempKeys = p_val0.getKeys();
	if( tempKeys.size() != 0)
		throw elw::lang::Exception(MKSTR("Unexpected response. attr <" << i << "> value is excessive.") );

}

void changeSensorStatus(
	int  port,
	int  devid,
	int  ionum,
	std::string const& status,
	xcp1::Message& resp
	)
{
	xcp1::Message req(xcp::msg::css);
	req.addAttribute(xcp1::Attribute(xcp::attr::port).addValue(MKSTR(port)));
	req.addAttribute(xcp1::Attribute(xcp::attr::devid).addValue(MKSTR(devid)));
	req.addAttribute(xcp1::Attribute(xcp::attr::ionum).addValue(MKSTR(ionum)));
	req.addAttribute(xcp1::Attribute(7).addValue(status));

	Dapp::instance().write2croptrol(req);

	Dapp::instance().getMessage(resp);
	if (resp.getType() != xcp::msg::css+1)
		throw elw::lang::Exception(MKSTR("Unexpected response. type:" << resp.getType()));;

	checkAttribute(xcp::attr::hwaddr, resp, "255;0;4;0");
}

void waitForNewSensorData(
		xcp1::Message& resp,
		dword dwWait)
{
    Dapp::instance().getMessage(resp, dwWait);

    if (resp.getType() != xcp::msg::nsd)
        throw elw::lang::Exception(MKSTR("Unexpected response. type:" << resp.getType()));;

    checkAttribute(xcp::attr::hwaddr, resp, "255;0;8;0");

    if (resp.getAttribute(7).getValues().size() != 1)
        throw elw::lang::Exception(MKSTR("Invalid value attr. size:" << resp.getAttribute(7).getValues().size()));
    if (resp.getAttribute(8).getValues().size() != 1)
        throw elw::lang::Exception(MKSTR("Invalid time attr. size:" << resp.getAttribute(8).getValues().size()));
}

std::vector<sDevice> getDeviceList (std::string const& device)
{
	std::vector<sDevice> lstAdded;

	if (device == "0")
	{
		sDevice version_1;
		version_1.name = "DeviceI";
		version_1.devType = MKSTR(conf::dev::PING);
		version_1.port = "0";
		version_1.first_ionum = "0";


		version_1.ionum.set("0", "192.168.1.182");
		version_1.ionum.set("1", "192.168.1.136");
		version_1.ionum.set("2", "192.168.1.33");

		version_1.number_ionum = 3;

		lstAdded.push_back(version_1);

		sDevice version_2;
		version_2.name = "DeviceII";
		version_2.devType = MKSTR(conf::dev::TELNET);
		version_2.port = "0";
		version_2.first_ionum = "0";


		version_2.ionum.set("0", "192.168.1.182");
		version_2.ionum.set("1", "192.168.1.223");

		version_2.number_ionum = 2;

		lstAdded.push_back(version_2);

		return lstAdded;
	}
	else if (device == "1")
	{
		sDevice version_1;
		version_1.name = "DeviceI";
		version_1.devType = MKSTR(conf::dev::PING);
		version_1.port = "0";
		version_1.first_ionum = "0";


		version_1.ionum.set("0", "192.168.1.182");
		version_1.ionum.set("1", "192.168.1.136");
		version_1.ionum.set("2", "192.168.1.33");

		version_1.number_ionum = 3;

		lstAdded.push_back(version_1);
	}

	else if (device == "2")
	{
		sDevice version_2;
		version_2.name = "DeviceII";
		version_2.devType = MKSTR(conf::dev::TELNET);
		version_2.port = "0";
		version_2.first_ionum = "0";


		version_2.ionum.set("0", "192.168.1.182");
		version_2.ionum.set("1", "192.168.1.223");

		version_2.number_ionum = 2;

		lstAdded.push_back(version_2);
	}

	return lstAdded;
}

}


