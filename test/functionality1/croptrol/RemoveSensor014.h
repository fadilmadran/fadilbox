/*
 * RemoveSensor014.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef RemoveSensor014_h__
#define RemoveSensor014_h__

class RemoveSensor014 :
    public ITest
{
public:
    std::string getDescription() const;
    void run();
};

#endif /* RemoveSensor014_h__ */
