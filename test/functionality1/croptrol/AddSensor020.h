/*
 * AddSensor020.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef AddSensor020_h__
#define AddSensor020_h__

class AddSensor020 :
    public ITest
{
public:
    std::string getDescription() const;
    void run();
};

#endif /* AddSensor020_h__ */
