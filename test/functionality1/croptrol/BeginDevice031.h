/*
 * BeginDevice031.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef BeginDevice031_h__
#define BeginDevice031_h__

class BeginDevice031 :
    public ITest
{
public:
    std::string getDescription() const;
    void run();
};

#endif /* BeginDevice031_h__ */
