/*
 * RemoveSensor030.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef RemoveSensor030_h__
#define RemoveSensor030_h__

class RemoveSensor030 :
    public ITest
{
public:
    std::string getDescription() const;
    void run();
};

#endif /* RemoveSensor030_h__ */
