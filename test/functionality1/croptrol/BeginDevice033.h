/*
 * BeginDevice033.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef BeginDevice033_h__
#define BeginDevice033_h__

class BeginDevice033 :
    public ITest
{
public:
    std::string getDescription() const;
    void run();
};

#endif /* BeginDevice033_h__ */
