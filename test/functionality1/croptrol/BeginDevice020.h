/*
 * BeginDevice020.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef BeginDevice020_h__
#define BeginDevice020_h__

class BeginDevice020 :
    public ITest
{
public:
    std::string getDescription() const;
    void run();
};

#endif /* BeginDevice020_h__ */
