/*
 * AddSensor031.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef AddSensor031_h__
#define AddSensor031_h__


#include "util.h"

class AddSensor031 :
    public ITest
{
public:
    std::string getDescription() const;
    void run();
    void run(std::vector<util::sDevice>& lstAdded);
};



#endif /* AddSensor031___ */
