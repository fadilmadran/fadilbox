/*
 * GetSensorValue040.h
 *
 *  Created on: May 12, 2014
 *      Author: bulentk
 *
 */

#ifndef GetSensorValue040_h__
#define GetSensorValue040_h__

class GetSensorValue040 :
    public ITest
{
public:
    std::string getDescription() const;
    void run();
};

#endif /* GetSensorValue040_h__ */
