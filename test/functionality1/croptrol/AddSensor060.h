/*
 * AddSensor060.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef AddSensor060_h__
#define AddSensor060_h__

class AddSensor060 :
    public ITest
{
public:
    std::string getDescription() const;
    void run();
};

#endif /* AddSensor060_h__ */
