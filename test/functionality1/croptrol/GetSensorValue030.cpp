/*
 * GetSensorValue030.cpp
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#include "stdafx.h"
#include "Dapp.h"
#include "GetSensorValue030.h"
#include "util.h"
#include "AddSensor050.h"

std::string GetSensorValue030::getDescription() const
{
    return "Monsrvr GSD request.IN RUNNING 	Device I && Device II Multiple Sensor";
}

void GetSensorValue030::run()
{
	std::vector<util::sDevice> lstAdded = util::getDeviceList();
	AddSensor050 add;
	add.run(lstAdded);

	std::vector<util::sDevice> lstAdded_Temp = lstAdded;
	std::vector<util::sDevice>::iterator it;

	for (it = lstAdded.begin() ; it != lstAdded.end() ; ++it)
	{
		std::list<std::string> tempKeys = (*it).ionum.getKeys();
		std::list<std::string>::iterator it2;
		for(it2 = tempKeys.begin(); it2 != tempKeys.end(); it2++)
		{
		    xcp1::Message resp;
		    util::getSensorData(
		    		(*it).port,
		    		(*it).devid,
		    		(*it2).data(),
		    		resp);

		    util::checkAttribute(xcp::attr::rslt, resp, "0");
		    util::checkAttribute(xcp::attr::port, resp, (*it).port);
		    util::checkAttribute(xcp::attr::devid, resp, (*it).devid);
		    util::checkAttribute(xcp::attr::ionum, resp, (*it2).data());
		}

	}
	lstAdded = lstAdded_Temp;
}

