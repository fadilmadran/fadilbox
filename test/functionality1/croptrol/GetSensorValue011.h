/*
 * GetSensorValue011.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef GetSensorValue011_h__
#define GetSensorValue011_h__

class GetSensorValue011 :
    public ITest
{
public:
    std::string getDescription() const;
    void run();
};

#endif /* GetSensorValue011_h__ */
