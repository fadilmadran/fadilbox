/*
 * RemoveSensor041.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef RemoveSensor041_h__
#define RemoveSensor041_h__

class RemoveSensor041 :
    public ITest
{
public:
    std::string getDescription() const;
    void run();
};

#endif /* RemoveSensor041_h__ */
