/*
 * AddSensor041.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef AddSensor041_h__
#define AddSensor041_h__


#include "util.h"

class AddSensor041 :
    public ITest
{
public:
    std::string getDescription() const;
    void run();
};



#endif /* AddSensor041___ */
