/*
 * RemoveDevice012.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef RemoveDevice012_h__
#define RemoveDevice012_h__

class RemoveDevice012 :
    public ITest
{
public:
    std::string getDescription() const;
    void run();
};

#endif /* RemoveDevice012_h__ */
