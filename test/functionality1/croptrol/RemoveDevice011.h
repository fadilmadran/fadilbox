/*
 * RemoveDevice011.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef RemoveDevice011_h__
#define RemoveDevice011_h__

class RemoveDevice011 :
    public ITest
{
public:
    std::string getDescription() const;
    void run();
};

#endif /* RemoveDevice011_h__ */
