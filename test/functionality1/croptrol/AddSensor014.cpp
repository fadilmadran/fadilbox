/*
 * AddSensor014.cpp
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#include "stdafx.h"
#include "Dapp.h"
#include "AddSensor014.h"
#include "util.h"

std::string AddSensor014::getDescription() const
{
    return "Monsrvr AS request.IN ACTIVATION   INVALID Port";
}

void AddSensor014::run()
{
	Dapp::instance().reset();
    util::bact();
	std::vector<util::sDevice> lstAdded = util::getDeviceList("1");

    std::string devType = lstAdded[0].devType;
    std::string port =    lstAdded[0].port;
    xcp1::Message resp;
    util::beginDevice(
    					devType,
    					port,
    					resp);

    util::checkAttribute(xcp::attr::rslt, resp, "0"); // success

    std::string devid = resp.getAttribute(xcp::attr::devid).getValues()[0];
    std::string ionum = lstAdded[0].first_ionum;
    std::string oid = lstAdded[0].ionum.get(ionum);

    util::addSensor("5",
    				devid,
    				ionum ,
    				oid,
    				resp);

    util::checkAttribute(xcp::attr::rslt, resp, "1");
    util::checkAttribute(xcp::attr::error, resp, MKSTR(xcp::attr::port));

}
