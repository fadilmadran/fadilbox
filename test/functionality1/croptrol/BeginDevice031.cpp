/*
 * BeginDevice031.cpp
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#include "stdafx.h"
#include "Dapp.h"
#include "BeginDevice031.h"
#include "util.h"

std::string BeginDevice031::getDescription() const
{
    return "Monsrvr BeginDevice request. IN ACTIVATION 	Device I & IN RUNNING 		Device II";
}

void BeginDevice031::run()
{
	Dapp::instance().reset();
    util::bact();
	std::vector<util::sDevice> lstAdded = util::getDeviceList();

    std::string expected_devid = "1";

    std::string devType = lstAdded[0].devType;
    std::string port =    lstAdded[0].port;
    xcp1::Message resp;
    util::beginDevice(
    					devType,
    					port,
    					resp);

    util::checkAttribute(xcp::attr::rslt, resp, "0"); // success
    util::checkAttribute(xcp::attr::port, resp, port);
    util::checkAttribute(xcp::attr::devid, resp, expected_devid);


    std::string devid = resp.getAttribute(xcp::attr::devid).getValues()[0];

	util::endDevice();

	util::eact();

    expected_devid = "2";

    devType = lstAdded[1].devType;
    port =    lstAdded[1].port;
    util::beginDevice(
    					devType,
    					port,
    					resp);

    util::checkAttribute(xcp::attr::rslt, resp, "0"); // success
    util::checkAttribute(xcp::attr::port, resp, port);
    util::checkAttribute(xcp::attr::devid, resp, expected_devid);


    devid = resp.getAttribute(xcp::attr::devid).getValues()[0];

	util::endDevice();
}
