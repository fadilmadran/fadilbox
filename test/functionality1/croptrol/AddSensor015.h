/*
 * AddSensor015.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef AddSensor015_h__
#define AddSensor015_h__

class AddSensor015 :
    public ITest
{
public:
    std::string getDescription() const;
    void run();
};

#endif /* AddSensor015_h__ */
