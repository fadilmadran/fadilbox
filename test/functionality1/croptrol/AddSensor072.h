/*
 * AddSensor072.h
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#ifndef AddSensor072_h__
#define AddSensor072_h__

class AddSensor072 :
    public ITest
{
public:
    std::string getDescription() const;
    void run();
};

#endif /* AddSensor072_h__ */
