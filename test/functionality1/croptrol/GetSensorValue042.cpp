/*
 * GetSensorValue042.cpp
 *
 *  Created on: May 12, 2014
 *      Author: bulentk
 *
 */

#include "stdafx.h"
#include "Dapp.h"
#include "GetSensorValue042.h"
#include "util.h"
#include "AddSensor030.h"


std::string GetSensorValue042::getDescription() const
{
    return "Monsrvr GSD request.IN RUNNING 	Device I Multiple Sensor by Invalid Ionum List";
}

void GetSensorValue042::run()
{
	std::vector<util::sDevice> lstAdded = util::getDeviceList("1");
	AddSensor030 add;
	add.run(lstAdded);

	elw::util::Properties invalid_ionum;
	invalid_ionum.set("13","Invalid");
	invalid_ionum.set("17","Invalid");
	invalid_ionum.set("19","Invalid");
	invalid_ionum.set("23","Invalid");
	std::vector<util::sDevice> lstAdded_Temp = lstAdded;
	std::vector<util::sDevice>::iterator it;

	for (it = lstAdded.begin() ; it != lstAdded.end() ; ++it)
	{
	    xcp1::Message resp;
	    util::getSensorDataList(
	    		(*it).port,
	    		(*it).devid,
	    		invalid_ionum,
	    		resp
	    		);

	    util::checkAttribute(xcp::attr::rslt, resp, "0");
	    util::checkAttribute(xcp::attr::port, resp, (*it).port);
	    util::checkAttribute(xcp::attr::devid, resp, (*it).devid);
	    util::checkAttributeList(xcp::attr::ionum, resp, invalid_ionum);

	}
	lstAdded = lstAdded_Temp;
}

