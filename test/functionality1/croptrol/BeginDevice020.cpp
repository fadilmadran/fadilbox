/*
 * BeginDevice020.cpp
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#include "stdafx.h"
#include "Dapp.h"
#include "BeginDevice020.h"
#include "util.h"

std::string BeginDevice020::getDescription() const
{
    return "Monsrvr BeginDevice request. IN RUNNING 	Device I";
}

void BeginDevice020::run()
{
	Dapp::instance().reset();
    util::bact();
	util::eact();

	std::vector<util::sDevice> lstAdded = util::getDeviceList("1");

    std::string expected_devid = "1";

    std::string devType = lstAdded[0].devType;
    std::string port =    lstAdded[0].port;
    xcp1::Message resp;
    util::beginDevice(
    					devType,
    					port,
    					resp);

    util::checkAttribute(xcp::attr::rslt, resp, "0"); // success
    util::checkAttribute(xcp::attr::port, resp, port);
    util::checkAttribute(xcp::attr::devid, resp, expected_devid);


    std::string devid = resp.getAttribute(xcp::attr::devid).getValues()[0];

	util::endDevice();


}
