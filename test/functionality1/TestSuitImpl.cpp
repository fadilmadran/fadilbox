/*
 * TestSuitImpl.cpp
 *
 *  Created on: May 8, 2014
 *      Author: bulentk
 *
 */

#include "stdafx.h"
#include "TestSuitImpl.h"
#include "Dapp.h"
#include "testcases.h"

TestSuitImpl::TestSuitImpl()
{
	elw::lang::ClassFactory::Registrar<ITest, BeginDevice010>("croptrol1");
	elw::lang::ClassFactory::Registrar<ITest, BeginDevice011>("croptrol2");
	elw::lang::ClassFactory::Registrar<ITest, BeginDevice012>("croptrol3");

	elw::lang::ClassFactory::Registrar<ITest, BeginDevice020>("croptrol4");
	elw::lang::ClassFactory::Registrar<ITest, BeginDevice021>("croptrol5");
	elw::lang::ClassFactory::Registrar<ITest, BeginDevice022>("croptrol6");

	elw::lang::ClassFactory::Registrar<ITest, BeginDevice030>("croptrol7");
	elw::lang::ClassFactory::Registrar<ITest, BeginDevice031>("croptrol8");
	elw::lang::ClassFactory::Registrar<ITest, BeginDevice032>("croptrol9");
	elw::lang::ClassFactory::Registrar<ITest, BeginDevice033>("croptrol10");

	elw::lang::ClassFactory::Registrar<ITest, AddSensor010>("croptrol11");
	elw::lang::ClassFactory::Registrar<ITest, AddSensor011>("croptrol12");
	elw::lang::ClassFactory::Registrar<ITest, AddSensor012>("croptrol13");
	elw::lang::ClassFactory::Registrar<ITest, AddSensor013>("croptrol14");
	elw::lang::ClassFactory::Registrar<ITest, AddSensor014>("croptrol15");
	elw::lang::ClassFactory::Registrar<ITest, AddSensor015>("croptrol16");

	elw::lang::ClassFactory::Registrar<ITest, AddSensor020>("croptrol17");
	elw::lang::ClassFactory::Registrar<ITest, AddSensor021>("croptrol18");
	elw::lang::ClassFactory::Registrar<ITest, AddSensor022>("croptrol19");
	elw::lang::ClassFactory::Registrar<ITest, AddSensor023>("croptrol20");
	elw::lang::ClassFactory::Registrar<ITest, AddSensor024>("croptrol21");

	elw::lang::ClassFactory::Registrar<ITest, AddSensor030>("croptrol22");
	elw::lang::ClassFactory::Registrar<ITest, AddSensor031>("croptrol23");

	elw::lang::ClassFactory::Registrar<ITest, AddSensor040>("croptrol24");
	elw::lang::ClassFactory::Registrar<ITest, AddSensor041>("croptrol25");

	elw::lang::ClassFactory::Registrar<ITest, AddSensor050>("croptrol26");

	elw::lang::ClassFactory::Registrar<ITest, AddSensor060>("croptrol27");

	elw::lang::ClassFactory::Registrar<ITest, AddSensor070>("croptrol28");
	elw::lang::ClassFactory::Registrar<ITest, AddSensor071>("croptrol29");
	elw::lang::ClassFactory::Registrar<ITest, AddSensor072>("croptrol30");

	elw::lang::ClassFactory::Registrar<ITest, GetSensorValue010>("croptrol31");
	elw::lang::ClassFactory::Registrar<ITest, GetSensorValue011>("croptrol32");
	elw::lang::ClassFactory::Registrar<ITest, GetSensorValue012>("croptrol33");
	elw::lang::ClassFactory::Registrar<ITest, GetSensorValue013>("croptrol34");
	elw::lang::ClassFactory::Registrar<ITest, GetSensorValue014>("croptrol35");

	elw::lang::ClassFactory::Registrar<ITest, GetSensorValue020>("croptrol36");
	elw::lang::ClassFactory::Registrar<ITest, GetSensorValue021>("croptrol37");

	elw::lang::ClassFactory::Registrar<ITest, GetSensorValue030>("croptrol38");

	elw::lang::ClassFactory::Registrar<ITest, RemoveSensor010>("croptrol39");
	elw::lang::ClassFactory::Registrar<ITest, RemoveSensor011>("croptrol40");
	elw::lang::ClassFactory::Registrar<ITest, RemoveSensor012>("croptrol41");
	elw::lang::ClassFactory::Registrar<ITest, RemoveSensor013>("croptrol42");
	elw::lang::ClassFactory::Registrar<ITest, RemoveSensor014>("croptrol43");

	elw::lang::ClassFactory::Registrar<ITest, RemoveSensor020>("croptrol44");
	elw::lang::ClassFactory::Registrar<ITest, RemoveSensor021>("croptrol45");

	elw::lang::ClassFactory::Registrar<ITest, RemoveSensor030>("croptrol46");

	elw::lang::ClassFactory::Registrar<ITest, RemoveDevice010>("croptrol47");
	elw::lang::ClassFactory::Registrar<ITest, RemoveDevice011>("croptrol48");
	elw::lang::ClassFactory::Registrar<ITest, RemoveDevice012>("croptrol49");
	elw::lang::ClassFactory::Registrar<ITest, RemoveDevice013>("croptrol50");

	elw::lang::ClassFactory::Registrar<ITest, RemoveDevice020>("croptrol51");

	elw::lang::ClassFactory::Registrar<ITest, GetSensorValue040>("croptrol52");
	elw::lang::ClassFactory::Registrar<ITest, GetSensorValue041>("croptrol53");
	elw::lang::ClassFactory::Registrar<ITest, GetSensorValue042>("croptrol54");

	elw::lang::ClassFactory::Registrar<ITest, RemoveSensor040>("croptrol55");
	elw::lang::ClassFactory::Registrar<ITest, RemoveSensor041>("croptrol56");
	elw::lang::ClassFactory::Registrar<ITest, RemoveSensor042>("croptrol57");
}

TestSuitImpl::~TestSuitImpl()
{
//    Dapp::release();
}

void TestSuitImpl::getScopes(std::vector<std::string>& lst)
{
    lst.push_back("croptrol");
    lst.push_back("discovery");
}

REGISTER_CLASS("TestSuit", ITestSuit, TestSuitImpl);
