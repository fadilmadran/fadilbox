/*
 * Dapp.cpp
 *
 *  Created on: May 9, 2014
 *      Author: bulentk
 *
 */

#include "stdafx.h"
#include "Dapp.h"

extern "C" {
IDeviceApplication* create(int argc, char* argv[]);
}

elw::lang::SmartPtr<Dapp> Dapp::m_ptr;

Dapp& Dapp::instance()
{
	ENTEREXIT0();
    if (m_ptr == NULL)
        m_ptr = new Dapp();
    return *m_ptr;
}

void Dapp::release()
{
	ENTEREXIT0();
    if (m_ptr != NULL)
        m_ptr->stop();
    m_ptr = NULL;
}

Dapp::Dapp()
{
    __g_uDevBoxLogID__ = LOGGER().newModule(
            "Monsrvr",
            0
//           log::lvl::ext
            );

    LOGGER().addTarget("log::target::stdout");

    m_ptrWriter = new WriterImpl();

    char* argv[] = {
            "monsrvr"
            };
    m_ptrApp = create(1, argv);
    ICommunicationHandler& c = m_ptrApp->getCroptrolHandler();
    c.setWriter(m_ptrWriter.ptr());
    ICommunicationHandler& d = m_ptrApp->getDiscoveryHandler();
    d.setWriter(m_ptrWriter.ptr());
}

void Dapp::reset()
{
    if (m_ptr == NULL)
        m_ptr = new Dapp();
    else if (m_ptr != NULL)
        m_ptr->stop();

    m_ptr->start();
    m_ptr->clearWaitingMessages();
}

void Dapp::start()
{
    m_ptrApp->start();
}

void Dapp::stop()
{
    m_ptrApp->stop();
}

void Dapp::clearWaitingMessages()
{
    m_ptrWriter->clear();
}

void Dapp::getMessage(xcp1::Message& m, dword dw)
{
    m_ptrWriter->getMessage(m, dw);
    while(1)
    {

		if (m.getType() == xcp::msg::act ||
				m.getType() == xcp::msg::ddc ||
				m.getType() == xcp::msg::dcc ||
				m.getType() == xcp::msg::dpd ||
				m.getType() == xcp::msg::dup ||
				m.getType() == xcp::msg::drp ||
				m.getType() == xcp::msg::sdc ||
				m.getType() == xcp::msg::scc )
		{
			m.clear();
			m_ptrWriter->getMessage(m, dw);
		}
		else break;
    }
}

void Dapp::write2croptrol(xcp1::Message const& m)
{
    std::cout << "SEND " << m << std::endl;
    m_ptrApp->getCroptrolHandler().onMessage(m);
}

void Dapp::write2discovery(xcp1::Message const& m)
{
    m_ptrApp->getDiscoveryHandler().onMessage(m);
}
