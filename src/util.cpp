/*
 * util.cpp
 *
 *  Created on: Jul 8, 2014
 *      Author: emre
 */

#include "stdafx.h"
#include "util.h"


int const maxDeviceNumber = 1;
static int __AllAvailableDevId__[maxDeviceNumber];

int GetAvailableDevId()
{
	for (int i = 1 ; i <= maxDeviceNumber ; i++)
	{
		if (__AllAvailableDevId__[i] == 0)
		{
			__AllAvailableDevId__[i] = 1;
			return i;
		}
	}
	return 0;
}


void RemoveDevId(int i)
{
	if (i > maxDeviceNumber)
		throw elw::lang::Exception(MKSTR("Devid is out of range" << i));

	__AllAvailableDevId__[i] = 0;
}

void ClearAllDevId()
{
	for (int i = 1 ; i <= maxDeviceNumber ; i++)
		__AllAvailableDevId__[i] = 0;
}


uint getSensorType(uint io)
{
	int type;

	if (io == 1)
		type = 1;
	else if (io == 2 || io == 3 || io == 4)
		type = 2;
	else
		type = 3;

	return type;
}
