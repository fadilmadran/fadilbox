/*
 * CroptrolHandler.cpp
 *
 *  Created on: Nov 4, 2013
 *      Author: bulentk
 *
 */

#include "stdafx.h"
#include "DeviceManager.h"
#include "util.h"
#include "CroptrolHandler.h"

static bool validator_port(std::string const& v)
{
    return v == "0";
}


static bool validator_devid(std::string const& v)
{
	return v == "1";
}


static bool validator_ionum(std::string const& v)
{
    return v == "1" || v == "2" || v == "3" || v == "4" || v == "5";
}


CroptrolHandler::CroptrolHandler() :
    m_pWriter(NULL)
{
}


void CroptrolHandler::setWriter(IWriter* pWriter)
{
    ENTEREXIT1("%p", pWriter);
    m_pWriter = pWriter;
}


void CroptrolHandler::setHwAddr(std::string const& strHwAddr)
{
    m_strHwAddr = strHwAddr;
}


void CroptrolHandler::onMessage(xcp1::Message const& m)
{
    ENTEREXIT1("msg:%s", MKSTR(m).c_str());
    try
    {
        switch (m.getType())
        {
        case xcp::msg::rst:
            onReset(m);
            break;
        case xcp::msg::bd:
            onBeginDevice(m);
            break;
        case xcp::msg::ud:
            onUpdateDevice(m);
            break;
        case xcp::msg::ed:
            onEndDevice(m);
            break;
        case xcp::msg::rd:
            onRemoveDevice(m);
            break;
        case xcp::msg::as:
            onAddSensor(m);
            break;
        case xcp::msg::us:
            onUpdateSensor(m);
            break;
        case xcp::msg::rs:
            onRemoveSensor(m);
            break;
        case xcp::msg::ping:
            onPingConnector(m);
            break;
        case xcp::msg::gsd:
            onGetSensorValues(m);
            break;
        case xcp::msg::bact:
        	onBeginActivation(m);
        	break;
        case xcp::msg::eact:
        	onEndActivation(m);
        	break;
        default:
            throw elw::lang::Exception("Unsupported event !!!");
            break;
        }
    }
    catch (MandatoryParameterError& e)
    {
        WARNING1("%s, msg:%s", e.what(), MKSTR(m).c_str());
        sendResp(m.getType()+1, "1", MKSTR(e.attr()));
    }
    catch (std::exception& e)
    {
        WARNING1("%s, msg:%s", e.what(), MKSTR(m).c_str());
        sendResp(m.getType()+1, "2");
    }
    catch (...)
    {
        WARNING1("Message not processing. msg:%s", MKSTR(m).c_str());
        sendResp(m.getType()+1, "3");
    }
}


void CroptrolHandler::onReset(xcp1::Message const& m)
{
    ENTEREXIT1("msg:%s", MKSTR(m).c_str());
    DEVMNGR().reset();
}


void CroptrolHandler::onBeginDevice(xcp1::Message const& m)
{
    ENTEREXIT1("msg:%s", MKSTR(m).c_str());

    std::string port = GetMandatoryAttributeValue(m, xcp::attr::port, validator_port);
    std::string devid = GetMandatoryAttributeValue(m, xcp::attr::devid, validator_devid);
	std::string read_period = GetMandatoryAttributeValue(m, 8);

    if ( GetAvailableDevId() == 0)
    	throw elw::lang::Exception(MKSTR("Trying to add so many devices!"));


    m_ptrAddingDevice = new Device(
    		devid,
			::atoi(read_period.c_str())
    		 );


    INFO1("Receiving BD request. port:%s , devid:%s",
    		port.c_str(), devid.c_str()
        );

    xcp1::Message resp(m.getType()+1);
    resp.addAttribute(xcp1::Attribute(xcp::attr::rslt).addValue("0"));
    resp.addAttribute(xcp1::Attribute(xcp::attr::hwaddr).addValue(m_strHwAddr));
    resp.addAttribute(xcp1::Attribute(xcp::attr::port).addValue("0"));
    resp.addAttribute(xcp1::Attribute(xcp::attr::devid).addValue(MKSTR(devid)));

    send(resp);
}


void CroptrolHandler::onUpdateDevice(xcp1::Message const& m)
{
	ENTEREXIT0();

	std::string const port = GetMandatoryAttributeValue(m, xcp::attr::port);
	std::string const devid = GetMandatoryAttributeValue(m, xcp::attr::devid);
	std::string const ionum = GetMandatoryAttributeValue(m, xcp::attr::ionum);
	std::string const readPeriod = GetMandatoryAttributeValue(m, 8);


    INFO1("Receiving UD for sens:{ %s.%s.%s}",
                port.c_str(),devid.c_str(), ionum.c_str());

	sendUpdateDeviceResp(ionum,readPeriod);
}


void CroptrolHandler::onEndDevice(xcp1::Message const& m)
{
    ENTEREXIT1("msg:%s", MKSTR(m).c_str());

    std::string port = GetMandatoryAttributeValue(m, xcp::attr::port);
    std::string devid = GetMandatoryAttributeValue(m, xcp::attr::devid);

    if (m_ptrAddingDevice == NULL)
        throw elw::lang::Exception("New device is null");

    INFO1("Receiving ED request. port:%s , devid:%s",
    		port.c_str(), devid.c_str()
            );

    DEVMNGR().addDevice(m_ptrAddingDevice);
    m_ptrAddingDevice = NULL;

    xcp1::Message resp(m.getType()+1);
    resp.addAttribute(xcp1::Attribute(xcp::attr::rslt).addValue("0"));
    resp.addAttribute(xcp1::Attribute(xcp::attr::hwaddr).addValue(m_strHwAddr));

    send(resp);
}


void CroptrolHandler::onRemoveDevice(xcp1::Message const& m)
{
    ENTEREXIT1("msg:%s", MKSTR(m).c_str());

    std::string const& port  = GetMandatoryAttributeValue(m, xcp::attr::port, validator_port);
    std::string const& devid = GetMandatoryAttributeValue(m, xcp::attr::devid, validator_devid);
    std::string const& ionum = GetMandatoryAttributeValue(m, xcp::attr::ionum, validator_ionum);

    INFO1("Receiving RD for sens:{ %s.%s.%s}",
                port.c_str(),devid.c_str(), ionum.c_str());

    sendRemoveDeviceResp(ionum);
}


void CroptrolHandler::onAddSensor(xcp1::Message const& m)
{
	ENTEREXIT0();
	std::string const& port  = GetMandatoryAttributeValue(m, xcp::attr::port, validator_port);
    std::string const& devid = GetMandatoryAttributeValue(m, xcp::attr::devid, validator_devid);
    std::string const& ionum = GetMandatoryAttributeValue(m, xcp::attr::ionum, validator_ionum);

    INFO1("Receiving AS request for sens:{%s.%s.%s}",
            port.c_str(),
			devid.c_str(),
			ionum.c_str());


    if (m_ptrAddingDevice != NULL)
    {
    	if (m_ptrAddingDevice->getId() == devid)
    	{
    	    int type = getSensorType(::atoi(ionum.c_str()));

    	    if  (m_ptrAddingDevice->getSensor(::atoi(ionum.c_str())) != NULL)
    		   sendAddSensorError(xcp::attr::ionum);
    	    else
    	    {
				m_ptrAddingDevice->addSensor(
				new Sensor(
						::atoi(ionum.c_str()),
						 type
						)
						);
				sendAddSensorResp(ionum);
    	    }
    	}
    	else
    		sendAddSensorError(xcp::attr::devid);
    }
    else
    	DEVMNGR().addSensor(devid,ionum);
}


void CroptrolHandler::onUpdateSensor(xcp1::Message const& m)
{
	ENTEREXIT0();
	std::string const& port = GetMandatoryAttributeValue(m, xcp::attr::port, validator_port);
    std::string const& devid = GetMandatoryAttributeValue(m, xcp::attr::devid, validator_devid);
    std::string const& ionum = GetMandatoryAttributeValue(m, xcp::attr::ionum, validator_ionum);

    INFO1("Receiving US for sens:{%s.%s.%s}",
    		port.c_str(),devid.c_str(),ionum.c_str());


    if (m_ptrAddingDevice != NULL)
    {
    	if (m_ptrAddingDevice->getId() == devid)
    	{

    		m_ptrAddingDevice->updateSensor(
    						(ionum.c_str()
    						)
    				);

    		sendUpdateSensorResp(ionum);
    	}
    	else
    		sendUpdateSensorError(xcp::attr::devid);
    }
    else
    	DEVMNGR().updateSensor(devid,ionum);
}


void CroptrolHandler::onRemoveSensor(xcp1::Message const& m)
{
    ENTEREXIT1("msg:%s", MKSTR(m).c_str());

    std::string const& port = GetMandatoryAttributeValue(m, xcp::attr::port, validator_port);
    std::string const& devid = GetMandatoryAttributeValue(m, xcp::attr::devid, validator_devid);
    std::string const& ionum = GetMandatoryAttributeValue(m, xcp::attr::ionum, validator_ionum);


    INFO1("Receiving RS request for sens:{%s.%s.%s}",
    		port.c_str(),
			devid.c_str(),
			ionum.c_str());


	DEVMNGR().removeSensor(devid,ionum);
}


void CroptrolHandler::onGetSensorValues(xcp1::Message const& m)
{
    ENTEREXIT1("msg:%s", MKSTR(m).c_str());

    std::string const& port = GetMandatoryAttributeValue(m, xcp::attr::port,validator_port);
    std::string const& devid = GetMandatoryAttributeValue(m, xcp::attr::devid,validator_devid);
    std::string const& ionum = GetMandatoryAttributeValue(m, xcp::attr::ionum,validator_ionum);


    INFO1("Receiving GSD request for sens:{%s.%s.%s}",
    		port.c_str(),devid.c_str(),ionum.c_str()
			);

	DEVMNGR().getSensorData(devid,ionum);
}


void CroptrolHandler::onPingConnector(xcp1::Message const& m)
{
	ENTEREXIT0();
	DEVMNGR().pingConnector();
}


void CroptrolHandler::onBeginActivation(xcp1::Message const& m)
{
	ENTEREXIT0();
	DEVMNGR().beginActivation();
}


void CroptrolHandler::onEndActivation(xcp1::Message const& m)
{
	ENTEREXIT0();
	DEVMNGR().endActivation();
}


/////////////////////////////// Send Responses////////////////////////////////
/////////////////////////////// Send Responses////////////////////////////////
/////////////////////////////// Send Responses////////////////////////////////


void CroptrolHandler::send(xcp1::Message const& m)
{
    if (m_pWriter)
        m_pWriter->write(m);
}


void CroptrolHandler::sendResp(
        int iMsgType,
        std::string const& iRslt,
        std::string const& iErrDesc
        )
{
    xcp1::Message resp(iMsgType);
    resp.addAttribute(xcp1::Attribute(xcp::attr::rslt).addValue(iRslt));
    resp.addAttribute(xcp1::Attribute(xcp::attr::hwaddr).addValue(m_strHwAddr));
    if (!iErrDesc.empty())
    {
        resp.addAttribute(xcp1::Attribute(xcp::attr::error).addValue(iErrDesc));
    }

    send(resp);
}


void CroptrolHandler::sendSensorOptResp(
        int iMsgType,
        std::string const& devid,
        std::vector<std::string> const& lst
        )
{
    xcp1::Message resp(iMsgType);
    resp.addAttribute(xcp1::Attribute(xcp::attr::rslt).addValue(lst.empty() ? "1" : "0"));
    resp.addAttribute(xcp1::Attribute(xcp::attr::hwaddr).addValue(m_strHwAddr));
    resp.addAttribute(xcp1::Attribute(xcp::attr::port).addValue("0"));
    resp.addAttribute(xcp1::Attribute(xcp::attr::devid).addValue(devid));


    if (!lst.empty())
    {
        xcp1::Attribute ionums(xcp::attr::ionum);
        std::vector<std::string>::const_iterator it;
        for (it = lst.begin(); it != lst.end(); ++it)
        {
            ionums.addValue((*it));
        }

        resp.addAttribute(ionums);
    }
    else
    {
        resp.addAttribute(xcp1::Attribute(xcp::attr::error).addValue("5")); // ionum
    }


    send(resp);
}


void CroptrolHandler::sendNewSensorData(
		std::string const& ionum,
		int value
        )
{
    ENTEREXIT1("ionum:%s", ionum.c_str());

    xcp1::Message m(xcp::msg::nsd);

    m.addAttribute(xcp1::Attribute(xcp::attr::hwaddr).addValue(m_strHwAddr));
    m.addAttribute(xcp1::Attribute(xcp::attr::port).addValue("0"));
    m.addAttribute(xcp1::Attribute(xcp::attr::devid).addValue("1"));
    m.addAttribute(xcp1::Attribute(xcp::attr::ionum).addValue(ionum));
    m.addAttribute(xcp1::Attribute(7).addValue(MKSTR(value)));
    m.addAttribute(xcp1::Attribute(8).addValue(MKSTR(::time(NULL))));

    send(m);
}


void CroptrolHandler::sendAddSensorResp(
		std::string const& ionum
        )
{
    ENTEREXIT1("ionum: %s", ionum.c_str());

    xcp1::Message resp(xcp::msg::as + 1);
    resp.addAttribute(xcp1::Attribute(xcp::attr::rslt).addValue("0"));
    resp.addAttribute(xcp1::Attribute(xcp::attr::hwaddr).addValue(m_strHwAddr));
    resp.addAttribute(xcp1::Attribute(xcp::attr::port).addValue("0"));
    resp.addAttribute(xcp1::Attribute(xcp::attr::devid).addValue("1"));
    resp.addAttribute(xcp1::Attribute(xcp::attr::ionum).addValue(ionum));

    send(resp);
}


void CroptrolHandler::sendAddSensorError(
		int errornum
        )
{
    sendResp(xcp::msg::as+1, "1", MKSTR( errornum));
}


void CroptrolHandler::sendUpdateDeviceResp(

        std::string const& ionum,
		std::string const& readPeriod
        )
{
    xcp1::Message resp(xcp::msg::ud+1);
    resp.addAttribute(xcp1::Attribute(xcp::attr::rslt).addValue("0"));
    resp.addAttribute(xcp1::Attribute(xcp::attr::hwaddr).addValue(m_strHwAddr));
    resp.addAttribute(xcp1::Attribute(xcp::attr::port).addValue("0"));
    resp.addAttribute(xcp1::Attribute(xcp::attr::devid).addValue("0"));
    resp.addAttribute(xcp1::Attribute(xcp::attr::ionum).addValue(ionum));
    resp.addAttribute(xcp1::Attribute(8).addValue(readPeriod));

    send(resp);
}


void CroptrolHandler::sendUpdateDeviceError(
        int hata
        )
{
    sendResp(xcp::msg::ud+1, "1", MKSTR(hata));
}


void CroptrolHandler::sendRemoveDeviceResp(
        std::string const& ionum
        )
{
    xcp1::Message resp(xcp::msg::rd+1);
    resp.addAttribute(xcp1::Attribute(xcp::attr::rslt).addValue("0"));
    resp.addAttribute(xcp1::Attribute(xcp::attr::hwaddr).addValue(m_strHwAddr));
    resp.addAttribute(xcp1::Attribute(xcp::attr::devid).addValue("0"));
    resp.addAttribute(xcp1::Attribute(xcp::attr::port).addValue("0"));
    resp.addAttribute(xcp1::Attribute(xcp::attr::ionum).addValue(ionum));

    send(resp);
}


void CroptrolHandler::sendRemoveDeviceError(
        int hata
        )
{
    sendResp(xcp::msg::rd+1, "1", MKSTR(hata));
}


void CroptrolHandler::sendUpdateSensorResp(
		std::string const& ionum
        )
{
    ENTEREXIT1("ionum:%s", ionum.c_str());

    xcp1::Message resp(xcp::msg::us + 1);
    resp.addAttribute(xcp1::Attribute(xcp::attr::rslt).addValue("0"));
    resp.addAttribute(xcp1::Attribute(xcp::attr::hwaddr).addValue(m_strHwAddr));
    resp.addAttribute(xcp1::Attribute(xcp::attr::port).addValue("0"));
    resp.addAttribute(xcp1::Attribute(xcp::attr::devid).addValue("1"));
    resp.addAttribute(xcp1::Attribute(xcp::attr::ionum).addValue(ionum));

    send(resp);
}


void CroptrolHandler::sendUpdateSensorError(
        int errornum
        )
{
    sendResp(xcp::msg::us+1, "1" ,MKSTR(errornum));
}


void CroptrolHandler::sendRemoveSensorResp(
		std::string const& ionum
        )
{
    ENTEREXIT1("ionum:{ %s }, ",ionum.c_str());
    xcp1::Message resp(xcp::msg::rs + 1);
    resp.addAttribute(xcp1::Attribute(xcp::attr::rslt).addValue("0"));
    resp.addAttribute(xcp1::Attribute(xcp::attr::hwaddr).addValue(m_strHwAddr));
    resp.addAttribute(xcp1::Attribute(xcp::attr::port).addValue("0"));
    resp.addAttribute(xcp1::Attribute(xcp::attr::devid).addValue("1"));
    resp.addAttribute(xcp1::Attribute(xcp::attr::ionum).addValue(ionum));

    send(resp);
}


void CroptrolHandler::sendRemoveSensorError(
		int errornum
        )
{
        sendResp(xcp::msg::rs+1, "1" ,MKSTR(errornum));
}


void CroptrolHandler::sendSensorDataResp(
		int value,
		std::string const& ionum
		)
{
    ENTEREXIT1("ionum:{ %s }", ionum.c_str());

    xcp1::Message resp(xcp::msg::gsd + 1);
    resp.addAttribute(xcp1::Attribute(xcp::attr::rslt).addValue("0"));
    resp.addAttribute(xcp1::Attribute(xcp::attr::hwaddr).addValue(m_strHwAddr));
    resp.addAttribute(xcp1::Attribute(xcp::attr::port).addValue("0"));
    resp.addAttribute(xcp1::Attribute(xcp::attr::devid).addValue("1"));

	resp.addAttribute(xcp1::Attribute(xcp::attr::ionum).addValue(ionum));
	resp.addAttribute(xcp1::Attribute(7).addValue(MKSTR(value)));
	resp.addAttribute(xcp1::Attribute(8).addValue(MKSTR(::time(NULL))));

    send(resp);
}


void CroptrolHandler::sendSensorDataRespError(
		int errornum
        )
{
	sendResp(xcp::msg::gsd+1, "1" ,MKSTR(errornum));
}


void CroptrolHandler::sendActivatingReq()
{
	sendResp(xcp::msg::act);
}


void CroptrolHandler::sendResetResp()
{
	sendResp(xcp::msg::rst + 1);
}


void CroptrolHandler::sendBeginActResp()
{
	sendResp(xcp::msg::bact + 1);
}


void CroptrolHandler::sendEndActResp()
{
	sendResp(xcp::msg::eact + 1);
}


void CroptrolHandler::sendPingResp()
{
	sendResp(xcp::msg::ping + 1);
}


void CroptrolHandler::sendNackResp()
{
	sendResp((xcp::msg::bact + 1), "3");
}
