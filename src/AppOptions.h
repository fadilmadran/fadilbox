/*
 * AppOptions.h
 *
 *  Created on: Jul 1, 2014
 *      Author: emre
 */

#ifndef __AppOptions_h__
#define __AppOptions_h__

class AppOptions:
	public elw::util::Properties
{
	static elw::lang::SmartPtr<AppOptions> m_ptr;

public:
    static AppOptions& instance();

    uint getTelnetTimeout();
    uint getPingTimeout();
    dword getActRequiredTimeout();

public:
	AppOptions();
	virtual ~AppOptions();
};

#define APPOPTS() 	AppOptions::instance()

#endif /* __AppOptions_h__ */
