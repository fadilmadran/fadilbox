/*
 * Port.cpp
 *
 *  Created on: Nov 8, 2013
 *      Author: bulentk
 *
 */

#include "stdafx.h"
#include "Device.h"

Device::Device(
		std::string const& id,
		dword m_dwReadPeriod
		) :
	m_strDevid(id),
	m_dwReadPeriod(m_dwReadPeriod),
	m_dwTickCount(0)
{
	markLost(false);
    m_it = m_lstIO.end();
}

Device::~Device()
{
    // TODO Auto-generated destructor stub
}

std::string const& Device::getId() const
{
	return m_strDevid;
}

dword Device::getReadPeriod() const
{
	return m_dwReadPeriod;
}

void Device::setReadPeriod(dword dw)
{
	m_dwReadPeriod = dw;
}

dword Device::getReadTickCount() const
{
	return m_dwTickCount;
}

void Device::setReadTickCount(dword dw)
{
	m_dwTickCount = dw;
}

void Device::addSensor(SensorPtr ptr)
{
    m_lstIO[ptr->ionum()] = ptr;
}

void Device::updateSensor(
		std::string const& ionum)
{
	//TODO Update sensor ??

}

SensorPtr Device::removeSensor(
					int ionum)
{
    SensorPtr ptr;
    std::map<int, SensorPtr>::iterator it = m_lstIO.find(ionum);
    if (it != m_lstIO.end())
    {
        ptr = (*it).second;
        m_lstIO.erase(it);
    }
    return ptr;
}

SensorPtr Device::getSensor(int ionum)
{
    SensorPtr ptr;
    std::map<int, SensorPtr>::iterator it = m_lstIO.find(ionum);
    if (it != m_lstIO.end())
        ptr = (*it).second;
    return ptr;
}

SensorPtr Device::nextSensor()
{
    if (m_lstIO.empty())
        return NULL;
    if (m_it == m_lstIO.end())
        m_it = m_lstIO.begin();

    SensorPtr ptr = (*m_it).second;
    ++m_it;
    return ptr;
}

void Device::getSensorList(std::vector<SensorPtr>& lst)
{
    std::map<int, SensorPtr>::iterator it;
    for (it = m_lstIO.begin(); it != m_lstIO.end(); ++it)
        lst.push_back((*it).second);
}
