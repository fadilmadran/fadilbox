/*
 * DiscoveryHandler.cpp
 *
 *  Created on: Nov 4, 2013
 *      Author: bulentk
 *
 */

#include "stdafx.h"
#include "DeviceManager.h"
#include "DiscoveryHandler.h"

DiscoveryHandler::DiscoveryHandler() :
    m_pWriter(NULL)
{

}

void DiscoveryHandler::setWriter(IWriter* pWriter)
{
    ENTEREXIT1("%p", pWriter);
    m_pWriter = pWriter;
}

void DiscoveryHandler::onMessage(xcp1::Message const& m)
{
	ENTEREXIT1("type: %u", m.getType());
	try
	{
		switch (m.getType())
		{
		case 3:
			onGetConnectorValues(m);
			break;
		case 7:
			onGetDeviceValues(m);
			break;
		default:
			ERROR1("Unsupported event type:%d", m.getType());
			break;
		}
	}
	catch (elw::lang::Exception& e)
	{
        EXCEPTION1("Message not responded. error:%s, msg:%s",
        		e.what(), MKSTR(m).c_str());
	}
}

void DiscoveryHandler::onGetConnectorValues(xcp1::Message const& m)
{
	ENTEREXIT0();
	xcp1::Message resp(4);

    elw::util::Properties p;
    FlashFile::read<flash::Properties>("connector", p);

	if (m.hasAttribute(5))
	{
		resp.addAttribute(
				xcp1::Attribute(5).addValue(p.get("hwaddr"))
				);
	}

	if (m.hasAttribute(7))
	{
		resp.addAttribute(
				xcp1::Attribute(7).addValue(p.get("name"))
				);
	}

	if (m.hasAttribute(8))
	{
		resp.addAttribute(
				xcp1::Attribute(8).addValue(p.get("type"))
				);
	}

	if (m.hasAttribute(9))
	{
		resp.addAttribute(
				xcp1::Attribute(9).addValue(p.get("version"))
				);

	}

    DEBUG1("Send response:{ %s } %p", MKSTR(resp).c_str(), m_pWriter);

	m_pWriter->write(resp);
}

void GetDeviceContext(
		std::list<elw::util::Properties> lst,
		xcp1::Message& resp,
		int attrId,
		std::string const& devid
		)
{
	std::string attrName;
	if (attrId == 5)
	{
		attrName = "id";
	}
	else if (attrId == 7)
	{
		attrName = "name";
	}
	else if (attrId == 8)
	{
		attrName = "type";
	}

	xcp1::Attribute attr(attrId);
	std::list<elw::util::Properties>::iterator it;
	for (it = lst.begin(); it != lst.end(); ++it)
	{
		if ((*it).get("id") == devid)
		{
			attr.addValue((*it).get(attrName));
		}
	}
	resp.addAttribute(attr);
}

void DiscoveryHandler::onGetDeviceValues(xcp1::Message const& m)
{
	ENTEREXIT0();

	xcp1::Message resp(8);
    std::list<elw::util::Properties> lstDevice;
    FlashFile::read<flash::List<flash::Properties> >("devices", lstDevice);

    if (m.hasAttribute(12))
    {
        std::list<elw::util::Properties>::iterator it;

        xcp1::Attribute attr(12);
        for (it = lstDevice.begin() ; it != lstDevice.end() ; ++it)
        {
        	attr.addValue((*it).get("id"));
        }
        resp.addAttribute(attr);
    }
    else
    {
        std::string const& devid = GetMandatoryAttributeValue(m, 1);

        if (m.hasAttribute(5))
    	{
    	    GetDeviceContext(lstDevice, resp, 5,  devid);
    	}

    	if (m.hasAttribute(7))
    	{
    		GetDeviceContext(lstDevice, resp, 7, devid);
    	}

    	if (m.hasAttribute(8))
    	{
    		GetDeviceContext(lstDevice, resp, 8, devid);
    	}
    }

    DEBUG1("Send Message:{ %s }", MKSTR(resp).c_str());

	m_pWriter->write(resp);
}
