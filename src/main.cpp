/*
 * main.cpp
 *
 *  Created on: Mar 5, 2014
 *      Author: bulentk
 *
 */

#include "stdafx.h"
#include "CroptrolHandler.h"
#include "DiscoveryHandler.h"
#include "DeviceManager.h"

class AtmonApplication :
    public IDeviceApplication
{
    CroptrolHandler m_c;
    DiscoveryHandler m_d;

public:
    AtmonApplication()
    {
    }

    void start()
    {
        DEVMNGR().start(m_c);
    }

    void stop()
    {
        DEVMNGR().stop();
    }

    void loop(LifecycleListenerPtr ptr)
    {
    	DEVMNGR().loop(ptr);
    }

    ICommunicationHandler& getCroptrolHandler()
    {
        return m_c;
    }

    ICommunicationHandler& getDiscoveryHandler()
    {
        return m_d;
    }
};


extern "C" {

IDeviceApplication* create(int argc, char* argv[])
{
    elw::io::File f("/media/config/devbox/atmon.flash");
    if (!f.exists())
        throw elw::lang::Exception(MKSTR("Missing flash file. path:" << f.getPath()));


    INFO1("Initializing flash file. { %s }", f.getPath().c_str());
    FlashFile::initialize(f.getPath());

    return new AtmonApplication();
}

}
