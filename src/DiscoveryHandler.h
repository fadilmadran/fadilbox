/*
 * DiscoveryHandler.h
 *
 *  Created on: Nov 4, 2013
 *      Author: bulentk
 *
 */

#ifndef DiscoveryHandler_h__
#define DiscoveryHandler_h__

class DiscoveryHandler :
    public ICommunicationHandler
{
    IWriter* m_pWriter;

public:
    void onMessage(xcp1::Message const& m);

    void setWriter(IWriter* pWriter);

public:
    DiscoveryHandler();

protected:
    void onGetConnectorValues(xcp1::Message const& m);
    void onGetDeviceValues(xcp1::Message const& m);
};

#endif /* DiscoveryHandler_h__ */
