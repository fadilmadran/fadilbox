/*
 * AddDevice.h
 *
 *  Created on: Jun 3, 2014
 *      Author: bulentk
 *
 */

#ifndef AddDevice_h__
#define AddDevice_h__

#include "Event.h"
#include "Device.h"

namespace event
{

class AddDevice :
    public Event
{
    DevicePtr m_ptrDevice;
public:

    DevicePtr getDevice() const
    {
        return m_ptrDevice;
    }

    AddDevice(DevicePtr ptrDev) :
        Event(event::type::ADD_DEVICE),
        m_ptrDevice(ptrDev)
    {
    }
};

typedef elw::lang::SmartPtr<AddDevice> AddDevicePtr;

} /* namespace event */

#endif /* AddDevice_h__ */
