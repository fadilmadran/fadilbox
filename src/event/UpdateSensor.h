/*
 * UpdateSensor.h
 *
 *  Created on: Mar 7, 2014
 *      Author: bulentk
 *
 */

#ifndef UpdateSensorEvent_h__
#define UpdateSensorEvent_h__

#include "Event.h"

namespace event {

class UpdateSensor :
    public Event
{
	std::string m_devid;
	std::string m_ionum;

public:
    UpdateSensor(
    		std::string const& devid,
    		std::string const& ionum) :
        Event(event::type::UPDATE_SENSOR),
        m_devid(devid),
        m_ionum(ionum)
    {
    }

    std::string const& devid() const
    {
    	return m_devid;
    }

    std::string const& ionum() const
    {
    	return m_ionum;
    }
};

typedef elw::lang::SmartPtr<UpdateSensor> UpdateSensorPtr;

}

#endif /* UpdateSensorEvent_h__ */
