/*
 * GetSensorData.h
 *
 *  Created on: Mar 12, 2014
 *      Author: bulentk
 *
 */

#ifndef GetSensorData_h__
#define GetSensorData_h__

#include "Event.h"

namespace event {

class GetSensorData :
    public Event
{
    std::string m_devid;
    std::string m_ionum;
public:
    GetSensorData(std::string devid, std::string ionum):
        Event(event::type::GET_SENSOR_DATA),
        m_devid(devid),
		m_ionum(ionum)
    {
    }

    std::string const& ionum() const
    {
        return m_ionum;
    }

    std::string const& devid() const
    {
        return m_devid;
    }
};

typedef elw::lang::SmartPtr<GetSensorData> GetSensorDataPtr;

}

#endif /* GetSensorData_h__ */
