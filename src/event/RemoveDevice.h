/*
 * RemoveDevice.h
 *
 *  Created on: Mar 7, 2014
 *      Author: bulentk
 *
 */

#ifndef RemoveDevice_h__
#define RemoveDevice_h__

#include "Event.h"

namespace event {

class RemoveDevice :
    public Event
{
    std::string m_devid;
public:
    RemoveDevice(
    		std::string const& devid
    		) :
        Event(event::type::REMOVE_DEVICE),
        m_devid(devid)
    {
    }

    std::string const& devid() const
    {
        return m_devid;
    }

};

typedef elw::lang::SmartPtr<RemoveDevice> RemoveDevicePtr;

}

#endif /* RemoveDevice_h__ */
