/*
 * AddSensor.h
 *
 *  Created on: Mar 7, 2014
 *      Author: bulentk
 *
 */

#ifndef AddSensor_h__
#define AddSensor_h__

#include "Event.h"

namespace event {

class AddSensor :
    public Event
{
	std::string m_devid;
	std::string m_ionum;
public:
    AddSensor(
    		std::string const& id,
			std::string const& n
    		) :
        Event(event::type::ADD_SENSOR),
        m_devid(id),
        m_ionum(n)
	{
	}

    std::string const& devid() const
    {
    	return m_devid;
    }

    std::string const& ionum() const
    {
        return m_ionum;
    }

};

typedef elw::lang::SmartPtr<AddSensor> AddSensorPtr;

}

#endif /* AddSensor_h__ */
