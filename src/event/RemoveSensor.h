/*
 * RemoveSensor.h
 *
 *  Created on: Mar 7, 2014
 *      Author: bulentk
 *
 */

#ifndef RemoveSensor_h__
#define RemoveSensor_h__

#include "Event.h"

namespace event {

class RemoveSensor :
    public Event
{
    std::string m_devid;
    std::string m_ionum;
public:
    RemoveSensor(
    		std::string const& devid,
			std::string const& ionum
    		) :
        Event(event::type::REMOVE_SENSOR),
        m_devid(devid),
		m_ionum(ionum)
    {
    }

    std::string const& devid() const
    {
        return m_devid;
    }

    std::string const& ionum() const
    {
        return m_ionum;
    }
};

typedef elw::lang::SmartPtr<RemoveSensor> RemoveSensorPtr;

}

#endif /* RemoveSensor_h__ */
