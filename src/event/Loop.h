/*
 * Loop.h
 *
 *  Created on: Mar 7, 2014
 *      Author: bulentk
 *
 */

#ifndef Loop_h__
#define Loop_h__

#include "Event.h"

namespace event {

class Loop :
    public Event
{
	LifecycleListenerPtr m_ptr;

public:
    Loop(LifecycleListenerPtr& p):
        Event(event::type::LOOP),
        m_ptr(p)
    {
    }

    LifecycleListenerPtr const& getPtr() const
    {
    	return m_ptr;
    }

};

typedef elw::lang::SmartPtr<Loop> LoopPtr;

}

#endif /* Loop_h__ */
