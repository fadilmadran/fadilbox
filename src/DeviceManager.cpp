/*
 * DeviceManager.cpp
 *
 *  Created on: Mar 5, 2014
 *      Author: bulentk
 *
 */

#include "stdafx.h"
#include "Device.h"
#include "event/AddDevice.h"
#include "event/RemoveDevice.h"
#include "event/AddSensor.h"
#include "event/RemoveSensor.h"
#include "event/UpdateSensor.h"
#include "event/GetSensorData.h"
#include "event/Loop.h"
#include "DeviceManager.h"
#include "AppOptions.h"
#include "util.h"

elw::lang::SmartPtr<DeviceManager> DeviceManager::m_ptr;

DeviceManager& DeviceManager::instance()
{
    if (m_ptr == NULL)
        m_ptr = new DeviceManager();
    return *m_ptr;
}


DeviceManager::DeviceManager():
	m_pCroptrol(NULL),
	m_fIsRunning(false)
{
    elw::util::Properties p;
    FlashFile::read<flash::Properties>("connector", p);
    LOGGER().setModuleLevel(__g_uDevBoxLogID__, p.get<uint>("loglvl", 31));
}


void DeviceManager::start(CroptrolHandler& c)
{
	ENTEREXIT0();
    m_pCroptrol = &c;

    elw::util::Properties prop;
    FlashFile::read<flash::Properties> ("connector", prop);
    m_pCroptrol->setHwAddr(prop.get("hwaddr"));

    m_itCurrent = m_lstDevices.begin();

    ClearAllDevId();
    m_fIsRunning = true;
    elw::thread::Thread::start();
}


void DeviceManager::stop()
{
	m_fIsRunning = false;
    m_queue.push(NULL);
    join();
    m_pCroptrol = NULL;
    m_ptr = NULL;
}


DevicePtr DeviceManager::getNextDevice()
{
	if (m_lstDevices.empty())
		return NULL;

    if (++m_itCurrent == m_lstDevices.end())
    	return m_lstDevices.begin()->second;

    return m_itCurrent->second;
}


DevicePtr DeviceManager::getDevice(std::string const& devid)
{
	if (m_lstDevices.find(devid) == m_lstDevices.end())
		return NULL;

	return m_lstDevices.find(devid)->second;
}


void DeviceManager::read(DevicePtr& ptrDev, SensorPtr & ptrSens)
{

	ENTEREXIT0()
//	int rslt =  //TODO read öğrenince yazz

	std::string port = "0";
	int rslt = 1;

	if (rslt != ptrSens->value())
	{
		INFO1("Sensor value is changed.  Sensor : {%s.%s.%d} , New value : %d , Old value : %d",
				port.c_str(),
				ptrDev->getId().c_str(),
				ptrSens->ionum(),
				rslt,
				ptrSens->value()
				);

		m_pCroptrol->sendNewSensorData( MKSTR(ptrSens->ionum()), rslt);

		ptrSens->value(rslt);
	}
}


bool DeviceManager::getEvent(EventPtr& ptrEvent, dword dwTimeout)
{
    bool fRslt = false;

    dword dwWait = 0;
    do
    {
        if (m_queue.peek(ptrEvent))
        {
            fRslt = true;
            break;
        }

        if (dwTimeout >= 10)
        {
            elw::thread::sleep(10);
            dwWait += 10;
        }
    }
    while (dwTimeout > dwWait);

    return fRslt;
}


void DeviceManager::processEvent(EventPtr& ptrEvent)
{
    switch (ptrEvent->type())
    {
    case event::type::RESET:
    	processReset(ptrEvent);
    	break;
    case event::type::ADD_DEVICE:
        processAddDevice(ptrEvent);
        break;
    case event::type::REMOVE_DEVICE:
        processRemoveDevice(ptrEvent);
        break;
    case event::type::ADD_SENSOR:
        processAddSensor(ptrEvent);
        break;
    case event::type::UPDATE_SENSOR:
        processUpdateSensor(ptrEvent);
        break;
    case event::type::REMOVE_SENSOR:
        processRemoveSensor(ptrEvent);
        break;
    case event::type::GET_SENSOR_DATA:
        processGetSensorData(ptrEvent);
        break;
    case event::type::PING_CONNECTOR:
    	processPingConnector(ptrEvent);
    	break;
    case event::type::LOOP:
    	processLoop(ptrEvent);
    	break;
    case event::type::BEGIN_ACTIVATION:
    	processBeginActivation(ptrEvent);
    	break;
    case event::type::END_ACTIVATION:
    	processEndActivation(ptrEvent);
    	break;
    }
}


void DeviceManager::reset()
{
	ENTEREXIT0();
	m_queue.push(new Event(event::type::RESET));
}


void DeviceManager::addDevice(
		DevicePtr& ptrDev
		)
{
	ENTEREXIT0();
	m_queue.push(new event::AddDevice(ptrDev));
}


void DeviceManager::removeDevice(
		std::string const& devid
		)
{
	ENTEREXIT0();
	m_queue.push(new event::RemoveDevice(devid));
}


void DeviceManager::addSensor(
		std::string const& devid,
		std::string const& ionum
		)
{
	ENTEREXIT0();
	m_queue.push(new event::AddSensor(devid, ionum));
}


void DeviceManager::updateSensor(
		std::string const & devid,
		std::string const & ionum
		)
{
	ENTEREXIT0();
	m_queue.push(new event::UpdateSensor(devid, ionum));
}


void DeviceManager::removeSensor(
		std::string const & devid,
		std::string const& ionum
		)
{
	ENTEREXIT0();
	m_queue.push(new event::RemoveSensor(devid, ionum));
}


void DeviceManager::getSensorData(
		std::string const& devid,
		std::string const& ionum
		)
{
	ENTEREXIT0();
	m_queue.push(new event::GetSensorData(devid, ionum));
}


void DeviceManager::loop(LifecycleListenerPtr& ptr)
{
	ENTEREXIT0();
	m_queue.push(new event::Loop(ptr));
}


void DeviceManager::pingConnector()
{
	ENTEREXIT0();
	m_queue.push(new Event(event::type::PING_CONNECTOR));
}


void DeviceManager::beginActivation()
{
	ENTEREXIT0();
	m_queue.push(new Event(event::type::BEGIN_ACTIVATION));
}


void DeviceManager::endActivation()
{
	ENTEREXIT0();
	m_queue.push(new Event(event::type::END_ACTIVATION));
}


////////////////////////// PROCESS //////////////////////////


void DeviceManager::processReset(EventPtr& ptrEvent)
{
    m_lstDevices.clear();
    ClearAllDevId();
    m_pCroptrol->sendResetResp();
}


void DeviceManager::processAddDevice(EventPtr& ptrEvent)
{
	ENTEREXIT0();
    event::AddDevicePtr ptr = ptrEvent.cast<event::AddDevice>();

    DevicePtr ptrDev = ptr->getDevice();
    if (m_lstDevices.find(ptrDev->getId()) != m_lstDevices.end())
    {
    	DEBUG1("Device is already added.devid:{ %s }",
    			ptrDev->getId().c_str()
    			);
    	return;
    }

    m_lstDevices[ptrDev->getId()] = ptrDev;
    m_itCurrent = m_lstDevices.begin();
}


void DeviceManager::processRemoveDevice(EventPtr& ptrEvent)
{
	ENTEREXIT0();
    event::RemoveDevicePtr ptr = ptrEvent.cast<event::RemoveDevice>();

    DevicePtr ptrDev = getDevice(ptr->devid());
    if (ptrDev != NULL)
    {
        RemoveDevId(::atoi(ptr->devid().c_str()));
        m_lstDevices.erase(ptr->devid());
        m_itCurrent = m_lstDevices.begin();

        m_pCroptrol->sendRemoveDeviceResp(
                ptr->devid()
                );
    }
    else
    {
        m_pCroptrol->sendRemoveDeviceError(
                xcp::attr::devid
                );
    }
}


void DeviceManager::processAddSensor(EventPtr& ptrEvent)
{
    event::AddSensorPtr ptr = ptrEvent.cast<event::AddSensor>();
    DevicePtr ptrDev = getDevice(ptr->devid());

    if (ptrDev != NULL)
    {
    	int ty = getSensorType(::atoi(ptr->ionum().c_str())); //TODO aynı sensor ekleniyo mu ??

 	   if  (ptrDev->getSensor(::atoi(ptr->ionum().c_str())) != NULL)
 		   m_pCroptrol->sendAddSensorError(xcp::attr::ionum);
 	   else
 	   {
 	    	ptrDev->addSensor(
 	    			new Sensor(	::atoi(ptr->ionum().c_str()),ty)
 	    						);

 	        m_pCroptrol->sendAddSensorResp(ptr->ionum());
 	   }
    }
    else
    {
        m_pCroptrol->sendAddSensorError(
        		xcp::attr::devid
				);
    }
}


void DeviceManager::processUpdateSensor(EventPtr& evnt)
{
	ENTEREXIT0();
	event::UpdateSensor & ptr = *(evnt.cast<event::UpdateSensor>());
	DevicePtr ptrDev = getDevice(ptr.devid());

	if (ptrDev != NULL)
	{
		ptrDev->updateSensor(
				ptr.ionum()
				);

		m_pCroptrol->sendUpdateSensorResp(
				ptr.ionum()
				);
	}
    else
    {
        m_pCroptrol->sendUpdateSensorError(
        		xcp::attr::devid
				);
    }
}


void DeviceManager::processRemoveSensor(EventPtr& ptrEvent)
{
	ENTEREXIT0();
    event::RemoveSensorPtr ptr = ptrEvent.cast<event::RemoveSensor>();
    DevicePtr ptrDevice = getDevice(ptr->devid());

    if (ptrDevice != NULL)
    {

		if(ptrDevice->getSensor(::atoi(ptr->ionum().c_str())) != NULL)
		{
			ptrDevice->removeSensor(
						::atoi(ptr->ionum().c_str())
						);

			m_pCroptrol->sendRemoveSensorResp(ptr->ionum());
		}
		else
			m_pCroptrol->sendRemoveSensorError(xcp::attr::ionum);
    }
    else
    	m_pCroptrol->sendRemoveSensorError(xcp::attr::devid);
}


void DeviceManager::processGetSensorData(EventPtr& ptrEvent)
{
    event::GetSensorDataPtr ptr = ptrEvent.cast<event::GetSensorData>();
    DevicePtr ptrDevice = getDevice(ptr->devid());

    if (ptrDevice != NULL)
    {
            SensorPtr ptrSensor = ptrDevice->getSensor(::atoi(ptr->ionum().c_str()));
        	if( ptrSensor != NULL )
        	{
                m_pCroptrol->sendSensorDataResp(ptrSensor->value(), ptr->ionum());
        	}
        	else
        		m_pCroptrol->sendSensorDataRespError(xcp::attr::ionum);
    }
    else
    	m_pCroptrol->sendSensorDataRespError(xcp::attr::devid);
}


void DeviceManager::processPingConnector(EventPtr& ptrEvent)
{
	m_pCroptrol->sendPingResp();
}


void DeviceManager::processLoop(EventPtr& ptrEvent)
{
	ENTEREXIT0();
	event::LoopPtr ptr = ptrEvent.cast<event::Loop>();
	ptr->getPtr()->completed();
}


void DeviceManager::processBeginActivation(EventPtr& ptrEvent)
{
	ENTEREXIT0();
    m_lstDevices.clear();
    ClearAllDevId();
	m_pCroptrol->sendBeginActResp();
}


void DeviceManager::processEndActivation(EventPtr& ptrEvent)
{
	ENTEREXIT0();
	m_pCroptrol->sendEndActResp();
}


void DeviceManager::running()
{
	ENTEREXIT0()
    for ( ; ; )
    {
    	DevicePtr ptrDev = getNextDevice();
        try
        {
        	if (ptrDev != NULL)
        	{
        		std::vector<SensorPtr> lstSensor;
        		ptrDev->getSensorList(lstSensor);
        		std::vector<SensorPtr>::iterator it;


        		if (isTimeToRead(ptrDev))
        		{
        			DEBUG1("Device read period expired.Period:%lu",
        					ptrDev->getReadPeriod()
        					);
            		for (it = lstSensor.begin() ; it != lstSensor.end() ; ++it)
            		{
            			try
            			{
            				read(ptrDev, (*it));
            			}
            			catch (elw::lang::Exception& e)
            			{
            				EXCEPTION1("Read sensor.error:{ %s }",e.what());
    					}
            		}

        		}
        		else
        		{
        			DEBUG1("Device read period does not expired.Period:%lu",
        					ptrDev->getReadPeriod()
        					);
        		}

        	}

            {
                EventPtr ptrEvent;
                while (getEvent(ptrEvent, 200))
                {
                    if (ptrEvent == NULL)
                        return;
                    try
                    {
                        processEvent(ptrEvent);
                    }
                    catch (elw::lang::Exception& e)
                    {
                    	EXCEPTION1("Process Event Exception:{ %s }", e.what());
                    }
                }
            }
        }
        catch (elw::lang::Exception& e)
        {
        	EXCEPTION1("Process Exception:{ %s }", e.what());
        }
    }

    return;
}


bool DeviceManager::isActivationEvent(EventPtr& ptr)
{
	return ((ptr->type() == event::type::BEGIN_ACTIVATION) ||
			(ptr->type() == event::type::ADD_DEVICE)
			);
}


bool DeviceManager::isTimeToRead(DevicePtr& ptrDev)
{
	dword dwTick = elw::thread::tick();

    dword dwInterval = dwTick  >= ptrDev->getReadTickCount() ?  dwTick - ptrDev->getReadTickCount() : (MAX_ULONG - ptrDev->getReadTickCount()) + dwTick;
    if (dwInterval >= ptrDev->getReadPeriod())
    {
    	ptrDev->setReadTickCount(dwTick);
        return true;
    }

    return false;
}


void DeviceManager::activating()
{
	ENTEREXIT0();

    dword dw1;
    dword dw0 = elw::thread::tick();
    EventPtr ptrEvent;
    for ( ; ; )
    {
        if (getEvent(ptrEvent, 1000))
        {
            if (ptrEvent == NULL)
               return;

            try
            {
                if (isActivationEvent(ptrEvent))
                {
                    processEvent(ptrEvent);
                    dw0 = elw::thread::tick();
                }
                else if (ptrEvent->type() == event::type::END_ACTIVATION)
                {
                	processEndActivation(ptrEvent);
                    return;
                }
                else if (ptrEvent->type() == event::type::LOOP)
                {
                	processLoop(ptrEvent);
                }
                else
                {
                	WARNING1("Unsupported event in activating.type:{ %s }",
                			ptrEvent->tostr().c_str()
                			);

                	m_pCroptrol->sendNackResp();
                }
            }
            catch (elw::lang::Exception& e)
            {
                EXCEPTION1("Process Event Exception:{ %s }", e.what());
            }
        }

        dw1 = elw::thread::tick();
        dword dwInterval = dw1  >= dw0 ?  dw1 - dw0 : (MAX_ULONG - dw0) + dw1;
        if (dwInterval >= APPOPTS().getActRequiredTimeout())
        {
            dw0 = dw1;
            m_pCroptrol->sendActivatingReq();
        }
    }

}


int DeviceManager::run()
{
	elw::thread::sleep(500);
	m_pCroptrol->sendActivatingReq();

	activating();
	if (m_fIsRunning)
		running();
	return 0;
}
