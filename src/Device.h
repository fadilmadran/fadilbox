/*
 * Device.h
 *
 *  Created on: Nov 8, 2013
 *      Author: bulentk
 *
 */

#ifndef Device_h__
#define Device_h__

class ObjectPoint :
    public elw::IInterface
{
    bool m_fIsLost;
    uint m_uFailure;
public:
    uint failed()
    {
        return ++m_uFailure;
    }

    void succed()
    {
        m_uFailure = 0;
    }

    bool isLost() const
    {
        return m_fIsLost;
    }

    void markLost(bool fLost = true)
    {
        m_fIsLost = fLost;
    }
};

class Sensor :
    public ObjectPoint
{
    int m_ionum;
    int m_type;
    int m_iTick;
    int m_iValue;

public:
    Sensor(int num, int t) :
        m_ionum(num),
        m_type(t),
        m_iTick(0),
        m_iValue(-1)
    {
    	markLost(false);
    }

    int tick()
    {
        return m_iTick;
    }

    int ionum() const
    {
        return m_ionum;
    }

    int type() const
    {
        return m_type;
    }

    int value() const
    {
        return m_iValue;
    }

    int value(int val)
    {
        ++m_iTick;
        if (val == m_iValue)
            return val;
        int v = m_iValue;
        m_iValue = val;
        return v;
    }


};

typedef elw::lang::SmartPtr<Sensor> SensorPtr;

class Device :
    public ObjectPoint
{
	std::string m_strDevid;
	dword m_dwReadPeriod;
	dword m_dwTickCount;
    std::map<int, SensorPtr> m_lstIO;
    std::map<int, SensorPtr>::iterator m_it; //TODO ??
public:
    void addSensor(
            SensorPtr ptr
            );

    void updateSensor(
    		std::string const& ionum
			);

    SensorPtr removeSensor(
    		int ionum
            );

    SensorPtr getSensor(
    		int ionum
            );

    SensorPtr nextSensor(); //TODO ??

    void getSensorList(  //TODO ??
            std::vector<SensorPtr>& lst
            );

    std::string const& getId() const;  //TODO ??


public:
    Device(
    		std::string const& id,
    		dword m_dwReadPeriod
    		);
    virtual ~Device();

public:
    dword getReadPeriod() const;
    void setReadPeriod(dword dw);
    dword getReadTickCount() const;
    void setReadTickCount(dword dw);
};

typedef elw::lang::SmartPtr<Device> DevicePtr;

#endif /* Device_h__ */
