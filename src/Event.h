/*
 * Event.h
 *
 *  Created on: Mar 7, 2014
 *      Author: bulentk
 *
 */

#ifndef Event_h__
#define Event_h__

namespace event { namespace type {
enum
{
    RESET,
    ADD_DEVICE,
    REMOVE_DEVICE,
    ADD_SENSOR,
    UPDATE_SENSOR,
    REMOVE_SENSOR,
    GET_SENSOR_DATA,
    PING_CONNECTOR,
    BEGIN_ACTIVATION,
    END_ACTIVATION,
    LOOP
};

}}


class Event :
    public elw::IInterface
{
    uint m_type;
public:
    uint type() const
    {
        return m_type;
    }

public:
    Event(uint t) :
        m_type(t)
    {
    }

    std::string tostr() const
	{
		switch (m_type)
		{
		case event::type::RESET:
			return "RESET";
		case event::type::ADD_DEVICE:
			return "ADD_DEVICE";
		case event::type::REMOVE_DEVICE:
			return "REMOVE_DEVICE";
		case event::type::ADD_SENSOR:
			return "ADD_SENSOR";
		case event::type::UPDATE_SENSOR:
			return "UPDATE_SENSOR";
		case event::type::REMOVE_SENSOR:
			return "REMOVE_SENSOR";
		case event::type::GET_SENSOR_DATA:
			return "GET_SENSOR_DATA";
		case event::type::LOOP:
			return "LOOP";
		case event::type::PING_CONNECTOR:
			return  "PING_CONNECTOR";
		case event::type::BEGIN_ACTIVATION:
			return "BEGIN_ACTIVATION";
		case event::type::END_ACTIVATION:
			return "END_ACTIVATION";
		}

		return MKSTR("UNKNOWN type:" << m_type);
	}
};

typedef elw::lang::SmartPtr<Event>  EventPtr;

#endif /* Event_h__ */
