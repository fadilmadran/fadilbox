/*
 * AtmonApplication.h
 *
 *  Created on: Mar 5, 2014
 *      Author: bulentk
 *
 */

#ifndef AtmonApplication_h__
#define AtmonApplication_h__

#include "DiscoveryHandler.h"
#include "CroptrolHandler.h"
#include "Device.h"
#include "Event.h"


class DeviceManager :
    public elw::thread::Thread
{
private:
    static elw::lang::SmartPtr<DeviceManager> m_ptr;
public:
    static DeviceManager& instance();
private:
    CroptrolHandler* m_pCroptrol;
    bool m_fIsRunning;

    elw::thread::ThreadQueue<EventPtr> m_queue;
    std::map<std::string, DevicePtr> m_lstDevices;
    std::map<std::string, DevicePtr>::iterator m_itCurrent;

public:
    DevicePtr getDevice(std::string const& devid);

protected:
    DevicePtr getNextDevice();
    void read(DevicePtr& ptrDev, SensorPtr& ptrSens);

public:
    void start(CroptrolHandler& c);
    void stop();

protected:
    bool getEvent(EventPtr& ptrEvent, dword dwTimeout);
    void processEvent(EventPtr& ptrEvent);

public:
    void reset();
    void addDevice(DevicePtr& ptrDev);
    void removeDevice(std::string const& devid);
    void addSensor(std::string const& devid, std::string const& ionum);
    void updateSensor(
    		std::string const & devid,
    		std::string const & ionum
    		);
    void removeSensor(std::string const& devid, std::string const& ionum);
    void getSensorData(std::string const& devid, std::string const& ionum);
    void loop(LifecycleListenerPtr & ptr);
    void pingConnector();
    void beginActivation();
    void endActivation();

protected:
    void processReset(EventPtr& ptrEvent);
    void processAddDevice(EventPtr& ptrEvent);
    void processRemoveDevice(EventPtr& ptrEvent);
    //TODO processUpdateDevice() ???
    void processAddSensor(EventPtr& ptrEvent);
    void processUpdateSensor(EventPtr& evnt);
    void processRemoveSensor(EventPtr& ptrEvent);
    void processGetSensorData(EventPtr& ptrEvent);
    void processLoop(EventPtr& ptrEvent);
    void processPingConnector(EventPtr & ptrEvent);
    void processBeginActivation(EventPtr & ptrEvent);
    void processEndActivation(EventPtr & ptrEvent);

protected:

    bool isTimeToRead(DevicePtr& ptrDev);

protected:
    int run();
    void running();
    void activating();

    bool isActivationEvent(EventPtr& ptr);

protected:
    DeviceManager();
};


#define DEVMNGR()   DeviceManager::instance()

#endif /* AtmonApplication_h__ */
