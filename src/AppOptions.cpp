/*
 * AppOptions.cpp
 *
 *  Created on: Jul 1, 2014
 *      Author: emre
 */

#include "stdafx.h"
#include "AppOptions.h"

elw::lang::SmartPtr<AppOptions> AppOptions::m_ptr;

AppOptions::AppOptions()
{
    FlashFile::read<flash::Properties>("options", *this);
}

AppOptions::~AppOptions()
{

}

AppOptions& AppOptions::instance()
{
	if (m_ptr == NULL)
		m_ptr = new AppOptions();

	return *m_ptr;
}

uint AppOptions::getPingTimeout()
{
	uint u = 10;
	if ((*this).hasKey("pingtimeout"))
		u = (*this).get<uint>("pingtimeout");

	return u;
}

uint AppOptions::getTelnetTimeout()
{
	uint u = 10;
	if ((*this).hasKey("telnettimeout"))
		u = (*this).get<uint>("telnettimeout");

	return u;
}

dword AppOptions::getActRequiredTimeout()
{
	dword dw = 15000;
	if ((*this).hasKey("acttimeout"))
		dw = (*this).get<dword>("acttimeout");

	return dw;
}
