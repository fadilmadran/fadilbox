/*
 * CroptrolHandler.h
 *
 *  Created on: Nov 4, 2013
 *      Author: bulentk
 *
 */

#ifndef CroptrolHandler_h__
#define CroptrolHandler_h__

#include "Device.h"

class CroptrolHandler :
    public ICommunicationHandler
{
    IWriter* m_pWriter;
    std::string m_strHwAddr;
    DevicePtr m_ptrAddingDevice;

public:
    void onMessage(xcp1::Message const& m);
    void setWriter(IWriter* pWriter);
    void setHwAddr(std::string const& strHwAddr);

protected:
    void onReset(xcp1::Message const& m);
    void onBeginDevice(xcp1::Message const& m);
    void onUpdateDevice(xcp1::Message const& m);
    void onEndDevice(xcp1::Message const& m);
    void onRemoveDevice(xcp1::Message const& m);
    void onAddSensor(xcp1::Message const& m);
    void onUpdateSensor(xcp1::Message const& m);
    void onRemoveSensor(xcp1::Message const& m);
    void onGetSensorValues(xcp1::Message const& m);
    void onPingConnector(xcp1::Message const& m);
    void onBeginActivation(xcp1::Message const& m);
    void onEndActivation(xcp1::Message const& m);

public:
    void sendNewSensorData(
    		std::string const& ionum,
    		int value
            );

    void sendAddSensorResp(
    		std::string const& ionum
            );

    void sendAddSensorError(
    		int errornum
            );

    void sendRemoveSensorResp(
    		std::string const& ionum
            );

    void sendRemoveSensorError(
    		int errornum
    		);

    void sendUpdateSensorResp(
    		std::string const& ionum
            );

    void sendUpdateSensorError(
            int errornum
            );

    void sendSensorDataResp(
    		int value,
			std::string const& ionum
    		);

    void sendSensorDataRespError(
    		int errornum
            );

    void sendUpdateDeviceResp(
            std::string const& ionum,
			std::string const& readPeriod
            );

	void sendUpdateDeviceError(
            int hata
            );

    void sendRemoveDeviceResp(
            std::string const& ionum
            );

    void sendRemoveDeviceError(
            int hata
            );


    void sendResetResp();
    void sendBeginActResp();
    void sendEndActResp();
    void sendPingResp();
    void sendActivatingReq();
    void sendNackResp();

protected:
    void send(xcp1::Message const& m);

    void sendResp(
            int iMsgType,
            std::string const& iRslt = "0",
            std::string const& iErrDesc = ""
            );

    void sendSensorOptResp(
            int iMsgType,
            std::string const& devid,
            std::vector<std::string> const& lst
            );

public:
    CroptrolHandler();
};

#endif /* CroptrolHandler_h__ */
