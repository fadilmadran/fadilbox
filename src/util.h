/*
 * util.h
 *
 *  Created on: Jul 8, 2014
 *      Author: emre
 */

#ifndef util__h_
#define util__h_

int ping(
		std::string const& strRemoteId,
		uint uTimeout = 10
		);


int telnet(
		std::string const& strRemoteId,
		uint uTimeout = 10
		);

void RemoveDevId(int i);
void ClearAllDevId();
int GetAvailableDevId();
uint getSensorType(uint io);

#endif /* util__h_ */
